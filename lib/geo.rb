require 'rgeo/proj4'

module Geo
  class Position
    attr_accessor :latitude, :longitude

    def initialize(attributes = {})
      attributes.each { |k, v| send "#{k}=", v }
    end
    alias y latitude
    alias x longitude
    alias y= latitude=
    alias x= longitude=
  end

  class Transformation
    def self.create(from, to)
      RgeosTransformation.new(from, to)
    end
  end

  class RgeosTransformation
    def initialize(from, to)
      from = from.upcase
      to = to.upcase

      @from_coordsys = RGeo::CoordSys::Proj4.create(from)
      @to_coordsys = RGeo::CoordSys::Proj4.create(to)
    end

    def transform(position)
      x, y = RGeo::CoordSys::Proj4.transform_coords(from_coordsys, to_coordsys, position.x, position.y, nil)
      Geo::Position.new x: x.to_f.truncate(8), y: y.to_f.truncate(8)
    end

    private

    attr_reader :from_coordsys, :to_coordsys
  end
end
