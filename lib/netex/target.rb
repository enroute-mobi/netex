module Netex
  module Target
    def self.build(output, **options)
      Base.new output, **options
    end

    class Base
      attr_reader :output, :profile, :resource_count

      def initialize(output, profile: nil, participant_ref: nil, publication_timestamp: nil, validity_periods: nil, after_add: nil, profile_options: {})
        @output = output
        @profile = profile || DefaultProfile.new
        @resource_count = 0
        @after_add = after_add

        {
          participant_ref: participant_ref,
          publication_timestamp: publication_timestamp,
          validity_periods: validity_periods
        }.merge(profile_options).each do |k,v|
          # Test if k method exists before send
          self.profile.send("#{k}=", v) if self.profile.respond_to?("#{k}=") && v
        end
      end

      def participant_ref
        profile.participant_ref
      end

      def publication_timestamp
        profile.publication_timestamp
      end

      def validity_periods
        profile.validity_periods
      end

      def add(resource)
        Netex.logger.debug { "#{self.class.name} Add resource #{resource.inspect}" }
        resource = transform(resource)
        return unless resource

        @resource_count += 1

        document = document_for_resource(resource)
        Netex.logger.debug { "#{self.class.name} Add resource to Document #{document.filename}" }

        if document
          document.add(resource)
          after_add&.call resource
        end
      end
      alias << add

      attr_accessor :after_add

      def empty?
        @resource_count.zero?
      end

      def transform(resource)
        profile.transform(resource) if profile.respond_to?(:transform)
      end

      def documents
        profile.documents
      end

      def document_for_resource(resource)
        profile.document_for resource
      end

      def close
        container = profile.create_container output
        container.documents = documents
        container.write

        # Could be
        # profile.container(output).open do |writer|
        #  writer.write document
        # end
      end
    end
  end
end
