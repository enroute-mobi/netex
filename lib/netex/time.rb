# frozen_string_literal: true

module Netex
  # Manage Time of the Day : hour, minute, second and (optional) timezone
  # like xsd:time does
  class Time
    attr_accessor :hour, :minute, :second, :time_zone

    def initialize(hour, minute = 0, second = 0, time_zone = nil)
      @hour = hour
      @minute = minute
      @second = second
      @time_zone = time_zone
    end

    PARSE_REGEX = /
      \A
      ([01]?\d|2[0-3])
      :?
      ([0-5]\d)?
      :?
      ([0-5]\d)?
      \z
    /x.freeze

    # TODO: support time_zone
    def self.parse(definition)
      return definition if definition.is_a?(self.class)
      return unless PARSE_REGEX =~ definition

      hour = Regexp.last_match(1).to_i
      minute = Regexp.last_match(2).to_i
      second = Regexp.last_match(3).to_i

      new hour, minute, second
    end

    def to_xml_value
      base_part = format '%<hour>02d:%<minute>02d:%<second>02d', hour: hour, minute: minute, second: second
      base_part += time_zone.to_s if time_zone
      base_part
    end

    alias to_s to_xml_value

    def eql?(other)
      return false unless other

      hour == other.hour &&
        minute == other.minute &&
        second == other.second &&
        time_zone == other.time_zone
    end
    alias == eql?
  end
end
