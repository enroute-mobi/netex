module Netex
  module Cache
    class WithNil
      def values
        @values ||= {}
      end

      NONE = :none

      def fetch(key, &block)
        if value = values[key]
          value == NONE ? nil : value
        else
          value = block.call
          values[key] = value || NONE
          value
        end
      end
    end
  end
end
