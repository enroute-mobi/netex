module Netex
  class TimeOfDay
    attr_reader :hour, :minute, :second

    def initialize(hour: 0 , minute: 0, second: 0)
      @hour, @minute, @second = hour.to_i, minute.to_i, second.to_i
    end

    def self.parse(definition)
      return if definition.nil? || definition.empty?

      hour, minute, second = definition.split(':')
      new hour: hour, minute: minute, second: second
    end

    def to_hms
      [ format('%02d', hour), format('%02d', minute), format('%02d', second) ].join(':')
    end
  end
end
