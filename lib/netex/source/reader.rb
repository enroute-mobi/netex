# frozen_string_literal: true

module Netex
  module Source
    class Reader

      def self.for(source, filename, type: nil)
        reader_class =
          (type&.to_sym || Netex::Source.file_type(filename)) == :zip ? ZipReader : XMLReader
        reader_class.new source, filename
      end

      def initialize(source, filename)
        @source, @filename = source, filename
      end

      attr_reader :source, :filename

      # For security reason, a file bigger than 10 Giga Byte is not read
      DEFAULT_MAX_SIZE = 1024**3 * 10

      def max_size
        @max_size ||= DEFAULT_MAX_SIZE
      end

      def check_max_size(file)
        raise 'File too large when extracted' if file.size > max_size
      end

    end

    class ZipReader < Reader

      def read
        Zip::File.open(filename) do |zip_file|

          zip_file.glob("**/*.xml").each do |entry|
            check_max_size entry
            source.parse entry.get_input_stream, filename: entry.name
          end
        end
      end

    end

    class XMLReader < Reader

      def read
        File.open(filename, "r") do |file|
          check_max_size file
          source.parse file
        end
      end

    end
  end
end
