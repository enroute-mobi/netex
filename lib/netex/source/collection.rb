# frozen_string_literal: true

require 'objspace'

module Netex
  module Source
    class Collection
      include Enumerable

      def index(resource)
        resource.indexes.each do |attribute, values|
          Array(values).each do |value|
            indexes[attribute][value] << resource.id
          end
        end

        resource.indexes.clear
      end

      def indexes
        @indexes ||= Hash.new do |hash, attribute|
          hash[attribute] = Hash.new { |h, value| h[value] = [] }
        end
      end

      def find_id_by(search)
        attribute, value = search.first
        indexes[attribute][value].dup
      end

      def find_by(search)
        attribute, value = search.first
        identifiers = indexes[attribute][value]

        Enumerator.new do |enumerator|
          identifiers.each { |id| enumerator << find(id) }
        end
      end
    end

    class MemoryCollection < Collection
      def initialize
        super

        @resources = []
        @resources_by_id = {}
      end

      def add(resource)
        index resource

        @resources << resource
        @resources_by_id[resource.id] = resource
      end
      alias << add

      def find(id)
        @resources_by_id[id] unless id.nil?
      end

      def each(&block)
        @resources.each(&block)
      end

      def count
        @resources.count
      end

      def close
        @resources.clear
        @resources_by_id.clear
      end

      def memory?
        true
      end

      def size
        @resources ? @resources.sum { |r| ObjectSpace.memsize_of r } : 0
      end
    end

    class OfflineCollection < Collection
      def initialize(existing_collection = nil)
        super()

        return unless existing_collection

        existing_collection.each do |resource|
          add resource
        end

        @indexes = existing_collection.indexes
      end

      def hash
        @hash ||= Offline::Hash.new(Tempfile.new(binmode: true))
      end

      def add(resource)
        index resource

        Netex.logger.debug { "OfflineCollection#add: #{resource.inspect}" }
        hash[resource.id] = resource
      end
      alias << add

      def find(id)
        hash[id]
      end

      extend Forwardable
      def_delegators :hash, :each, :count

      def close; end

      def memory?
        false
      end

      def size
        @hash.file_size
      end
    end
  end
end
