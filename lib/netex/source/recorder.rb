module Netex::Source
  class Recorder
    def buffer
      @buffer ||= StringIO.new
    end

    def attr(name, str)
      record " #{name}='#{str}'"
    end

    def attrs_done
      record '>'
    end

    def text(text)
      encoded_text = CGI.escape_html(text)
      record encoded_text
    end

    def start_element(name)
      record "<#{name}"
    end

    def end_element(name)
      record "</#{name}>"
    end

    def record(raw_xml)
      buffer.write raw_xml
    end

    def empty?
      buffer.size == 0
    end

    def raw_xml
      buffer.string unless empty?
    end
  end
end
