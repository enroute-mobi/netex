module Netex::Source
  class Sax < ::Ox::Sax

    def initialize(include_raw_xml: false, &block)
      super()
      @source = block
      @include_raw_xml = include_raw_xml

      @line = @column = nil
      @mapper_stack = []
    end
    attr_reader :source, :mapper_stack

    def include_raw_xml?
      @include_raw_xml
    end

    def start_element(original_name)
      name = strip_namespace(original_name)
      # The Mapper can support this sub Element like:
      #
      # <StopPlace>
      #   <Name>
      #
      # or a Resource can match
      #
      # <ResourceFrame>
      #   ...
      #   <StopPlace>
      Netex.logger.info { "Sax#start_element(#{name}) at #{@line}:#{@column}" }
      Netex.logger.debug { "Sax#current_mapper: #{current_mapper.inspect}" }

      new_mapper = current_mapper&.start_element(name) ||
                   frame_mapper(name) ||
                   resource_mapper(name) ||
                   Mapper::Nop.instance

      new_mapper.line = @line
      new_mapper.column = @column

      Netex.logger.debug { "Sax#new_mapper: #{mapper_stack.size + 1} #{new_mapper.inspect}" }
      mapper_stack.push new_mapper

      current_xml_recorder&.start_element original_name
    end

    def attrs_done
      current_xml_recorder&.attrs_done
    end

    def add(resource)
      source.call resource
    end

    def resource_mapper(name)
      frame = current_frame
      frame_tags = frame.nil? ? nil : { frame_id: frame.id, frame_class: frame.class.short_name }

      mapper = Mapper::Resource.for(name)
      return unless mapper

      if include_raw_xml?
        Netex.logger.debug { 'Sax#resource_mapper: new xml recorder' }
        self.current_xml_recorder = Recorder.new
      end

      mapper.ends_with do |resource, _mapper|
        resource.tags.merge! frame_tags if frame_tags

        raw_xml = current_xml_recorder&.raw_xml
        Netex.logger.debug { "Raw resource XML: #{raw_xml.inspect}" }
        resource.raw_xml = raw_xml

        self.current_xml_recorder = nil

        add resource
      end
    end

    def frame_mapper(name)
      Mapper::Frame.for(name)&.ends_with do |frame|
        add frame
      end
    end

    class NamespaceStripper
      def initialize
        @stripped_names = {}
      end

      def strip(name)
        @stripped_names[name] ||=
          name.to_s.split(':').last.to_sym
      end
    end

    def namespace_stripper
      @namespace_stripper ||= NamespaceStripper.new
    end

    def strip_namespace(name)
      namespace_stripper.strip name
    end

    def current_mapper
      mapper_stack.last
    end

    def current_frame
      mapper_stack.reverse_each do |mapper|
        return mapper.frame if mapper.respond_to?(:frame)
      end

      nil
    end

    attr_accessor :current_xml_recorder 

    def end_element(name)
      Netex.logger.info { "Sax#end_element #{name}" }

      raise "No current_mapper" unless current_mapper
      if current_mapper.respond_to?(:name) && current_mapper.name != name
        raise "Wrong current_mapper #{current_mapper.inspect} vs #{name}"
      end

      current_xml_recorder&.end_element(name)

      mapper = mapper_stack.pop
      mapper.end_element
      Netex.logger.info { "Sax#ended_mapper: #{mapper.inspect}" }
    end

    def attr(name, value)
      return unless current_mapper
      return if name.to_s.start_with?("xmlns")

      current_mapper.attr name, value
      current_xml_recorder&.attr name, value
    end

    def text(value)
      Netex.logger.debug { "Sax#text: #{value.inspect}" }

      current_mapper.text value
      current_xml_recorder&.text value
    end

    def error(message, line, column)
      Netex.logger.error { "#{message} #{line}:#{column}" }
    end
  end
end
