# frozen_string_literal: true

module Netex
  module Source
    module Tagger
      # Tag NeTEx resources with line_id and line_name according to the associated Lines
      class Line
        def initialize(source)
          @source = source
        end

        attr_reader :source

        class Indexer < Netex::Transformer::Composite
          def initialize
            index ::Netex::ServiceJourney, by: :day_type_ref, &:day_types
            index ::Netex::DayTypeAssignment, by: :operating_period_ref
            index(::Netex::JourneyPattern, by: :scheduled_stop_point_ref) do |resource|
              resource.points_in_sequence.map &:scheduled_stop_point_ref
            end
            index(::Netex::Route, by: :route_point_ref) do |resource|
              resource.points_in_sequence.map &:route_point_ref
            end
            index ::Netex::Route, by: :direction_ref
            index ::Netex::JourneyPattern, by: :destination_display_ref
          end

          def index(resource_class, by:, &block)
            indexer = ::Netex::Transformer::Indexer.new(resource_class, by: by, &block)
            add indexer
          end
        end

        # Find and add tags for a given Resource
        class ResourceTagger
          def initialize(**default_tag_options, &tags_provider)
            @default_tag_options = default_tag_options
            @tags_provider = tags_provider
          end

          attr_accessor :tags_provider, :default_tag_options

          def tag(resource, **options)
            resource.with_tag(tags(resource_id(resource, **options)))
            resource
          end

          def resource_id(resource, attribute: nil, reference: nil)
            unless attribute || reference
              reference = default_tag_options[:reference]
              attribute = default_tag_options[:attribute]
            end

            if reference
              resource.send(reference)&.ref
            else
              resource.send(attribute)
            end
          end

          def cache_tags
            @cache_tags ||= {}
          end

          def merge_tags(tags)
            return tags if tags.is_a?(Hash)

            tags.each_with_object({}) do |merging_tags, merged_tags|
              merging_tags.each do |name, value|
                merged_tags[name] = if merged_tags.key?(name)
                                      (Array(merged_tags[name]) + [value]).uniq
                                    else
                                      value
                                    end
              end
            end
          end

          def tags(resource_id)
            return {} unless resource_id

            cache_tags[resource_id] ||= merge_tags(tags_provider.call(resource_id))
          end
        end

        def by_line_tagger
          @by_line_tagger ||= ResourceTagger.new(reference: :line_ref) do |line_id|
            line = source.lines.find(line_id)
            operator = source.operators.find(line.operator_ref&.ref)

            {
              line_id: line.id,
              line_name: line.name,
              operator_id: operator&.id,
              operator_name: operator&.name
            }.compact
          end
        end

        def by_route_tagger
          @by_route_tagger ||= ResourceTagger.new(reference: :route_ref) do |route_id|
            route = source.routes.find(route_id)
            line_id = route.line_ref&.ref

            by_line_tagger.tags(line_id)
          end
        end

        def by_route_point_tagger
          @by_route_point_tagger ||= ResourceTagger.new(attribute: :id) do |route_point_id|
            route_ids = source.routes.find_id_by(route_point_ref: route_point_id)

            route_ids.map do |route_id|
              by_route_tagger.tags(route_id)
            end
          end
        end

        def by_direction_tagger
          @by_direction_tagger ||= ResourceTagger.new(attribute: :id) do |direction_id|
            route_ids = source.routes.find_id_by(direction_ref: direction_id)

            route_ids.map do |route_id|
              by_route_tagger.tags(route_id)
            end
          end
        end

        def by_journey_pattern_tagger
          @by_journey_pattern_tagger ||= ResourceTagger.new(reference: :journey_pattern_ref) do |journey_pattern_id|
            journey_pattern = source.journey_patterns.find(journey_pattern_id)

            route_id = journey_pattern&.route_ref&.ref
            by_route_tagger.tags(route_id)
          end
        end

        def by_destination_display_tagger
          @by_destination_display_tagger ||= ResourceTagger.new(attribute: :id) do |destination_display_id|
            journey_pattern_ids = source.journey_patterns.find_id_by(destination_display_ref: destination_display_id)

            journey_pattern_ids.map do |journey_pattern_id|
              by_journey_pattern_tagger.tags(journey_pattern_id)
            end
          end
        end

        def by_service_journey_tagger
          @by_service_journey_tagger ||= ResourceTagger.new do |service_journey_id|
            service_journey = source.service_journeys.find(service_journey_id)

            journey_pattern_id = service_journey&.journey_pattern_ref&.ref
            by_journey_pattern_tagger.tags(journey_pattern_id)
          end
        end

        def by_day_type_tagger
          @by_day_type_tagger ||= ResourceTagger.new(reference: :day_type_ref) do |day_type_id|
            service_journey_ids = source.service_journeys.find_id_by(day_type_ref: day_type_id)

            service_journey_ids.map do |service_journey_id|
              by_service_journey_tagger.tags(service_journey_id)
            end
          end
        end

        def by_operating_period_tagger
          @by_operating_period_tagger ||= ResourceTagger.new(attribute: :id) do |operating_period_id|
            day_type_assignments = source.day_type_assignments.find_by(operating_period_ref: operating_period_id)

            day_type_assignments.map do |day_type_assignment|
              by_day_type_tagger.tags(day_type_assignment.day_type_ref&.ref)
            end
          end
        end

        def by_scheduled_stop_point_tagger
          @by_scheduled_stop_point_tagger ||=
            ResourceTagger.new(reference: :scheduled_stop_point_ref) do |scheduled_stop_point_id|
              journey_pattern_ids =
                source.journey_patterns.find_id_by(scheduled_stop_point_ref: scheduled_stop_point_id)

              journey_pattern_ids.map do |journey_pattern_id|
                by_journey_pattern_tagger.tags(journey_pattern_id)
              end
            end
        end

        # Decorate a collection by adding tags on each resource
        class Collection
          def initialize(collection, tagger, **tag_options)
            @collection = collection
            @tagger = tagger
            @tag_options = tag_options
          end

          attr_reader :collection, :tagger, :tag_options

          include Enumerable

          def each(&block)
            collection.each do |resource|
              block.call tag(resource)
            end
          end

          def tag(resource)
            tagger.tag(resource, **tag_options)
          end

          def find(resource_id)
            tag(collection.find(resource_id))
          end
        end

        SUPPORTED_RESOURCES = %i[
          Line Route JourneyPattern ServiceJourney
          DayType DayTypeAssignment OperatingPeriod
          ScheduledStopPoint PassengerStopAssignment
          RoutePoint Direction DestinationDisplay
        ].freeze

        def self.supported_resource_classes
          @supported_resource_classes ||= SUPPORTED_RESOURCES.map do |resource_class_name|
            Netex::Resource.for resource_class_name
          end.freeze
        end

        def each_resource(&block)
          self.class.supported_resource_classes.each do |resource_class|
            resources_method = "#{resource_class.name_of_class}s"
            send(resources_method).each(&block)
          end

          source.each_resource(except: self.class.supported_resource_classes, &block)
        end

        def resources
          enum_for :each_resource
        end

        def lines
          @lines ||= Collection.new(source.lines, by_line_tagger, attribute: :id)
        end

        def routes
          @routes ||= Collection.new(source.routes, by_line_tagger)
        end

        def route_points
          @route_points ||= Collection.new(source.route_points, by_route_point_tagger)
        end

        def directions
          @directions ||= Collection.new(source.directions, by_direction_tagger)
        end

        def journey_patterns
          @journey_patterns ||= Collection.new(source.journey_patterns, by_route_tagger)
        end

        def destination_displays
          @destination_displays ||= Collection.new(source.destination_displays, by_destination_display_tagger)
        end

        def service_journeys
          @service_journeys ||= Collection.new(source.service_journeys, by_journey_pattern_tagger)
        end

        def day_types
          @day_types ||= Collection.new(source.day_types, by_day_type_tagger, attribute: :id)
        end

        def day_type_assignments
          @day_type_assignments ||= Collection.new(source.day_type_assignments, by_day_type_tagger)
        end

        def operating_periods
          @operating_periods ||= Collection.new(source.operating_periods, by_operating_period_tagger)
        end

        def scheduled_stop_points
          @scheduled_stop_points ||= Collection.new(
            source.scheduled_stop_points,
            by_scheduled_stop_point_tagger,
            attribute: :id
          )
        end

        def passenger_stop_assignments
          @passenger_stop_assignments ||= Collection.new(
            source.passenger_stop_assignments,
            by_scheduled_stop_point_tagger
          )
        end
      end
    end
  end
end
