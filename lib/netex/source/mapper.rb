# frozen_string_literal: true

module Netex
  module Source
    module Mapper
      class Base
        attr_accessor :line, :column

        class Underscorer
          def initialize
            @underscorered = {}
          end

          def underscore(string)
            @underscorered[string] ||= string.to_s.gsub(/([a-z]+)([A-Z0-9])/, '\1_\2').downcase
          end
        end

        def self.underscorer
          @underscorer ||= Underscorer.new
        end

        def self.underscore(string)
          underscorer.underscore string
        end

        def underscore(string)
          self.class.underscore(string)
        end

        attr_reader :finalizer

        def ends_with(&block)
          @finalizer = block
          self
        end

        def finalize
          @finalizer&.call content_value, self
        end

        def end_element
          finalize
        end
      end

      class Nop < Base
        include Singleton

        def start_element(name); end
        def attr(name, value); end
        def text(value); end
      end

      class Frame < Base
        def initialize(frame_class)
          super()

          @frame_class = frame_class
        end

        attr_accessor :frame_class

        def self.for(name)
          frame_class = frame_class(name)
          return unless frame_class

          new frame_class
        end

        @@frame_class_by_name = {}
        def self.frame_class(name)
          return unless name.to_s.end_with?('Frame')

          @@frame_class_by_name[name] ||=
            begin
              Object.const_get "Netex::#{name}"
            rescue StandardError
              Netex::Frame
            end
        end

        def create_frame
          frame_class.new
        end

        def frame
          @frame ||= create_frame
        end
        alias content_value frame

        def attr(name, value)
          return if name.start_with?('xmlns')

          attribute_name = underscore(name)
          frame.send "#{attribute_name}=", value
        end

        def start_element(name)
          element_name = underscore(name).to_sym

          if (element = frame.element(element_name))
            return Mapper::Content.for(frame, element)
          end

          finalize
        end

        def finalize
          frame.tags[:line] = line
          frame.tags[:column] = column

          super
          @finalizer = nil
        end
      end

      class Content < Base
        attr_reader :resource, :content

        def initialize(resource, content)
          super()
          @resource = resource
          @content = content

          @finalizer = proc do |content_value|
            Netex.logger.debug { "#{self.class.name} change_value #{content.name} with #{content_value.inspect}" }
            content.change_value resource, content_value
          end
        end

        def self.for(resource, content)
          if content.collection?
            Mapper::Collection.new resource, content
          elsif content.reference?
            Mapper::Reference.new resource, content
          elsif content.resource?
            Mapper::Resource.for(content.type).ends_with do |content_resource|
              content.change_value resource, content_resource
            end
          else
            Mapper::Value.new resource, content
          end
        end
      end

      class Collection < Content
        def start_element(element_name)
          Netex.logger.debug { "#{self.class.name}#start_element #{element_name} #{content.type.inspect}" }

          unless content.type.xml_name == element_name
            Netex.logger.warn do
              "#{self.class.name}#start_element doesn't support mixed-type collections (#{element_name} != #{content.type.xml_name})"
            end

            return Mapper::Resource.for(element_name)&.ends_with do |collection_resource|
              Netex.logger.debug { "#{self.class.name} add_value #{collection_resource.inspect} to #{content.name}" }
              content.add_value resource, collection_resource
            end
          end

          Mapper::Content.for(resource, content.type).ends_with do |value|
            Netex.logger.debug { "#{self.class.name} add_value #{value.inspect} to #{content.name}" }
            content.add_value resource, value
          end
        end

        def end_element; end
      end

      class Value < Content
        def value
          if @language && @value
            LocalizedString.new(@value, language: @language)
          else
            @value
          end
        end
        alias content_value value

        def text(value)
          Netex.logger.debug { "#{self.class.name}#text #{value.inspect}" }
          @value = value
        end

        def attr(name, value)
          Netex.logger.debug { "#{self.class.name}#attr #{name.inspect}=#{value.inspect}" }
          return unless name == :lang

          @language = value
        end
      end

      # Manage String value with the associated language
      class LocalizedString < ::String
        attr_accessor :language

        def initialize(value, language: nil)
          super value
          self.language = language
        end
      end

      class Hash < Base
        def hash
          @hash ||= {}
        end
        alias content_value hash

        def text(value)
          Netex.logger.debug { "#{self.class.name}#text #{value.inspect}" }
          hash[:text] = value
        end

        def attr(name, value)
          name = underscore(name).to_sym
          Netex.logger.debug { "#{self.class.name}#attr #{name}=#{value}" }
          hash[name] = value
        end

        def start_element(element_name)
          element_name = underscore(element_name).to_sym
          Mapper::Hash.new.ends_with do |element_hash|
            Netex.logger.debug { "#{self.class.name} element ends_with #{element_hash.inspect}" }
            hash[element_name] = element_hash
          end
        end
      end

      class Reference < Content
        def initialize(resource, reference_content)
          super resource, reference_content
          @reference = Netex::Reference.new type: reference_content.type
        end

        attr_reader :reference
        alias content_value reference

        def attr(name, value)
          attribute_name = underscore(name)
          reference.send "#{attribute_name}=", value
        end

        VERSION_IN_TEXT = /version="([^"]+)"/

        def text(value)
          # To support IDFM special syntax like <LineRef ref="..">version="any"</LineRef>
          return unless VERSION_IN_TEXT =~ value

          reference.version = ::Regexp.last_match(1)
        end
      end

      class Resource < Base
        @@mapper_class_by_resource_class = {}
        def self.mapper_class_for(resource_class)
          @@mapper_class_by_resource_class[resource_class] ||=
            begin
              const_get resource_class.short_name, false
            rescue StandardError
              self
            end
        end

        def self.for(resource_class_or_name)
          resource_class =
            if resource_class_or_name.is_a?(Class)
              resource_class_or_name
            else
              Netex::Resource.for(resource_class_or_name)
            end
          return unless resource_class

          mapper_class_for(resource_class).new(resource_class)
        end

        def initialize(resource_class)
          super()
          @resource_class = resource_class
        end
        attr_reader :resource_class

        def has_resource?
          !resource_class.nil?
        end

        def resource
          @resource ||= create_resource
        end
        alias content_value resource

        def create_resource
          resource_class.new
        end

        def start_element(name)
          element_name = underscore(name).to_sym

          custom_map_method = "map_#{element_name}"
          if respond_to?(custom_map_method)
            Netex.logger.debug { "#{self.class.name} custom map method #{custom_map_method}" }
            return send custom_map_method
          end

          Netex.logger.debug { "#{self.class.name}#start_element #{resource_class} #{element_name}" }
          if (element = resource.element(element_name))
            Netex.logger.debug { "#{self.class.name} element found #{element.inspect}" }
            Mapper::Content.for(resource, element)
          else
            Netex.logger.debug 'no element found'
            nil
          end
        end

        def text(value)
          return if value.empty?

          Netex.logger.warn { "Resource mapper receives text: #{value.inspect}" }
        end

        def attr(name, value)
          attribute_name = underscore(name)
          resource.send "#{attribute_name}=", value
        end

        def end_element
          resource.tags[:line] = line
          resource.tags[:column] = column

          finalize
        end

        def todo
          method_name = caller[0]
          Netex.logger.info { "#{self.class.name}##{method_name} TODO" }
          nil
        end

        module TransportMode
          def map_transport_submode
            Mapper::Hash.new.ends_with do |hash|
              Netex.logger.debug { "#{self.class.name} #{hash.inspect}" }
              resource.transport_submode = hash.values.first[:text]
            end
          end
        end

        class Line < Resource
          include TransportMode
        end

        class ServiceJourney < Resource
          include TransportMode
        end

        module LegacyTransportMode
          def respond_to?(method)
            return true if method.end_with?('_submode')

            super
          end

          def method_missing(method, *args, &block)
            if method.end_with?('_submode')
              map_submode
            else
              super
            end
          end

          def map_submode
            Mapper::Hash.new.ends_with do |hash|
              Netex.logger.debug { "#{self.class.name} #{hash.inspect}" }
              resource.transport_submode = hash[:text]
            end
          end
        end

        class StopPlace < Resource
          include LegacyTransportMode
        end

        class Quay < StopPlace; end

        class VehicleJourneyStopAssignment < Resource
          def map_vehicle_journey_ref
            reference_mapper.ends_with do |reference|
              Netex.logger.debug { "#{self.class.name} #{reference.inspect}" }
              resource.vehicle_journey_refs << reference
            end
          end

          def reference_mapper
            Mapper::Reference.new(resource, reference_content)
          end

          def reference_content
            @reference_content ||= Netex::Content.new(:vehicle_journey_ref,
                                                      type: Netex::ServiceJourney, content_type: :reference)
          end
        end

        class Location < Resource
          def map_pos
            # Could be a GML::Pos mapper
            Mapper::Hash.new.ends_with do |hash|
              Netex.logger.debug { "#{self.class.name} #{hash.inspect}" }

              x, y = hash[:text].split.map(&:to_f)
              resource.coordinates = GML::Pos.new(srs_name: hash[:srs_name], x: x, y: y)
            end
          end
        end
      end
    end
  end
end
