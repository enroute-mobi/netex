module Netex
  module Target
    class Builder

      attr_reader :resource
      attr_accessor :builder_book
      def initialize(resource, options = {})
        options.each { |k,v| send "#{k}=", v }
        @resource = resource
      end

      @@builder_classes = Netex::Cache::WithNil.new
      def self.builder_class_for(resource)
        @@builder_classes.fetch(resource.class) do
          const_get(resource.class.short_name, false) rescue Netex::Target::Builder
        end
      end

      def self.for(resource, options = {})
        if resource.respond_to?(:raw_xml) && resource.raw_xml
          return Raw.new(resource)
        end

        builder_class_for(resource).new resource, options
      end

      def to_xml
        Ox.dump(to_xml_element, encoding: "UTF-8").strip
      end

      def default_xml_name
        resource.class.name.gsub('Netex::','')
      end

      attr_writer :xml_name
      def xml_name
        @xml_name ||= default_xml_name
      end

      def xml_element
        @xml_element ||= Ox::Element.new(xml_name)
      end

      def create_builder(resource, options = {})
        if builder_book
          builder_book.builder(resource, options)
        else
          self.class.for(resource, options)
        end
      end

      def to_xml_element
        resource.class.contents.each do |content|
          custom_method = "build_#{content.name}"
          if respond_to?(custom_method)
            send custom_method
            next
          end

          Content.new(self, content).build
        end

        xml_element
      end

      class Content

        attr_reader :builder, :content
        def initialize(builder, content)
          @builder, @content = builder, content
        end

        def resource
          builder.resource
        end
        def xml_element
          builder.xml_element
        end

        def build
          return unless xml_value

          if content.attribute?
            build_attribute
          else
            # TODO Ignore empty resource and collection of empty resources
            xml_element << build_element
          end
        end

        def xml_value
          @xml_value ||= content.xml_value resource
        end

        def value
          @value ||= content.value resource
        end

        def build_attribute
          xml_element[content.xml_name] = xml_value
        end

        def build_element
          if content.collection?
            build_collection_element
          elsif content.resource? || content.reference?
            build_resource_element
          else
            build_basic_element
          end
        end

        def build_resource_element
          builder.create_builder(value, xml_name: content.xml_name).to_xml_element
        end

        def build_collection_element
          # TODO manage several item types ?
          item_content = content.type

          # Create element like:
          #
          # <additionalOperators>
          #   <OperatorRef version="any" ref="first" />
          #   <OperatorRef version="any" ref="second" />
          # </additionalOperators>
          Ox::Element.new(content.xml_name).tap do |collection_element|
            if value.respond_to?(:raw_content) && value.raw_content
              collection_element << Ox::Raw.new(value.raw_content)
            end
            value.each do |item_value|
              item_element =
                builder.create_builder(item_value, xml_name: item_content.xml_name).to_xml_element
              collection_element << item_element if item_element
            end
          end
        end

        def build_basic_element
          Ox::Element.new(content.xml_name).tap do |element|
            element << xml_value
          end
        end

      end

      def build_key_list
        return if resource.key_list.nil?

        xml_key_list = Ox::Element.new("keyList")

        resource.key_list.each do |key_value|
          next if key_value.empty?

          xml_key_list << Ox::Element.new("KeyValue").tap do |xml_key_value|
            xml_key_value["typeOfKey"] = key_value.type_of_key if key_value.type_of_key
            xml_key_value << Ox::Element.new("Key").tap { |xml| xml << key_value.key.to_s }
            xml_key_value << Ox::Element.new("Value").tap { |xml| xml << key_value.value.to_s }
          end
        end

        return if xml_key_list.nodes.empty?
        xml_element << xml_key_list
      end

      def build_version
        xml_element[:version] = resource.version_or_any
      end

      class Raw
        attr_reader :resource

        def initialize resource
          @resource = resource
        end

        def to_xml
          resource.raw_xml
        end
      end

      module TransportSubmode
        def build_transport_submode
          return unless resource.transport_mode && resource.transport_submode

          intermediate_xml_name = TransportSubmode.submode_tag(resource.transport_mode)
          xml_element << Ox::Element.new(intermediate_xml_name).tap do |intermediate|
            intermediate << resource.transport_submode
          end
        end

        def self.submode_tag(transport_mode)
          unless transport_mode == 'cableway'
            "#{transport_mode.capitalize}Submode"
          else
            'TelecabinSubmode'
          end
        end
      end

      class Line < Builder
        def build_transport_submode
          return unless resource.transport_mode && resource.transport_submode

          child = Ox::Element.new("TransportSubmode").tap do |submode_xml|
            intermediate_xml_name = TransportSubmode.submode_tag(resource.transport_mode)
            submode_xml << Ox::Element.new(intermediate_xml_name).tap do |intermediate|
              intermediate << resource.transport_submode
            end
          end

          xml_element << child
        end
      end

      class StopPlace < Builder
        include TransportSubmode
      end
      class Quay < StopPlace
        include TransportSubmode
      end

      class Reference < Builder

        def default_xml_name
          unless resource.type == String
            "#{resource.type.short_name}Ref"
          else
            "Reference"
          end
        end

        def build_version
          # If the Reference .. references a Resource defined into the same Document
          # the version is mandatory.
          #
          # If the Reference references a Resource in another Document, the version
          # *must* be undefined.
          if resource.local?
            xml_element[:version] = resource.version_or_any
          end
          # TODO Use versionRef
          # else
          #   xml_element[:versionRef] = resource.version_or_any
          # end
        end

      end

      class Location < Builder

        def build_coordinates
          return if resource.coordinates.nil?
          xml_element << Ox::Element.new("gml:pos").tap do |gml_pos|
            gml_pos[:srsName] = resource.coordinates.srs_name
            gml_pos << "#{resource.coordinates.x} #{resource.coordinates.y}"
          end
        end

      end

      class VehicleJourneyStopAssignment < Builder
        def build_vehicle_journey_refs
          resource.vehicle_journey_refs.each do |reference|
            xml_element << Reference.new(reference, xml_name: 'VehicleJourneyRef').to_xml_element
          end
        end
      end

      class OperatingPeriod < Builder

        def build_from_date
          add_date_element "FromDate", resource.from_date
        end

        def build_to_date
          to_date = resource.to_date
          to_date += 1 if to_date.is_a?(Date)

          add_date_element "ToDate", to_date
        end

        def add_date_element(name, value)
          return unless value
          xml_element << Ox::Element.new(name).tap do |xml_datetime|
            xml_datetime << format_as_time(value)
          end
        end

        def format_as_time(date_or_time)
          case date_or_time
          when ::Time
            self.class.format_time date_or_time
          when Date
            self.class.format_date_as_time date_or_time
          else
            date_or_time.to_s
          end
        end
      end

      class UicOperatingPeriod < OperatingPeriod
      end

      # Build (fastly) XML for TimetabledPassingTime resource
      class TimetabledPassingTime < Builder
        def to_xml_element # rubocop:disable Metrics/AbcSize,Metrics/MethodLength,Metrics/CyclomaticComplexity,Metrics/PerceivedComplexity
          Ox::Element.new('TimetabledPassingTime').tap do |element| # rubocop:disable Metrics/BlockLength
            element[:version] = resource.version || 'any'
            element[:id] = resource.id

            if (stop_point_in_journey_pattern_ref = resource.stop_point_in_journey_pattern_ref)
              # element << create_builder(resource.stop_point_in_journey_pattern_ref).to_xml_element
              element << Ox::Element.new('StopPointInJourneyPatternRef').tap do |reference|
                reference[:ref] = stop_point_in_journey_pattern_ref.ref
                if stop_point_in_journey_pattern_ref.local?
                  reference[:version] = stop_point_in_journey_pattern_ref.version || 'any'
                end
              end
            end

            if resource.arrival_time
              element << Ox::Element.new('ArrivalTime').tap do |time|
                time << resource.arrival_time.to_s
              end
            end

            if resource.arrival_day_offset && resource.arrival_day_offset != 0
              element << Ox::Element.new('ArrivalDayOffset').tap do |time|
                time << resource.arrival_day_offset.to_s
              end
            end

            if resource.departure_time
              element << Ox::Element.new('DepartureTime').tap do |time|
                time << resource.departure_time.to_s
              end
            end

            if resource.departure_day_offset && resource.departure_day_offset != 0
              element << Ox::Element.new('DepartureDayOffset').tap do |time|
                time << resource.departure_day_offset.to_s
              end
            end
          end
        end
      end

      def self.format_time(time_or_date)
        time_or_date.to_time.utc.strftime('%Y-%m-%dT%H:%M:%SZ')
      end

      def self.format_time_only(time)
        time.strftime('%H:%M:%S')
      end

      def self.format_date(date)
        date.strftime('%Y-%m-%d')
      end

      def self.format_date_as_time(date)
        date.strftime('%Y-%m-%dT00:00:00')
      end

    end
  end
end
