module Netex
  module Target
    class Section

      attr_accessor :name, :accept, :accept_subclasses, :frame

      def initialize(attributes = {})
        self.accept_subclasses = false

        attributes.each { |k,v| send "#{k}=", v }
      end

      def accept?(resource)
        return true unless accept

        if accept_subclasses?
          resource.is_a? accept
        else
          resource.class == accept
        end
      end

      alias accept_subclasses? accept_subclasses

      def empty?
        @fragment.nil?
      end

      def fragment
        @fragment ||= Fragment::Auto.new
      end

      def add(resource)
        fragment.write to_xml(resource)
      end

      def frame_builder_book
        frame&.builder_book
      end

      attr_writer :builder_book
      def builder_book
        @builder_book ||= frame_builder_book || BuilderBook.new
      end

      def to_xml(resource)
        builder_book.to_xml resource
      end

      def to_xml_content
        fragment.to_xml_content
      end

    end
  end
end
