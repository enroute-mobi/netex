module Netex
  module Target
    module Container

      class Base

        attr_reader :output
        attr_accessor :documents

        def initialize(output)
          @output = output
        end

        def open_output
          if stream?
            yield output
          else
            file_mode = 'w'
            file_mode = "#{file_mode}b" if binary?

            File.open(output,file_mode) do |file|
              yield file
            end
          end
        end

        def binary?
          true
        end

        def stream?
          @stream ||= output.respond_to?(:write)
        end

      end

      class XML < Base

        def binary?
          false
        end

        def single_document
          unless documents&.size == 1
            raise "XML container only supports a single document: #{documents}"
          end
          documents.first
        end

        def write
          open_output do |io|
            single_document.write io
          end
        end

      end

      class Zip < Base

        def write
          open_output do |io|
            zip_stream = ::Zip::OutputStream.write_buffer io do |zip_io|
              documents.each do |document|
                zip_io.put_next_entry document.filename
                document.write zip_io
              end
            end
            zip_stream.flush
          end
        end

      end

    end
  end
end
