module Netex
  module Target
    class BuilderBook

      def builder(resource, options = {})
        Netex::Target::Builder.for resource, options.merge(builder_book: self)
      end

      def to_xml(resource)
        builder(resource).to_xml.tap do |xml|
          resource.raw_xml = xml
        end
      end

      # Provides #to_xml method when a BuilderBook has been provided
      #
      #   class MyClass
      #     include Netex::Target::BuilderBook::Support
      #
      #     def method
      #       xml = to_xml(resource)
      #     end
      #   end
      module Support
        extend ActiveSupport::Concern

        included do
          attr_accessor :builder_book
        end

        def to_xml(resource)
          builder_book&.to_xml resource
        end
      end
    end
  end
end
