module Netex
  module Target
    module Fragment

      class Memory

        def initialize
          @buffer = StringIO.new
        end

        def write(content)
          @buffer.write content
          @buffer.write "\n"
        end

        def copy_to(output)
          output.write @buffer.string
        end

        def size
          @buffer.size
        end

        def memory?
          true
        end

        def to_xml_content
          # Remove final end of line
          Ox::Raw.new @buffer.string[0..-2]
        end

      end

      class File

        def initialize
          @file = Tempfile.new
        end

        def write(content)
          @file.write content
        end

        def copy_to(output)
          @file.rewind
          IO.copy_stream @file, output
        end

        def memory?
          false
        end

        def mark
          @mark ||= Mark.new
        end

        def to_xml_content
          Ox::Comment.new mark.to_text
        end

      end

      class Auto

        # Default cache is 64kB (to save some IO without using too many memory)
        attr_accessor :offline_threshold
        def initialize(offline_threshold = 64)
          @offline_threshold = offline_threshold
        end

        def fragment
          @fragment ||= Memory.new
        end

        KILO_BYTE = 1024
        def max_memory_fragment_size
          offline_threshold * KILO_BYTE
        end

        extend Forwardable
        def_delegators :fragment, :write, :copy_to, :to_xml_content, :mark, :memory?

        def write(content)
          fragment.write content
          switch_fragment
        end

        def switch_fragment
          if fragment.memory? && fragment.size > max_memory_fragment_size
            switch_fragment!
          end
        end

        def switch_fragment!
          file_fragment = File.new
          @fragment.copy_to file_fragment
          @fragment = file_fragment
        end

      end


      class Mark

        SIGNATURE = "Netex::Document Fragment Mark"
        FORMAT = /#{SIGNATURE} \[([0-9]+)\]/

        def initialize(identifier = nil)
          @identifier = (identifier || object_id)
        end

        def self.parse(line)
          if line =~ FORMAT
            new $1.to_i
          end
        end

        def hash
          identifier.hash
        end

        def eql?(other)
          identifier == other.identifier
        end
        alias == eql?

        def to_text
          "#{SIGNATURE} [#{identifier}]"
        end

        protected

        attr_reader :identifier

      end
    end
  end
end
