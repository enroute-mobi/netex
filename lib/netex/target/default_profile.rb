module Netex
  module Target
    class DefaultProfile

      attr_accessor :participant_ref, :publication_timestamp, :validity_periods
      def document_attributes
        { participant_ref: participant_ref,
          publication_timestamp: publication_timestamp }
      end

      def default_document
        @default_document ||= Target::Document.default(document_attributes)
      end

      # Transform with defined transformers
      include Netex::Transformer::Support

      transform Netex::Transformer::OrphanQuaysAsStopPlace
      transform Netex::Transformer::EmbeddedEntrances
      transform Netex::Transformer::EmbeddedQuays
      transform Netex::Transformer::EmbeddedPassingTimes

      attr_writer :builder_book
      def builder_book
        @builder_book ||= Netex::Target::BuilderBook.new
      end

      def document_for(_)
        default_document
      end

      def documents
        @documents ||= [default_document]
      end

      def create_container(output)
        Container::XML.new output
      end

    end
  end
end
