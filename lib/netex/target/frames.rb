# frozen_string_literal: true

module Netex
  module Target
    class Frame
      attr_accessor :id, :version, :data_source_ref, :name, :modification, :type_of_frame_ref

      def initialize(attributes = {})
        attributes.each { |k, v| send "#{k}=", v }
      end

      attr_writer :builder_book

      def builder_book
        @builder_book ||= BuilderBook.new
      end

      def sections
        @sections ||= Sections.new(self)
      end

      def validity_periods
        @validity_periods ||= ValidityPeriods.new
      end

      def validity_periods=(periods)
        @validity_periods = ValidityPeriods.new(periods)
      end

      class ValidityPeriods
        def initialize(periods = nil)
          @periods = periods || []
        end

        def to_xml_element!(element)
          @periods.each do |validity_period|
            validity_between = Ox::Element.new('ValidBetween')

            from = Ox::Element.new('FromDate')
            from << validity_period.begin.strftime('%Y-%m-%dT00:00:00%Z')
            validity_between << from

            to = Ox::Element.new('ToDate')
            to << validity_period.end.strftime('%Y-%m-%dT00:00:00%Z')
            validity_between << to

            element << validity_between
          end
        end
      end

      class Sections < SimpleDelegator
        def initialize(frame)
          super []
          @frame = frame
        end

        attr_reader :frame

        def create(attributes = {})
          section = Section.new(attributes.merge(frame: frame))
          push section
          section
        end

        def not_empty
          select { |s| !s.empty? }
        end
      end

      def empty?
        sections.not_empty.empty?
      end

      # SiteFrame, GeneralFrame, etc
      def xml_tag_name
        # TODO
        @xml_tag_name ||= self.class.name.split('::').last
      end

      def to_xml_element
        # <ThisFrame id="..." version="..." dataSourceRef="...">
        Ox::Element.new(xml_tag_name).tap do |element|
          element[:id] = id if id
          element[:version] = (version || 'any')
          element[:dataSourceRef] = data_source_ref if data_source_ref
          element[:modification] = modification if modification

          validity_periods.to_xml_element! element

          element << Ox::Element.new('Name').tap do |xml|
            xml << name
          end if name

          if type_of_frame_ref
            xml_type_of_frame_ref =
              if type_of_frame_ref.is_a?(String)
                Ox::Element.new('TypeOfFrameRef').tap do |element|
                  element[:ref] = type_of_frame_ref
                end
              else
                builder_book.builder(type_of_frame_ref, xml_name: 'TypeOfFrameRef').to_xml_element
              end

            element << xml_type_of_frame_ref
          end

          to_xml_element!(element)
        end
      end

      def to_xml_element!(element)
        # <ThisFrame ...>
        #   <section1>
        #     <!-- content -->
        #   </section1>
        #   <section2>
        #     <!-- content -->
        #   </section2>
        sections.not_empty.each do |section|
          section_receiver =
            if section.name
              Ox::Element.new(section.name).tap do |section_xml|
                element << section_xml
              end
            else
              element
            end

          section_receiver << section.to_xml_content
        end
      end
    end

    class GeneralFrame < Frame
      def to_xml_element!(element)
        # <GeneralFrame ...>
        #   <TypeOfFrameRef ref="..."/>
        #     <members>
        #       <!-- section1 -->
        #       <!-- section2 -->

        xml_members = Ox::Element.new('members')
        element << xml_members

        sections.not_empty.each do |section|
          # The section can return:
          # * raw xml
          # * a fragment comment
          xml_members << section.to_xml_content
        end
      end
    end

    class ResourceFrame < Frame
      def self.default
        new.tap do |frame|
          frame.sections.create name: 'organisations', accept: Netex::Operator
        end
      end
    end

    class SiteFrame < Frame
      def self.default
        new.tap do |frame|
          frame.sections.create name: 'stopPlaces', accept: Netex::StopPlace
          frame.sections.create name: 'pointsOfInterest', accept: Netex::PointOfInterest
        end
      end
    end

    class ServiceFrame < Frame
      def self.default
        # Sequence defined in netex_serviceFrame_version.xsd
        # https://github.com/NeTEx-CEN/NeTEx/blob/63602b2990a9647d23599f94d788f7aa1fc20747/xsd/netex_part_2/part2_frames/netex_serviceFrame_version.xsd#L126

        new.tap do |frame|
          frame.sections.create name: 'directions', accept: Netex::Direction
          frame.sections.create name: 'routePoints', accept: Netex::RoutePoint
          # routeLinks
          frame.sections.create name: 'routes', accept: Netex::Route
          frame.sections.create name: 'lines', accept: Netex::Line
          # groupsOfLines
          frame.sections.create name: 'destinationDisplays', accept: Netex::DestinationDisplay
          # lineNetworks
          frame.sections.create name: 'scheduledStopPoints', accept: Netex::ScheduledStopPoint
          # serviceLinks
          # servicePatterns ?
          # stopAreas ?
          # connections
          # tariffZones
          frame.sections.create name: 'tariffZones', accept: Netex::FareZone

          # timingLinks
          # timingPatterns
          frame.sections.create name: 'stopAssignments', accept: Netex::PassengerStopAssignment
          frame.sections.create name: 'journeyPatterns', accept: Netex::ServiceJourneyPattern
          # transferRestrictions
          frame.sections.create name: 'routingConstraintZones', accept: Netex::RoutingConstraintZone
          # serviceExclusions
          # timeDemandTypes
          frame.sections.create name: 'notices', accept: Netex::Notice
          # noticeAssignments
        end
      end
    end

    class TimetableFrame < Frame
      def self.default
        new.tap do |frame|
          frame.sections.create name: 'vehicleJourneys', accept: Netex::ServiceJourney
          frame.sections.create name: 'vehicleJourneyStopAssignments', accept: Netex::VehicleJourneyStopAssignment
        end
      end
    end

    class ServiceCalendarFrame < Frame
      def self.default
        new.tap do |frame|
          # TODO: we should not accept several ServiceCalendars
          frame.sections.create accept: Netex::ServiceCalendar
          frame.sections.create name: 'dayTypes', accept: Netex::DayType
          frame.sections.create name: 'operatingPeriods', accept: Netex::OperatingPeriod, accept_subclasses: true
          frame.sections.create name: 'dayTypeAssignments', accept: Netex::DayTypeAssignment
        end
      end
    end
  end
end
