module Netex
  module Target
    module ReferenceScope

      class Base

        def view(_resource)
          raise "Not yet implemented"
        end

        def local?(_reference)
          raise "Not yet implemented"
        end

        def mark_local_references(resource)
          resource.references.each do |reference|
            reference.local! if local? reference
          end
        end

      end

      class ByClass < Base
        def seen_classes
          @seen_classes ||= Set.new
        end

        def view(resource)
          resource.referencables.each do |referencable|
            seen_classes.add referencable.class
          end
        end

        def local?(reference)
          # We're ignoring "free" references like TypeOfXXXRef
          return false if reference.type == String
          seen_classes.any? do |seen_class|
            seen_class <= reference.type
          end
        end
      end
    end
  end
end
