module Netex
  module Target
    class Document

      attr_accessor :filename
      attr_writer :participant_ref, :publication_timestamp

      def initialize(attributes = {})
        attributes.each { |k,v| send "#{k}=", v }
      end

      def participant_ref
        @participant_ref ||= "enRoute"
      end

      def publication_timestamp
        @publication_timestamp ||= ::Time.now
      end

      def xml_namespaces
        @xml_namespaces ||= {}
      end

      def to_s
        "#<Netex::Target::Document '#{filename}'>"
      end

      def self.default(attributes = {})
        new(attributes).tap do |document|
          # TODO ?
          # document.publication_delivery.version = "TODO"
          document.publication_delivery.timestamp = document.publication_timestamp
          document.publication_delivery.participant_ref = document.participant_ref
          document.publication_delivery.xml_namespaces = document.xml_namespaces

          codespace = document.publication_delivery.participant_ref

          document.composite_frame.id = "#{codespace}:CompositeFrame:1"
          # document.composite_frame.version = "TODO"

          document.frames << ResourceFrame.default
          document.frames << SiteFrame.default
          document.frames << ServiceFrame.default
          document.frames << TimetableFrame.default
          document.frames << ServiceCalendarFrame.default

          # TODO Better solution for frame default id ?
          document.frames.each do |frame|
            frame.id = "#{codespace}:#{frame.xml_tag_name}:1"
          end
        end
      end

      def publication_delivery
        @publication_delivery ||= PublicationDelivery.new
      end

      def composite_frame
        @composite_frame ||= CompositeFrame.new(builder_book: builder_book)
      end

      attr_writer :builder_book
      def builder_book
        @builder_book ||= BuilderBook.new
      end

      def frames
        @frames ||= Frames.new(self)
      end

      class Frames < SimpleDelegator
        def initialize(document)
          super []
          @document = document
        end
        attr_reader :document

        def push(frame)
          super frame
          frame.builder_book = document.builder_book
        end
        alias << push

        def not_empty
          select { |s| ! s.empty? }
        end
      end

      def reference_scope
        @reference_scope ||= ReferenceScope::ByClass.new
      end

      def add(resource)
        reference_scope.view resource
        reference_scope.mark_local_references resource

        # Find the first section which accepts the resource
        each_section do |section|
          if section.accept? resource
            section.add resource
            return
          end
        end
      end

      def each_section
        frames.each do |frame|
          frame.sections.each do |section|
            yield section
          end
        end
      end

      def sections
        enum_for(:each_section)
      end

      def each_fragment
        each_section do |section|
          yield section.fragment unless section.empty?
        end
      end

      def fragments
        enum_for(:each_fragment)
      end

      def memory?
        fragments.all?(&:memory?)
      end

      def include_composite_frame?
        frames.not_empty.size > 1
      end

      def to_xml_document
        Ox::Document.new.tap do |doc|
          instruct = Ox::Instruct.new(:xml)
          instruct[:version] = '1.0'
          instruct[:encoding] = 'UTF-8'
          doc << instruct

          xml_publication_delivery = publication_delivery.to_xml_element
          doc << xml_publication_delivery

          data_objects = Ox::Element.new('dataObjects')
          xml_publication_delivery << data_objects

          frames_container = data_objects

          if include_composite_frame?
            # <?xml version="1.0" encoding="utf-8"?>
            # <PublicationDelivery ...>
            #   <dataObjects>
            #     <CompositeFrame ...>
            #       <frames>

            xml_composite_frame = composite_frame.to_xml_element
            data_objects << xml_composite_frame

            xml_frames = Ox::Element.new('frames')
            xml_composite_frame << xml_frames

            frames_container = xml_frames
          end

          frames.not_empty.each do |frame|
            frames_container << frame.to_xml_element
          end
        end
      end

      def write(output)
        Writer.new(self).write(output)
      end

    end

    class PublicationDelivery
      attr_accessor :version, :timestamp, :participant_ref

      def xml_namespaces
        @xml_namespaces ||= {}
      end
      attr_writer :xml_namespaces

      def to_xml_element
        Ox::Element.new('PublicationDelivery').tap do |element|
          element[:xmlns] = "http://www.netex.org.uk/netex"

          if xml_namespaces
            xml_namespaces.each do |key, url|
              element["xmlns:#{key}"] = url
            end
          end

          element[:version] = version if version

          element << Ox::Element.new('PublicationTimestamp').tap do |xml|
            xml << Builder.format_time(timestamp)
          end if timestamp

          element << Ox::Element.new('ParticipantRef').tap do |xml|
            xml << participant_ref.to_s
          end if participant_ref
        end
      end
    end

    class CompositeFrame < Netex::Target::Frame

    end

    class Writer

      attr_reader :document

      def initialize(document)
        @document = document
      end

      def write(output)
        if document.memory?
          write_base output
        else
          write_in_two_phases output
        end
      end

      def write_in_two_phases(output)
        Tempfile.open do |base_file|
          write_base base_file
          base_file.rewind

          replace_fragment_marks base_file, output
        end
      end

      def write_base(output)
        output.write Ox.dump(document.to_xml_document, encoding: "UTF-8")
      end

      def fragments_by_mark
        @fragments_by_mark ||= document.fragments.select { |f| !f.memory? }.group_by(&:mark)
      end

      def replace_fragment_marks(base_file, output)
        loop do
          line = base_file.readline

          if mark = Fragment::Mark.parse(line)
            fragment = fragments_by_mark[mark].first
            raise "Missing fragment: #{mark}" unless fragment

            fragment.copy_to output
          else
            output.puts line
          end
        end
      rescue EOFError
        # ok
      end

    end

  end


end
