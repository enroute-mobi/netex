# frozen_string_literal: true

module Netex
  module Profile

    def self.create(type)
      class_for(type).new
    end

    def self.classify(type)
      type.to_s.sub("idfm","IDFM").sub("iboo","IBOO").sub("icar","ICAR")
        .gsub(/^(.)/) { $1.upcase }
        .gsub(/_(.)/) { $1.upcase }
        .gsub(%r{/(.)}) { "::" + $1.upcase }
    end

    def self.class_for(type)
      const_get classify(type)
    end

    class Base
      # Transform with defined transformers
      include Netex::Transformer::Support

      attr_reader :container_type
      attr_accessor :participant_ref, :publication_timestamp, :validity_periods


      def documents_by_name
        @documents_by_name ||= {}
      end

      def documents
        documents_by_name.values
      end

      def document_attributes
        {
          participant_ref: participant_ref,
          publication_timestamp: publication_timestamp,
          builder_book: builder_book
        }
      end

      attr_writer :builder_book
      def builder_book
        @builder_book ||= Netex::Target::BuilderBook.new
      end

      def base_document(name, &block)
        Target::Document.default(document_attributes.merge(filename: name)).tap do |document|
          block.call document if block_given?
        end
      end

      def document(name, &block)
        documents_by_name[name] ||= base_document(name, &block)
      end

      def create_container(output)
        Target::Container::Zip.new output
      end

      protected

      attr_writer :container_type

    end
  end
end
