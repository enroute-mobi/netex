# frozen_string_literal: true

module Netex
  module Offline
    #
    # Store key/value pairs into a File
    #
    class Hash
      def initialize(file)
        @file = file
        @count = 0
      end

      def [](key)
        return nil if key.nil?

        fetch key
      end

      def []=(key, object)
        store(key, object)
      end

      def keys
        contents.keys
      end

      def values
        enum_for :each
      end

      def file_size
        file.size
      end

      attr_reader :count

      private

      attr_reader :file

      def contents
        @contents ||= {}
      end

      def marshaller
        @marshaller ||= Marshal
      end

      def store(key, object)
        @count += 1

        file.seek 0, :END

        tell = file.tell
        size = file.write(marshaller.dump(object))

        contents[key] = Content.new(tell, size) unless key.nil?
      end

      def fetch(key)
        content = contents[key]
        return nil unless content

        file.seek(content.offset)
        marshaller.load(file.read(content.size))
      end

      def each
        return enum_for(:each) unless block_given?

        file.rewind

        begin
          loop do
            yield Marshal.load(file) # rubocop:disable Security/MarshalLoad
          end
        rescue EOFError
          # End Of the File, End Of the Loop
        end

        self
      end

      # Describe offset and size of a Content stored in the file
      class Content
        def initialize(offset, size)
          @offset = offset
          @size = size
        end

        attr_reader :offset, :size
      end
    end
  end
end
