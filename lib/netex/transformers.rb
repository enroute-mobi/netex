# frozen_string_literal: true

module Netex
  module Transformer
    # class Base
    #   class_attribute :transformed_resource_classes, default: []
    #   def self.transform(*resource_classes)
    #     self.transformed_resource_classes = resource_classes
    #   end

    #   def transform(resource)
    #     resource
    #   end

    #   def transform?(resource)
    #     resource_classes.any? do |resource_class|
    #       resource.is_a? resource_class
    #     end
    #   end
    # end

    # Defines a transform method with uses transformers defined in the class
    #
    #   class MyClass
    #     include Netex::Transformer::Support
    #
    #     transform First
    #     transform Second, option: 'dummy'
    #
    #     def transform(resource)
    #       resource = super(resource)
    #       # First and Second transformers are already applied
    #     end
    #   end
    module Support
      extend ActiveSupport::Concern

      included do
        class_attribute :transformer_configs, default: []
      end

      class_methods do
        def transform(transformer_class, options = {})
          self.transformer_configs += [Config.new(transformer_class, options)]
        end
      end

      class Config
        def initialize(transformer_class, options = {})
          @transformer_class = transformer_class
          @options = options
        end

        attr_reader :transformer_class, :options

        def transformer(&block)
          transformer_class.new(**options).tap do |transformer|
            block.call transformer if block_given?
          end
        end
      end

      def transformers
        @transformers ||= transformer_configs.map do |transformer_config|
          transformer_config.transformer do |transformer|
            transformer.builder_book = builder_book if transformer.respond_to?('builder_book=')
          end
        end
      end

      def transform(original_resource)
        transformers.reduce(original_resource) do |resource, transformer|
          transformer.transform(resource)
        end
      end
    end

    # Transform the given resource if a method transform_<name_of_class> is defined
    #
    #   class MyTransformer < ByClass
    #     def transform_stop_place_entrance(resource)
    #       # Given resources are only Netex::StopPlaceEntrance
    #     end
    #   end
    class ByClass
      def transform(resource)
        return nil unless resource

        resource_method = "transform_#{resource.class.name_of_class}"
        if respond_to? resource_method
          send resource_method, resource
        else
          resource
        end
      end
    end

    # Index in the given resource the target attribute
    #
    #   source.transformers << Netex::Transformer::Indexer.new(Netex::DayTypeAssignment, by: :day_type_ref)
    #   source.add Netex::DayTypeAssignment.new(day_type_ref: 'target')
    #   source.day_type_assignments.find_by(day_type_ref: 'target')
    #
    class Indexer
      def initialize(resource_class, by:, &block)
        @resource_class = resource_class
        @attribute = by
        @reducer = block_given? ? block : Proc.new { |r| r.send(attribute) }
      end

      attr_reader :resource_class, :attribute, :reducer

      def transform(resource)
        return resource unless resource.is_a?(resource_class)

        value = reducer.call(resource)
        return resource if value.nil?

        case value
        when Netex::Reference
          value = value.ref
        when Array
          value = value.compact.map(&:ref)
        end

        resource.indexes[attribute] = value

        resource
      end
    end

    class ServiceJourneyDayTypesIndexer
      def transform(resource)
        return resource unless resource.is_a?(Netex::ServiceJourney)

        resource.indexes[:day_type_ref] = resource.day_types&.map(&:ref)
        resource
      end
    end

    class JourneyPatternScheduledStopPointIndexer
      def transform(resource)
        return resource unless resource.is_a?(Netex::JourneyPattern)

        resource.indexes[:scheduled_stop_point_ref] =
          resource.points_in_sequence.map { |p| p.scheduled_stop_point_ref&.ref }

        resource
      end
    end

    # The transformer is available to avoid duplication for the same class and the same id
    #
    #   xml = '<root><StopPlace id="target"/><StopPlace id="target"/></root>'
    #   source.transformers << Netex::Transformer::Uniqueness.new
    #   source.parse(StringIO.new(xml))
    #   source.stop_places.count => 1
    #   source.stop_places => ["<#Netex::StopPlace id='target'/>"]
    #
    class Uniqueness
      def transform(resource)
        resource unless known?(resource)
      end

      def known?(resource)
        identifiers[resource.class].add?(resource.id).nil?
      end

      def identifiers
        @identifiers ||= Hash.new { |h, resource_class| h[resource_class] = Set.new }
      end
    end

    module RawContentCache
      class Base
        def normalise_key(key)
          key.to_s
        end
      end

      class File < Base
        def file
          @file ||= Tempfile.new
        end

        def contents
          @contents ||= Hash.new { |h, k| h[k] = [] }
        end

        attr_accessor :last_key, :last_content

        def store(key, entry)
          key = normalise_key(key)
          file.seek 0, :END

          tell = file.tell
          size = file.write entry

          if key == last_key
            last_content.increase size
          else
            self.last_key = key
            self.last_content = Content.new(tell, size)

            contents[key] << last_content
          end
        end

        def fetch(key)
          key = normalise_key(key)
          entry = StringIO.new

          contents[key].each do |content|
            file.seek(content.offset)
            entry.write file.read(content.size)
          end
          entry.string
        end

        def memory?
          false
        end

        class Content
          attr_reader :offset, :size

          def initialize(offset, size)
            @offset = offset
            @size = size
          end

          def increase(size)
            @size += size
          end
        end
      end

      class Memory < Base
        attr_reader :size

        def initialize
          @size = 0
        end

        def store(key, raw_content)
          key = normalise_key(key)

          increase(raw_content.size)
          contents[key] << raw_content
        end

        def fetch(key)
          key = normalise_key(key)
          contents[key] if contents.key?(key)
        end

        def contents
          @contents ||= Hash.new { |h, k| h[k] = String.new }
        end

        def memory?
          true
        end

        def increase(size)
          @size += size
        end
      end

      class Auto
        def cache(key, raw_content = nil)
          if raw_content
            store key, raw_content
          else
            fetch key
          end
        end

        def initialize(offline_threshold = 64)
          @offline_threshold = offline_threshold
        end
        attr_accessor :offline_threshold

        def internal_cache
          @internal_cache ||= Memory.new
        end

        KILO_BYTE = 1024
        def max_size
          offline_threshold * KILO_BYTE
        end

        delegate :memory?, :fetch, to: :internal_cache

        def store(key, content)
          internal_cache.store key, content
          switch
        end

        def switch
          return switch! if memory? && internal_cache.size > max_size

          internal_cache
        end

        def switch!
          file_cache = File.new

          internal_cache.contents.each_key do |key|
            file_cache.store key, internal_cache.fetch(key)
          end

          @internal_cache = file_cache
        end
      end
    end

    class QuaysWithParent < ByClass
      include Netex::Target::BuilderBook::Support
      def raw_content_cache
        @raw_content_cache ||= RawContentCache::Auto.new
      end
      delegate :cache, to: :raw_content_cache
    end

    class StandaloneQuays < QuaysWithParent
      def transform_quay(quay)
        parent_id = quay.tag(:parent_id)
        quay.parent_site_ref = Netex::Reference.new(parent_id, type: Netex::StopPlace) if parent_id
        quay
      end
    end

    class EmbeddedQuays < QuaysWithParent
      def transform_quay(quay)
        parent_id = quay.tag(:parent_id)
        return quay unless parent_id

        cache parent_id, to_xml(quay)
        nil
      end

      def transform_stop_place(stop_place)
        if (raw_content = cache(stop_place.id))
          stop_place.quays.raw_content = raw_content
        end
        stop_place
      end
    end

    class EntrancesWithParent < ByClass
      include Netex::Target::BuilderBook::Support

      def raw_content_cache
        @raw_content_cache ||= RawContentCache::Auto.new
      end
      delegate :cache, to: :raw_content_cache

      def transform_stop_place(stop_place)
        if (raw_content = cache(stop_place.id))
          stop_place.entrances.raw_content = raw_content
        end
        stop_place
      end
    end

    class StandaloneEntrances < EntrancesWithParent
      def transform_stop_place_entrance(entrance)
        parent_id = entrance.tag(:parent_id)
        cache parent_id, to_xml(Netex::Reference.new(entrance.id, type: 'StopPlaceEntranceRef')) if parent_id
        entrance
      end
    end

    class EmbeddedEntrances < EntrancesWithParent
      def transform_stop_place_entrance(entrance)
        parent_id = entrance.tag(:parent_id)
        return entrance unless parent_id

        cache parent_id, to_xml(entrance)
        nil
      end
    end

    class PassingTimesWithParent < ByClass
      include Netex::Target::BuilderBook::Support

      def raw_content_cache
        @raw_content_cache ||= RawContentCache::Auto.new
      end
      delegate :cache, to: :raw_content_cache

      def transform_service_journey(service_journey)
        if (raw_content = cache(service_journey.id))
          service_journey.passing_times.raw_content = raw_content
        end
        service_journey
      end
    end

    class EmbeddedPassingTimes < PassingTimesWithParent
      def transform_timetabled_passing_time(passing_time)
        parent_id = passing_time.tag(:parent_id)

        return passing_time unless parent_id

        cache parent_id, to_xml(passing_time)
        nil
      end
    end

    # Transform a Quay without parent into a StopPlace with quay type
    class OrphanQuaysAsStopPlace < ByClass
      def transform_quay(quay)
        return quay if quay.has_tag?(:parent_id)

        Netex.logger.debug { "Transform the orphan Quay##{quay.id} into StopPlace" }
        quay.to_stop_place.tap do |stop_place|
          stop_place.place_types = [Netex::Reference.new('quay', type: String)]
        end
      end
    end

    module LocationExtractor
      def self.location(resource)
        return resource if resource.is_a?(Netex::Location)

        return resource.location if resource.respond_to?(:location)

        return location(resource.centroid) if resource.respond_to?(:centroid)

        nil
      end
    end

    class CoordinatesFromLocation
      def initialize(target_projection:)
        @target_projection = target_projection
      end

      attr_reader :target_projection

      def transform(resource)
        update_coordinates resource
        resource
      end

      private

      def update_coordinates(resource)
        location = LocationExtractor.location(resource)
        return unless location

        # Keep existing coordinates
        return if location.coordinates

        target_coordinate = transform_location(location)
        return unless target_coordinate

        location.coordinates = GML::Pos.new(
          srs_name: target_projection,
          x: target_coordinate.x,
          y: target_coordinate.y
        )
      end

      def transform_location(location)
        return unless location.latitude && location.longitude

        position = Geo::Position.new(x: location.longitude, y: location.latitude)
        transformation.transform position
      end

      def transformation
        @transformation ||= Geo::Transformation.create('epsg:4326', target_projection)
      end
    end

    class LocationFromCoordinates
      def initialize(target_projection = 'epsg:4326')
        @target_projection = target_projection
      end

      attr_reader :target_projection

      def transform(resource)
        update_location resource
        resource
      end

      protected

      def update_location(resource)
        location = LocationExtractor.location(resource)
        # Keep existing latitude/longtitude
        return if location.nil? || location.latitude || location.longitude

        target_coordinate = transform_coordinates(location.coordinates)
        return unless target_coordinate

        location.latitude = target_coordinate.latitude
        location.longitude = target_coordinate.longitude
        location.srs_name = target_projection
      end

      def transform_coordinates(coordinates)
        return unless coordinates&.x && coordinates.y && coordinates.srs_name

        position = Geo::Position.new(x: coordinates.x, y: coordinates.y)
        transformation(coordinates.srs_name).transform position
      end

      def transformations
        @transformations ||= {}
      end

      def transformation(source_projection)
        transformations[source_projection.downcase] ||=
          Geo::Transformation.create(source_projection, target_projection)
      end
    end

    class Composite
      def transform(resource)
        transformers.each do |transformer|
          Netex.logger.debug { "#{self.class} Transform with #{transformer.inspect}: #{resource.inspect}" }

          resource = transformer.transform resource
          return if resource.nil?
        end

        resource
      end

      def transformers
        @transformers ||= []
      end

      def add(transformer)
        transformers << transformer
      end
    end
  end
end
