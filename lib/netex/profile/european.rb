module Netex
  module Profile
    class European < Base

      def initialize
        super

        self.container_type = :zip
      end

      STOPS_FILE = "stops.xml"
      COMMON_FILE = "common.xml"

      transform Netex::Transformer::OrphanQuaysAsStopPlace
      transform Netex::Transformer::EmbeddedEntrances
      transform Netex::Transformer::EmbeddedQuays
      transform Netex::Transformer::EmbeddedPassingTimes

      def document_for(resource)
        if resource.is_a? Netex::StopPlace
          return document STOPS_FILE
        end

        if line_id = resource.tag(:line_id)
          return document "line-#{line_id}.xml"
        end

        document COMMON_FILE
      end
    end
  end
end
