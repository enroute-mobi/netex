# frozen_string_literal: true

module Netex
  module Profile
    class French < Base
      VERSION = '1.09:FR-NETEX-2.1-1.0'

      def initialize
        super

        self.container_type = :zip
      end

      transform Netex::Transformer::EmbeddedEntrances
      transform Netex::Transformer::EmbeddedQuays
      transform Netex::Transformer::EmbeddedPassingTimes

      def stops_document
        @stops_document ||= Document::Stops.new(document_attributes)
      end

      def line_documents
        @line_documents ||= {}
      end

      def line_document(line_id:, line_name:)
        line_documents[line_id] ||= Document::Line.new line_id, document_attributes.merge(line_name: line_name)
      end

      def point_of_interests_document
        @point_of_interests_document ||= Document::PointOfInterests.new(document_attributes)
      end

      def resources_document
        @resources_document ||= Document::Resources.new(document_attributes)
      end

      def documents
        line_documents.values + [@resources_document, @point_of_interests_document, @stops_document].compact
      end

      def document_for(resource) # rubocop:disable Metrics/MethodLength
        case resource
        when Netex::StopPlace, Netex::Quay, Netex::StopPlaceEntrance
          return stops_document
        when Netex::PointOfInterest
          return point_of_interests_document
        when Netex::DayType, Netex::DayTypeAssignment, Netex::OperatingPeriod
          # to ignore line_id tag in this resources
          return resources_document
        when Netex::Line
          return line_document(line_id: resource.id, line_name: resource.name)
        end

        # Netex::Notice, Netex::Operator, Netex::Network

        if line_id = resource.tag(:line_id)
          return line_document(line_id: resource.tag(:line_id), line_name: resource.tag(:line_name))
        end

        resources_document
      end

      class Document < Netex::Target::Document
        def initialize(filename, attributes = {})
          super attributes.merge(filename: filename)

          publication_delivery.timestamp = publication_timestamp
          # TODO: manage a configurable ParticipantRef
          publication_delivery.participant_ref = 'enRoute'
          publication_delivery.version = VERSION
        end

        def frame_id(type_of_frame, class_frame = 'GeneralFrame')
          # FR:GeneralFrame:NETEX_ARRETS:LOC
          "FR:#{class_frame}:NETEX_#{type_of_frame}:LOC"
        end

        def general_frame(type, &block)
          Netex::Target::GeneralFrame.new.tap do |frame|
            frame.id = frame_id(type)
            frame.version = VERSION
            frame.type_of_frame_ref = "FR:TypeOfFrame:NETEX_#{type}:"

            block.call frame
          end
        end

        def setup_composite_frame(type)
          composite_frame.id = frame_id(type, 'CompositeFrame')
          composite_frame.version = VERSION
          composite_frame.type_of_frame_ref = "FR:TypeOfFrame:NETEX_#{type}:"
        end

        class Stops < Document
          FILENAME = 'STOP.xml'

          def initialize(attributes = {})
            super FILENAME, attributes

            frames << general_frame('ARRET') do |frame|
              frame.sections.create accept: Netex::StopPlace
              frame.sections.create accept: Netex::Quay
              frame.sections.create accept: Netex::StopPlaceEntrance
            end
          end
        end

        class Line < Document
          FILENAME_PREFIX = 'LINE'

          def initialize(line_id, attributes = {}) # rubocop:disable Metrics/MethodLength,Metrics/AbcSize
            line_name = attributes.delete(:line_name)

            super "#{FILENAME_PREFIX}-#{line_id}.xml", attributes

            setup_composite_frame 'LIGNE'
            composite_frame.name = line_name

            frames << general_frame('LIGNE') do |frame|
              frame.sections.create accept: Netex::Line
              frame.sections.create accept: Netex::Direction
              # frame.sections.create accept: Netex::GroupOfLine
              # frame.sections.create accept: Netex::Network ??
              frame.sections.create accept: Netex::Route
              frame.sections.create accept: Netex::RoutePoint
              frame.sections.create accept: Netex::PointOnRoute
              frame.sections.create accept: Netex::ScheduledStopPoint
              # frame.sections.create accept: Netex::RouteLink
              # frame.sections.create accept: Netex::FlexibleLine
              # frame.sections.create accept: Netex::FlexibleRoute
            end

            frames << general_frame('RESEAU') do |frame|
              # frame.sections.create accept: Netex::TariffZone ??
              frame.sections.create accept: Netex::DestinationDisplay
              # frame.sections.create accept: Netex::FlexiblePointProperties
              # frame.sections.create accept: Netex::FlexibleLinkProperties
              frame.sections.create accept: Netex::ServiceJourneyPattern
              frame.sections.create accept: Netex::StopPointInJourneyPattern
              # frame.sections.create accept: Netex::TimingPoint
              # frame.sections.create accept: Netex::Connection
              # frame.sections.create accept: Netex::DefaultConnection
              # frame.sections.create accept: Netex::SiteConnection ??
              frame.sections.create accept: Netex::RoutingConstraintZone
              # frame.sections.create accept: Netex::TransferRestriction
              frame.sections.create accept: Netex::PassengerStopAssignment
              # frame.sections.create accept: Netex::TrainStopAssignment
              # frame.sections.create accept: Netex::SchematicMap
              # TODO: no present in the profile document
              # frame.sections.create accept: Netex::ServiceLink
            end

            frames << general_frame('HORAIRE') do |frame|
              frame.sections.create accept: Netex::ServiceJourney
              # ...
              # FLEXIBLE SERVICE PROPERTIES
              # TEMPLATE SERVICE JOURNEY
              # HEADWAY JOURNEY GROUP
              # RHYTHMICAL JOURNEY GROUP
              # SERVICE JOURNEY INTERCHANGE
              # VEHICLE TYPE
              # COUPLED JOURNEY
              # JOURNEY PART COUPLE
              # JOURNEY PART
              # TRAIN
              # TRAIN COMPONENT
              # COMPOUND TRAIN
              # TRAIN NUMBER
              # TRAIN COMPONENT LABEL ASSIGNMENT
              # COUPLED JOURNEY
              # JOURNEY PART COUPLE
              # JOURNEY PART
              # TRAIN
              # TRAIN COMPONENT
              # COMPOUND TRAIN
              # TRAIN NUMBER
              # TRAIN COMPONENT LABEL ASSIGNMENT
            end
          end
        end

        class Resources < Document
          FILENAME = 'RESOURCE.xml'

          def initialize(attributes = {}) # rubocop:disable Metrics/MethodLength,Metrics/AbcSize
            super FILENAME, attributes

            setup_composite_frame 'FRANCE'

            frames << general_frame('COMMUN') do |frame|
              # frame.sections.create accept: Netex::AlternativeName
              frame.sections.create accept: Netex::Notice
              frame.sections.create accept: Netex::NoticeAssignment
              # frame.sections.create accept: Netex::ResponsibleRoleAssignment
              frame.sections.create accept: Netex::Organisation
              frame.sections.create accept: Netex::Operator
              frame.sections.create accept: Netex::PointProjection
              # frame.sections.create accept: Netex::ZoneProjection
            end

            frames << general_frame('CALENDRIER') do |frame|
              frame.sections.create accept: Netex::DayType
              frame.sections.create accept: Netex::OperatingPeriod, accept_subclasses: true
              # frame.sections.create accept: Netex::ServiceCalendar
              frame.sections.create accept: Netex::DayTypeAssignment
            end
          end
        end

        class PointOfInterests < Document
          FILENAME = 'POI.xml'

          def initialize(attributes = {})
            super FILENAME, attributes

            # TODO: fix POI file structure
            frames << general_frame('COMMUN') do |frame|
              frame.sections.create accept: Netex::PointOfInterest
            end
          end
        end
      end
    end
  end
end
