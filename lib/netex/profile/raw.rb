module Netex
  module Profile
    class Raw
      attr_accessor :participant_ref, :publication_timestamp, :validity_periods
      def document_attributes
        { participant_ref: participant_ref,
          publication_timestamp: publication_timestamp }
      end

      def document
        @document ||= Document.new(document_attributes)
      end

      class Document < Netex::Target::Document
        def initialize(attributes = {})
          super attributes

          publication_delivery.timestamp = publication_timestamp
          publication_delivery.participant_ref = 'enRoute'

          frames << general_frame
        end

        def general_frame
          Netex::Target::GeneralFrame.new.tap do |frame|
            frame.id = 'test'
            frame.sections << FreeSection.new

            # frame.version = VERSION
            # frame.type_of_frame_ref = "FR:TypeOfFrame:NETEX_#{type}:"
          end
        end
      end

      class FreeSection < Netex::Target::Section

      end

      # Transform with defined transformers
      include Netex::Transformer::Support

      transform Netex::Transformer::EmbeddedEntrances
      transform Netex::Transformer::EmbeddedQuays
      transform Netex::Transformer::EmbeddedPassingTimes

      attr_writer :builder_book
      def builder_book
        @builder_book ||= Netex::Target::BuilderBook.new
      end

      def document_for(_)
        document
      end

      def documents
        @documents ||= [document]
      end

      def create_container(output)
        Netex::Target::Container::XML.new output
      end
    end
  end
end
