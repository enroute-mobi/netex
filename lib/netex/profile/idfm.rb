# frozen_string_literal: true

module Netex
  module Profile
    module IDFM
      VERSION = '1.8'

      class Base < Netex::Profile::Base
        transform Netex::Transformer::EmbeddedPassingTimes

        def initialize
          super
          self.container_type = :zip
          self.builder_book = BuilderBook.new
        end

        def transform(resource)
          resource = super(resource)

          case resource
          when Netex::DayType
            SingleDayPropertyOfDayTranformer.new(resource).transform
          when Netex::ServiceJourney
            PublicCodeTransformer.new(resource).transform
          when Netex::GeneralOrganisation
            Netex::OrganisationalUnit.new(id: "FR1:OrganisationalUnit:#{resource.id}",
                                          name: resource.name,
                                          type_of_organisation_part_ref: Netex::Reference.new('FR1_Organisation',
                                                                                              type: String))
          when Netex::UicOperatingPeriod
            OperatingPeriodTransformer.new(resource).transform
          else
            resource
          end
        end

        class PublicCodeTransformer
          def initialize(service_journey)
            @service_journey = service_journey
          end
          attr_reader :service_journey

          def transform
            if candidate?
              service_journey.train_numbers << train_number_ref
              service_journey.public_code = nil
            end

            service_journey
          end

          def public_code
            service_journey.public_code
          end

          def candidate?
            public_code && !public_code.empty?
          end

          def netex_identifier
            @netex_identifier ||= Netex::ObjectId.parse(service_journey.id)
          end

          def train_number
            # objectid 'SNCF:ServiceJourney:31891-C01849-b96f67e2-d316-4686-b130-a16a4db03a42:LOC'
            # with public_code '8159244'
            # becomes:
            # 'SNCF:TrainNumber:8159244:LOC'
            if netex_identifier
              netex_identifier.change(type: 'TrainNumber', technical: public_code).to_s
            else
              public_code
            end
          end

          def train_number_ref
            Netex::Reference.new(train_number, type: String)
          end
        end

        class OperatingPeriodTransformer
          def initialize(operating_period)
            @operating_period = operating_period
          end
          attr_reader :operating_period

          def transform
            Netex::OperatingPeriod.new(
              id: operating_period.id,
              version: operating_period.version,
              data_source_ref: operating_period.data_source_ref,
              from_date: operating_period.from_date,
              to_date: operating_period.to_date,
              tags: operating_period.tags
            )
          end
        end

        # Rewrite DayType PropertyOfDays to define only a day per PropertyOfDays.
        # Remove the DayType#properties when no day of week is defined.
        #
        # See NETEX-20
        class SingleDayPropertyOfDayTranformer
          def initialize(day_type)
            @day_type = day_type
          end
          attr_accessor :day_type

          def single_property_of_day
            day_type.properties.first if day_type&.properties&.one?
          end

          def candidate?
            single_property_of_day && days_of_week.length != 1
          end

          def days_of_week
            if single_property_of_day.days_of_week
              single_property_of_day.days_of_week.split
            else
              []
            end
          end

          def properties
            return if days_of_week.empty?

            days_of_week.map do |day_of_week|
              Netex::PropertyOfDay.new(days_of_week: day_of_week)
            end
          end

          def transform
            day_type.properties = properties if candidate?
            day_type
          end
        end
      end

      class BuilderBook < Netex::Target::BuilderBook
        def builder(resource, options = {})
          case resource
          when Netex::Reference
            BuilderReference.new(resource, options)
          when Netex::OperatingPeriod
            BuilderOperatingPeriod.new(resource, options)
          else
            super
          end
        end

        class BuilderReference < Netex::Target::Builder::Reference
          def build_version
            # For example, TypeOfOrganisationPartRef is a String where version has no mean

            return if resource.type == String && resource.version.nil?

            if resource.local?
              xml_element[:version] = resource.version_or_any
            else
              xml_element << "version=\"#{resource.version_or_any}\""
            end
          end
        end

        class BuilderOperatingPeriod < Netex::Target::Builder::OperatingPeriod
          def build_to_date
            # Skip default logic of +1 on Date, see NETEX-131
            add_date_element 'ToDate', resource.to_date
          end
        end
      end
    end
  end
end
