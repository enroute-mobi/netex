# frozen_string_literal: true

# lib/full.rb

module Netex
  module Profile
    module IDFM
      class Full < Base
        transform Netex::Transformer::StandaloneEntrances
        transform Netex::Transformer::StandaloneQuays

        def document_for(resource)
          case resource
          when Netex::Notice, Netex::Operator, Netex::Line, Netex::Network
            lines_document
          when Netex::StopPlace, Netex::Quay, Netex::StopPlaceEntrance
            stops_document
          else
            if resource.has_tag?(:operator_id)
              operator_folder(operator_id: resource.tag(:operator_id),
                              operator_name: resource.tag(:operator_name)).document_for(resource)
            else
              # "TimeTables" not associated to an operator are exported in the root folder
              case resource
              when Netex::DayType, Netex::DayTypeAssignment, Netex::OperatingPeriod
                calendar_document
              end
            end
          end
        end

        def operator_folders
          @operator_folders ||= {}
        end

        def operator_folder(operator_id:, operator_name:)
          operator_folders[operator_id] ||=
            OperatorFolder.new(
              operator_id: operator_id,
              operator_name: operator_name,
              validity_periods: validity_periods,
              document_attributes: document_attributes
            )
        end

        def lines_document
          @lines_document ||= Document::Lines.new(document_attributes)
        end

        def stops_document
          @stops_document ||= Document::Stops.new(document_attributes)
        end

        def calendar_document
          @calendar_document ||= Document::Calendar.new(document_attributes)
        end

        def documents
          operator_folders.values.flat_map(&:documents).push(@lines_document, @stops_document,
                                                             @calendar_document).compact
        end

        class OperatorFolder
          def initialize(operator_id:, operator_name:, validity_periods: nil, document_attributes: nil)
            @id = operator_id
            @name = operator_name
            @base_document_attributes = document_attributes || {}
            @validity_periods = validity_periods
          end
          attr_reader :id, :name, :base_document_attributes
          attr_accessor :validity_periods

          def folder_name
            @folder_name ||= "OFFRE_#{Netex::ObjectId.technical(id)}_#{normalized_name}"
          end

          SUPPORTED_CHARACTERS =
            def normalized_name
              name.gsub(/[^a-z0-9_-]/i, '_')
            end

          def document_attributes
            @document_attributes ||= base_document_attributes.merge(folder: folder_name)
          end

          def document_for(resource)
            case resource
            when Netex::DayType, Netex::DayTypeAssignment, Netex::OperatingPeriod
              calendar_document
            when Netex::OrganisationalUnit # , Netex::ServiceJourneyNotice
              common_document
            else
              if resource.has_tag?(:line_id)
                offre_document line_id: resource.tag(:line_id), line_name: resource.tag(:line_name)
              end
            end
          end

          def calendar_document
            @calendar_document ||= Document::Calendar.new(document_attributes.merge!(validity_periods: validity_periods))
          end

          def common_document
            @common_document ||= Document::Common.new(document_attributes)
          end

          def offre_documents
            @offre_documents ||= {}
          end

          def offre_document(line_id:, line_name:)
            offre_documents[line_id] ||= Document::Offre.new document_attributes.merge(line_id: line_id,
                                                                                       line_name: line_name)
          end

          def documents
            offre_documents.values + [@calendar_document, @common_document].compact
          end
        end

        class Document < Netex::Target::Document
          def initialize(filename, attributes = {})
            super attributes.merge(filename: self.class.path(filename, attributes))

            publication_delivery.timestamp = publication_timestamp
            publication_delivery.participant_ref = 'FR1_OFFRE'
            publication_delivery.version = '1.04:FR1-NETEX-1.6-1.8'
          end

          def self.path(filename, attributes = {})
            if (folder = attributes.delete(:folder))
              "#{folder}/#{filename}"
            else
              filename
            end
          end

          def frame_id(class_frame, type_of_frame)
            # FR1:CompositeFrame:NETEX_IDF-20201126T132621Z:LOC
            # FR1:GeneralFrame:NETEX_STRUCTURE-20201126T132620Z:LOC
            "FR1:#{class_frame}:NETEX_#{type_of_frame}-#{publication_timestamp.utc.strftime('%Y%m%dT%H%M%SZ')}:LOC"
          end

          def general_frame(type, &block)
            Netex::Target::GeneralFrame.new.tap do |frame|
              frame.id = frame_id('GeneralFrame', type)
              frame.version = VERSION
              frame.data_source_ref = 'FR1-OFFRE_AUTO'
              frame.type_of_frame_ref = "FR1:TypeOfFrame:NETEX_#{type}:"

              block.call frame
            end
          end

          def setup_composite_frame(type)
            composite_frame.id = frame_id('CompositeFrame', type)
            composite_frame.version = VERSION
            composite_frame.data_source_ref = 'FR1-OFFRE_AUTO'
            composite_frame.type_of_frame_ref = "FR1:TypeOfFrame:NETEX_#{type}:"
          end

          class Calendar < Document
            FILENAME = 'calendriers.xml'

            attr_accessor :validity_periods

            def initialize(attributes = {})
              super FILENAME, attributes

              frames << general_frame('CALENDRIER') do |frame|
                frame.validity_periods = validity_periods

                frame.sections.create accept: Netex::DayType
                frame.sections.create accept: Netex::DayTypeAssignment
                frame.sections.create accept: Netex::OperatingPeriod
              end
            end
          end

          class Line < Document
            FILENAME = 'ligne.xml'

            def initialize(attributes = {})
              super FILENAME, attributes

              setup_composite_frame 'IDF'

              frames << general_frame('COMMUN') do |frame|
                frame.sections.create accept: Netex::Notice
                frame.sections.create accept: Netex::Operator
              end

              frames << general_frame('LIGNE') do |frame|
                frame.sections.create accept: Netex::Line
                frame.sections.create accept: Netex::Network
              end
            end
          end

          class Lines < Document
            FILENAME = 'lignes.xml'

            def initialize(attributes = {})
              super FILENAME, attributes

              setup_composite_frame 'IDF'

              frames << general_frame('COMMUN') do |frame|
                frame.sections.create accept: Netex::Notice
                frame.sections.create accept: Netex::Operator
              end

              frames << general_frame('LIGNE') do |frame|
                frame.sections.create accept: Netex::Line
                frame.sections.create accept: Netex::Network
              end
            end
          end

          class Stops < Document
            FILENAME = 'arrets.xml'

            def initialize(attributes = {})
              super FILENAME, attributes

              publication_delivery.xml_namespaces['gml'] = 'http://www.opengis.net/gml/3.2'

              setup_composite_frame 'IDF'

              frames << general_frame('ARRET_IDF') do |frame|
                frame.sections.create accept: Netex::StopPlace
                frame.sections.create accept: Netex::Quay
                frame.sections.create accept: Netex::StopPlaceEntrance
              end
            end
          end

          class Common < Document
            FILENAME = 'commun.xml'

            def initialize(attributes = {})
              super FILENAME, attributes

              frames << general_frame('COMMUN') do |frame|
                # TODO
                # frame.sections.create accept: Netex::ServiceJourneyNotice
                frame.sections.create accept: Netex::OrganisationalUnit
              end
            end
          end

          class Offre < Document
            def initialize(attributes = {})
              line_id = attributes.delete(:line_id)
              line_name = attributes.delete(:line_name)

              super self.class.filename(line_id: line_id, line_name: line_name), attributes

              setup_composite_frame 'OFFRE_LIGNE'
              composite_frame.name = line_name

              frames << general_frame('STRUCTURE') do |frame|
                frame.sections.create accept: Netex::Route
                frame.sections.create accept: Netex::Direction
                frame.sections.create accept: Netex::RoutePoint
                frame.sections.create accept: Netex::ServiceJourneyPattern
                frame.sections.create accept: Netex::DestinationDisplay
                frame.sections.create accept: Netex::ScheduledStopPoint
                frame.sections.create accept: Netex::PassengerStopAssignment
              end

              frames << general_frame('HORAIRE') do |frame|
                frame.sections.create accept: Netex::ServiceJourney
                frame.sections.create accept: Netex::VehicleJourneyStopAssignment
              end
            end

            def self.parametrize(name)
              name&.gsub(/[^A-Za-z0-9]/, '_')&.gsub(/__+/, '_')
            end

            def self.filename(line_id:, line_name:)
              line_id = Netex::ObjectId.technical(line_id)
              line_name = parametrize(line_name)
              "#{['offre', line_id, line_name].join('_')}.xml"
            end
          end
        end
      end
    end
  end
end
