# frozen_string_literal: true

module Netex
  module Profile
    module IDFM
      class IBOO < Base
        VERSION = '1.04:FR1-NETEX-2.0-0'

        def transform(resource)
          case resource
          when Netex::JourneyPattern
            resource.service_journey_pattern_type = 'passenger'
            super resource
          when Netex::Line, Netex::StopPlace, Netex::Quay
            nil
          else
            super resource
          end
        end

        def document_for(resource)
          case resource
          when Netex::DayType, Netex::DayTypeAssignment, Netex::OperatingPeriod
            calendar_document
          else
            if resource.has_tag?(:line_id)
              offre_document line_id: resource.tag(:line_id), line_name: resource.tag(:line_name)
            end
          end
        end

        attr_writer :code_space

        def code_space
          @code_space ||= 'CODE_SPACE'
        end

        def document_attributes
          super.merge(code_space: code_space)
        end

        def calendar_document
          @calendar_document ||=
            Document::Calendar.new(document_attributes.merge(validity_periods: validity_periods))
        end

        def offre_document(line_id:, line_name:)
          offre_documents[line_id] ||=
            Document::Offre.new document_attributes.merge(line_id: line_id, line_name: line_name)
        end

        def offre_documents
          @offre_documents ||= {}
        end

        def documents
          offre_documents.values + [@calendar_document].compact
        end

        class Document < Netex::Target::Document
          def initialize(filename, attributes = {})
            super attributes.merge(filename: self.class.path(filename, attributes))

            publication_delivery.timestamp = publication_timestamp
            publication_delivery.participant_ref = participant_ref
            publication_delivery.version = VERSION
          end

          def self.path(filename, attributes = {})
            publication_timestamp = attributes[:publication_timestamp]
            participant_ref = attributes[:participant_ref]

            "OFFRE_#{participant_ref}_#{publication_timestamp.utc.strftime('%Y%m%d%H%M%S')}Z/#{filename}"
          end

          attr_accessor :code_space

          def frame_id(class_frame, type_of_frame)
            # FR1:CompositeFrame:NETEX_IDF-20201126T132621Z:LOC
            # FR1:GeneralFrame:NETEX_STRUCTURE-20201126T132620Z:LOC
            "#{code_space}:#{class_frame}:NETEX_#{type_of_frame}-#{publication_timestamp.utc.strftime('%Y%m%dT%H%M%SZ')}:LOC"
          end

          def general_frame(type, &block)
            Netex::Target::GeneralFrame.new.tap do |frame|
              frame.id = frame_id('GeneralFrame', type)
              frame.type_of_frame_ref = Netex::Reference.new(
                "FR1:TypeOfFrame:NETEX_#{type}:",
                type: String,
                version: "1.04:FR1-NETEX_#{type}-2.1",
                local: false
              )

              block.call frame
            end
          end

          class Calendar < Document
            FILENAME = 'calendriers.xml'

            attr_accessor :validity_periods

            def initialize(attributes = {})
              super FILENAME, attributes

              frames << general_frame('CALENDRIER') do |frame|
                frame.validity_periods = validity_periods

                frame.sections.create accept: Netex::DayType
                frame.sections.create accept: Netex::DayTypeAssignment
                frame.sections.create accept: Netex::OperatingPeriod
              end
            end
          end

          class Offre < Document
            def initialize(attributes = {})
              line_id = attributes.delete(:line_id)
              line_name = attributes.delete(:line_name)

              super self.class.filename(line_id: line_id, line_name: line_name), attributes

              setup_composite_frame line_id: line_id
              composite_frame.name = line_name

              frames << general_frame('STRUCTURE') do |frame|
                frame.sections.create accept: Netex::Route
                frame.sections.create accept: Netex::Direction
                frame.sections.create accept: Netex::RoutePoint
                frame.sections.create accept: Netex::ServiceJourneyPattern
                frame.sections.create accept: Netex::DestinationDisplay
                frame.sections.create accept: Netex::ScheduledStopPoint
                frame.sections.create accept: Netex::PassengerStopAssignment
                frame.sections.create accept: Netex::RoutingConstraintZone
              end

              frames << general_frame('HORAIRE') do |frame|
                frame.sections.create accept: Netex::ServiceJourney
                frame.sections.create accept: Netex::VehicleJourneyStopAssignment
              end
            end

            def setup_composite_frame(line_id:)
              line_id = Netex::ObjectId.technical(line_id)

              composite_frame.id = "#{code_space}:CompositeFrame:NETEX_OFFRE_LIGNE-#{line_id}:LOC"
              composite_frame.type_of_frame_ref = Netex::Reference.new(
                'FR1:TypeOfFrame:NETEX_OFFRE_LIGNE:',
                type: String,
                version: "1.04:FR1-NETEX_OFFRE_LIGNE-2.1",
                local: false
              )
            end

            def self.parametrize(name)
              name&.gsub(/[^A-Za-z0-9]/, '_')&.gsub(/__+/, '_')
            end

            def self.filename(line_id:, line_name:)
              line_id = Netex::ObjectId.technical(line_id)
              line_name = parametrize(line_name)
              "#{['offre', line_id, line_name].join('_')}.xml"
            end
          end
        end
      end
    end
  end
end
