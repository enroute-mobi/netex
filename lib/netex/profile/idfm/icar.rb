# frozen_string_literal: true

module Netex
  module Profile
    module IDFM
      class ICAR < Base
        VERSION = '1.04:FR1-NETEX-2.0'

        def transform(resource)
          return nil unless resource.is_a?(Netex::Quay)

          super
        end

        transform Netex::Transformer::CoordinatesFromLocation, target_projection: 'EPSG:27572'

        class FilterKeyList
          SUPPORTED_KEYS = %w[ACS_ID ACS_NAME].freeze
          SUPPORTED_TYPE_OF_KEYS = %w[ALTERNATE_IDENTIFIER ALTERNATE_NAME].freeze

          def transform(resource)
            resource.key_list.select! do |key_value|
              SUPPORTED_TYPE_OF_KEYS.include?(key_value.type_of_key) &&
                SUPPORTED_KEYS.include?(key_value.key)
            end

            resource
          end
        end

        class FilterPostalAddress
          BLANK_ATTRIBUTES = %i[post_code town country_name].freeze

          def transform(resource)
            if resource.postal_address
              BLANK_ATTRIBUTES.each do |attribute|
                resource.postal_address.send "#{attribute}=", nil
              end
            end

            resource
          end
        end

        class FilterAccessibilityAssessment
          BLANK_ATTRIBUTES = %i[step_free_access escalator_free_access lift_free_access].freeze

          def transform(resource)
            if resource.accessibility_assessment&.limitation
              BLANK_ATTRIBUTES.each do |attribute|
                resource.accessibility_assessment.limitation.send "#{attribute}=", nil
              end
            end

            resource
          end
        end

        transform FilterKeyList
        transform FilterPostalAddress
        transform FilterAccessibilityAssessment

        def document_for(resource)
          case resource
          when Netex::Quay
            stops_document
          end
        end

        attr_writer :site_id, :site_name, :code_space

        def site_id
          @site_id ||= 'SITE_ID'
        end

        def site_name
          @site_name ||= 'SITE_NAME'
        end

        def code_space
          @code_space ||= 'CODE_SPACE'
        end

        def file_type
          @file_type ||= :total
        end

        def document_attributes
          super.merge(site_id: site_id, site_name: site_name,
                      file_type: file_type, code_space: code_space)
        end

        def stops_document
          @stops_document ||= Document::Stops.new(document_attributes)
        end

        def documents
          [@stops_document].compact
        end

        class Document < Netex::Target::Document
          def initialize(attributes = {})
            super attributes.merge(filename: self.class.path(attributes))

            publication_delivery.timestamp = publication_timestamp
            publication_delivery.participant_ref = participant_ref
            publication_delivery.version = VERSION
          end

          attr_accessor :code_space, :file_type, :site_id, :site_name

          def frame_id
            "#{code_space}:GeneralFrame:NETEX_ARRET_IDF_#{publication_timestamp.utc.strftime('%Y%m%dT%H%M%S')}Z:LOC"
          end

          MODIFICATIONS = { partial: 'delta', total: 'revise' }.freeze

          def general_frame(&block)
            Netex::Target::GeneralFrame.new.tap do |frame|
              frame.id = frame_id
              frame.type_of_frame_ref = Netex::Reference.new(
                'FR1:TypeOfFrame:NETEX_ARRET', type: String,
                                              version: '1.04:FR1-NETEX_ARRET_IDF-2.1', local: false
              )
              # TODO: use file_type
              frame.modification = MODIFICATIONS[file_type.to_sym]

              block.call frame if block_given?
            end
          end

          class Stops < Document
            def self.path(attributes = {})
              publication_timestamp = attributes[:publication_timestamp]
              site_id = attributes[:site_id]
              site_name = attributes[:site_name]
              file_type = attributes[:file_type]
              file_timestamp = publication_timestamp.utc.strftime('%Y%m%dT%H%M%S')

              "ARRET_#{site_id}_#{site_name}_#{file_type.upcase[0]}_#{file_timestamp}Z.xml"
            end

            def initialize(attributes = {})
              super attributes

              publication_delivery.xml_namespaces['gml'] = 'http://www.opengis.net/gml/3.2'

              frames << general_frame do |frame|
                frame.sections.create accept: Netex::Quay
              end
            end
          end
        end
      end
    end
  end
end
