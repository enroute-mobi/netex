module Netex

  class Resource

    def initialize(attributes = {}, tags = {})
      attributes.each { |k,v| send "#{k}=", v }
      self.tags.merge!(tags)
    end

    def indexes
      @indexes ||= {}
    end

    def self.short_name
      @short_name ||= name.gsub("Netex::","").freeze
    end

    def self.name_of_class
      @resource_name ||= short_name.gsub(/([a-z]+)([A-Z])/, '\1_\2').downcase
    end
    def name_of_class
      self.class.name_of_class
    end

    @@resource_class_by_name = Netex::Cache::WithNil.new
    def self.for(name)
      @@resource_class_by_name.fetch(name) do
        resource_class(name)
      end
    end

    # Don't use this method directly but Netex::Resource.for to cache resolved classes
    def self.resource_class(name)
      candidate_class = Object.const_get("Netex::#{name}")
      return nil unless candidate_class < Netex::Resource
      candidate_class
    rescue NameError
      nil
    end

    attr_accessor :raw_xml

    def tags
      @tags ||= {}
    end
    attr_writer :tags

    def tag(name)
      tags[name.to_sym]
    end

    def has_tag?(name, value = nil)
      name = name.to_sym
      if value.nil?
        !tags[name].nil?
      else
        tags[name] == value
      end
    end

    def with_tag(options={})
      options.each do |tag, value|
        tags[tag] = value unless value.nil?
      end

      self
    end

    def to_reference
      Netex::Reference.new(id, type: self.class)
    end

    EMPTY_IGNORED_ATTRIBUTES = %i[@tags @id].freeze
    def empty?
      # TODO use Contents
      instance_variables.all? do |attribute|
        value = !EMPTY_IGNORED_ATTRIBUTES.include?(attribute) ? instance_variable_get(attribute) : nil
        value.nil?
      end
    end

    def self.super_contents
      self.superclass.respond_to?(:contents) ? self.superclass.contents : []
    end

    def self.contents
      @contents ||= super_contents.dup
    end

    def self.attribute(name, type: String)
      define_content name, type: type, content_type: :attribute
    end

    def self.element(name, type: String)
      define_content name, type: type, content_type: :element
    end

    def self.collection(name, type: String)
      define_content name, type: type, content_type: :collection
    end

    def self.reference(name, type:)
      define_content name, type: type, content_type: :reference
    end

    def element(name)
      self.class.contents.find { |c| c.name == name && c.content_type != :attribute }
    end

    def reference(name)
      self.class.contents.find { |c| c.reference? && c.name == name }
    end

    def self.reference_contents
      @reference_contents ||= contents.select(&:reference?)
    end

    def references
      return [ self ] if is_a?(Reference)

      self.class.contents.map do |content|
        if content.collection?
          (content.value(self) || []).map(&:references)
        elsif content.reference?
          content.value self
        end
      end.flatten.compact
    end

    # Returns resources which should be managed as embedded
    # Example: Quays and StopPlaceEntrances in a StopPlace
    def embedded_resources
      []
    end

    # Returns References which should be managed as embedded
    # Example: StopPlaceEntranceRefs in a StopPlace
    def embedded_references
      []
    end

    # Returns true if the Reference to this resource should be managed like an embedded_references
    def embedded_referencable?
      false
    end

    def referencables
      [ self ]
    end

    def self.define_content(name, options = {})
      if contents.find { |c| c.name == name }
        raise ArgumentError, "Content already defined: #{name}"
      end

      content = Content.new(name, options)

      attr_writer name

      if options[:type] == ::Time
        define_method name do
          parse_content name do |value|
            ::Time.parse value
          end
        end
      elsif options[:type] == ::Date
        define_method name do
          parse_content name do |value|
            ::Date.parse value
          end
        end
      elsif options[:type] == ::Float
        define_method name do
          parse_content name do |value|
            value.to_f unless ["", nil].include?(value)
          end
        end
      elsif options[:type] == TimeOfDay
        define_method name do
          parse_content name do |value|
            TimeOfDay.parse value unless ["", nil].include?(value)
          end
        end
      elsif options[:content_type] == :collection
        define_method name do
          variable = "@#{name}"
          collection = instance_variable_get variable
          unless collection
            collection ||= Collection.new
            instance_variable_set variable, collection
          end
          collection
        end
      else
        attr_reader name
      end

      contents << content
    end

    # stop_place = Netex::StopPlace.new
    # stop_place.entrances.raw_context = "<StopPlaceEntrance ref=''/>"
    class Collection < Array
      attr_accessor :raw_content

      def empty?
        raw_content.nil? && super
      end
    end

    protected

    def parse_content(name)
      value = instance_variable_get("@#{name}")
      if value.is_a?(String)
        value = yield value
        instance_variable_set "@#{name}", value
      end
      value
    end

  end

  module Versionable
    def self.included(klass)
      klass.attribute :version
    end

    ANY = "any"
    def version_or_any
      version || ANY
    end

  end

  class Entity < Resource
    attribute :id
    attribute :created
    attribute :changed

    def created
      @created = ::Time.parse(@created) if @created.is_a?(String)

      @created
    end

    def changed
      @changed = ::Time.parse(@changed) if @changed.is_a?(String)

      @changed
    end
  end

  class VersionedChild < Entity
    include Versionable
  end

  class EntityInVersion < Entity
    attribute :data_source_ref
    attribute :modification

    include Versionable

    attribute :status
    element :valid_between, type: 'ValidBetween'
    attribute :derived_from_object_ref
    attribute :responsibility_set_ref

    collection :validity_conditions, type: Content.new(:availability_condition, type: 'AvailabilityCondition')
  end

  class KeyValue < Resource
    attribute :type_of_key
    element :key
    element :value

    def empty?
      value.nil? || value.empty?
    end

    def ==(other)
      other.is_a?(KeyValue) && key == other.key &&
        value == other.value &&
        type_of_key == other.type_of_key
    end
  end

  module KeyValueSupport
    def self.included(klass)
      klass.collection :key_list, type: Content.new(:key_value, type: KeyValue)
    end
  end

  class DataManagedObject < EntityInVersion
    include KeyValueSupport
    # TODO BrandingRef
  end

  class AvailabilityCondition < DataManagedObject
    collection :day_types, type: Content.new(:day_type, type: 'DayType')
    collection :timebands, type: Content.new(:timeband, type: 'Timeband')
    element :description
  end

  class ValidBetween < DataManagedObject
    element :from_date, type: ::Time
    element :to_date, type: ::Time
  end

  class Reference < Resource
    attribute :ref
    attribute :version_ref

    # Over matters to write version attribute after ref
    include Versionable

    # example: Operator, Notice, etc
    attr_accessor :type

    def local?
      @local
    end

    def local!
      @local = true
    end

    def initialize(ref = nil, type:, version: nil, local: false)
      super()

      @type =
        if type.is_a?(Class)
          type
        else
          # TODO The reference.type contains LineRef instead of Line ..
          Netex::Resource.for type.gsub(/Ref$/,"")
        end
      @ref, @version, @local = ref, version, local
    end

    def eql?(other)
      if other.respond_to?(:ref)
        ref == other.ref && type == other.type && version_or_any == other.version_or_any
      end
    end
    alias == eql?

    def hash
      [ref, type, version_or_any].hash
    end
  end

  class GroupOfEntity < DataManagedObject
    element :name
    element :short_name
    element :description
    element :public_code
    element :private_code
  end

  class Presentation < Resource
    element :colour
    element :colour_name
    element :text_colour

    attr_writer :version
  end

  class Notice < DataManagedObject
    element :name
    element :text
    element :public_code

    def empty?
      name.nil? && text.nil?
    end

    # example: LineNotice, ...
    # xml : <TypeOfNoticeRef ref="LineNotice" />
    reference :type_of_notice_ref, type: String

    def type_of_notice
      return type_of_notice_ref if type_of_notice_ref.is_a?(String)

      type_of_notice_ref&.ref
    end
  end

  class NoticeAssignment < DataManagedObject
    attribute :order
    reference :notice_ref, type: Notice

    # TODO NoticeAssignmentViews
    element :notice, type: Notice
  end

  class ContactDetails < Entity
    element :contact_person
    element :email
    element :phone
    element :url
    element :further_details
  end

  module OrganisationAttributes
    def self.included(klass)
      klass.element :public_code
      klass.element :name
      klass.element :description
      klass.element :contact_details, type: ContactDetails
    end
  end

  class Organisation < DataManagedObject
    include OrganisationAttributes
  end

  class OperatingOrganisationView < Resource
    include OrganisationAttributes
  end

  class Operator < Organisation
    element :address, type: 'PostalAddress'
  end

  module PointOfInterestClassificationAttributes
    def self.included(klass)
      klass.element :name
    end
  end

  class PointOfInterestClassification < DataManagedObject
    include PointOfInterestClassificationAttributes
  end

  class PointOfInterestClassificationView < Resource
    include PointOfInterestClassificationAttributes
  end

  class GroupOfLine < GroupOfEntity
  end

  class Network < GroupOfLine
  end

  class AccessibilityLimitation < EntityInVersion
    element :wheelchair_access
    element :step_free_access
    element :escalator_free_access
    element :lift_free_access
    element :audible_signals_available
    element :visual_signs_available
  end

  class AccessibilityAssessment < EntityInVersion
    element :mobility_impaired_access
    collection :limitations, type: Content.new(:accessibility_limitation, type: 'AccessibilityLimitation')

    def limitation
      limitations.first
    end

    def description
      validity_conditions.first&.description
    end

    delegate :wheelchair_access, :step_free_access,
             :escalator_free_access, :lift_free_access,
             :audible_signals_available, :visual_signs_available,
             to: :limitation, allow_nil: true
  end

  class Line < DataManagedObject
    element :name

    element :short_name
    element :description
    element :transport_mode
    element :transport_submode

    element :url

    element :public_code
    element :private_code

    reference :operator_ref, type: 'Operator'
    collection :additional_operators, type: Content.new(:operator_ref, type: 'Operator', content_type: :reference)

    reference :represented_by_group_ref, type: GroupOfLine
    reference :type_of_line_ref, type: String

    collection :notice_assignments, type: Content.new(:notice_assignment, type: NoticeAssignment)

    element :presentation, type: Presentation
    element :alternative_presentation, type: Presentation

    element :accessibility_assessment, type: AccessibilityAssessment

    collection :routes, type: Content.new(:route_ref, type: 'Route', content_type: :reference)
  end

  class OrganisationalUnit < DataManagedObject
    element :name
    reference :type_of_organisation_part_ref, type: String
  end

  class GeneralOrganisation < Organisation
  end

  class Location < Entity
    include KeyValueSupport # Comes from DataManagedObject, but Location can't inherit from EntityInVersion
    element :longitude, type: Float
    element :latitude, type: Float
    element :srs_name

    # expected a GML:Pos instance
    element :coordinates
  end

  class Point < DataManagedObject
    element :name
    element :location, type: Location
    collection :projections, type: Content.new(:point_projection, type: 'PointProjection')

    def latitude
      location&.latitude
    end
    def longitude
      location&.longitude
    end

    def latitude=(latitude)
      default_location.latitude=latitude
    end
    def longitude=(longitude)
      default_location.longitude=longitude
    end

    private

    def default_location
      self.location ||= Location.new
    end
  end

  class PointProjection < VersionedChild
    reference :project_to_point_ref, type: Point
  end

  class Place < DataManagedObject
    element :name

    # May be at the wrong place in the hiearchy.
    # But ensure order between name, short_name and description
    element :short_name

    element :description

    element :public_code
    element :private_code

    # Point
    element :centroid, type: Point
    # example: quay, monomodalStopPlace, ...
    collection :place_types, type: Content.new(:type_of_place_ref, type: String, content_type: :reference)

    def type_of_place
      place_types&.first&.ref
    end

    reference :parent_zone_ref, type: Place

    def latitude
      centroid&.latitude
    end
    def longitude
      centroid&.longitude
    end

    def latitude=(latitude)
      default_centroid.latitude = latitude
    end
    def longitude=(longitude)
      default_centroid.longitude = longitude
    end

    private

    def default_centroid
      self.centroid ||= Point.new
    end

  end

  class AddressablePlace < Place
    element :url
    element :postal_address, type: 'PostalAddress'
  end

  class PostalAddress < Place
    element :country_name
    element :house_number
    element :address_line_1
    element :address_line_2
    element :street
    element :town
    element :post_code
    element :post_code_extension
    element :postal_region
  end

  class Site < AddressablePlace
    reference :parent_site_ref, type: Site
    collection :entrances, type: Content.new(:stop_place_entrance_ref, type: 'StopPlaceEntrance', content_type: :reference)

    def embedded_resources
      super + entrances.reject { |entrance| entrance.is_a?(Netex::Reference) }
    end

    def embedded_references
      super + entrances.select { |entrance| entrance.is_a?(Netex::Reference) }
    end
    
    # TODO: provide accessibility assessment support via a module
    element :accessibility_assessment, type: AccessibilityAssessment

    def accessibility_limitation
      accessibility_assessment&.limitation
    end

    delegate :mobility_impaired_access, to: :accessibility_assessment, allow_nil: true

    delegate :wheelchair_access, :step_free_access,
             :escalator_free_access, :lift_free_access,
             :audible_signals_available, :visual_signs_available,
             to: :accessibility_limitation, allow_nil: true

    delegate :description, to: :accessibility_assessment, allow_nil: true, prefix: 'accessibility'
  end

  class PointOfInterest < Site
    element :operating_organisation_view, type: 'OperatingOrganisationView'

    collection :classifications,
      type: Content.new(:point_of_interest_classification_view, type: 'PointOfInterestClassificationView')
  end

  class StopPlace < Site
    element :transport_mode
    element :transport_submode

    # example: onstreetTram
    element :stop_place_type

    collection :quays, type: Content.new(:quay, type: 'Quay')

    def embedded_resources
      super + quays
    end
  end
  class Quay < StopPlace
    reference :parent_quay_ref, type: StopPlace

    def to_stop_place
      Netex::StopPlace.new.tap do |stop_place|
        instance_variables.each do |variable_name|
          attribute_value = instance_variable_get(variable_name)
          attribute_name = variable_name.to_s[1..-1]

          stop_place.send "#{attribute_name}=", attribute_value
        end
      end
    end
  end

  class SiteElement < AddressablePlace
  end

  class SiteComponent < SiteElement
  end

  class StopPlaceEntrance < SiteComponent
    element :entrance_type
    element :is_external
    element :is_entry
    element :is_exit
    element :width
    element :height

    def embedded_referencable?
      true
    end
  end

  class LinkSequence < DataManagedObject
    element :name
  end

  class RoutePoint < Point
  end

  class Direction < LinkSequence
  end

  class PointOnRoute < VersionedChild
    attribute :order
    reference :route_point_ref, type: RoutePoint
  end

  class Route < LinkSequence
    reference :line_ref, type: Line
    element :direction_type
    reference :direction_ref, type: Direction
    reference :inverse_route_ref, type: Route

    collection :points_in_sequence, type: Content.new(:point_on_route, type: PointOnRoute)
  end

  class ScheduledStopPoint < Point
    # TODO tariffZones

    element :short_name
    element :description
    element :public_code
    element :private_code

    # example: onstreetTram
    element :stop_type

    # TODO Presentations
  end

  class StopAssignment < DataManagedObject
    element :name
    element :description
    reference :scheduled_stop_point_ref, type: ScheduledStopPoint
  end

  class PassengerStopAssignment < StopAssignment
    attribute :order
    reference :stop_place_ref, type: StopPlace
    reference :quay_ref, type: Quay

    # TODO trainElements
  end

  class PointInLinkSequence < Point
    attribute :order
  end

  class Via < VersionedChild
    element :name
  end

  class DestinationDisplay < DataManagedObject
    element :side_text
    element :front_text
    element :public_code
    collection :vias, type: Content.new(:via, type: Via)
  end

  class StopPointInJourneyPattern < PointInLinkSequence
    reference :destination_display_ref, type: DestinationDisplay
    reference :scheduled_stop_point_ref, type: ScheduledStopPoint
    element :for_alighting
    element :for_boarding
  end

  class JourneyPattern < LinkSequence
    reference :route_ref, type: Route
    reference :direction_ref, type: Direction
    reference :destination_display_ref, type: DestinationDisplay

    collection :points_in_sequence, type: Content.new(:stop_point_in_journey_pattern, type: StopPointInJourneyPattern)

    element :service_journey_pattern_type

    def referencables
      [ self, *points_in_sequence ]
    end
  end

  class ServiceJourneyPattern < JourneyPattern

  end

  # Warning: TimetabledPassingTime XML is created by a dedicated/non-generic Builder
  # See Netex::Target::Builder::TimetabledPassingTime
  class TimetabledPassingTime < VersionedChild
    reference :stop_point_in_journey_pattern_ref, type: StopPointInJourneyPattern

    element :arrival_time, type: Time
    element :arrival_day_offset, type: Integer
    element :departure_time, type: Time
    element :departure_day_offset, type: Integer
  end

  class PropertyOfDay < Resource
    element :name
    element :days_of_week
  end

  class DayType < DataManagedObject
    element :name
    element :description

    collection :properties, type: Content.new(:property_of_day, type: PropertyOfDay)

    def monday?
      days.include? 'monday'
    end

    def tuesday?
      days.include? 'tuesday'
    end

    def wednesday?
      days.include? 'wednesday'
    end

    def thursday?
      days.include? 'thursday'
    end

    def friday?
      days.include? 'friday'
    end

    def saturday?
      days.include? 'saturday'
    end

    def sunday?
      days.include? 'sunday'
    end

    private

    def days
      @days ||= properties.map do |property_of_day|
        property_of_day.days_of_week.downcase.split(' ')
      end.flatten.uniq
    end
  end

  class Timeband < DataManagedObject
    element :start_time, type: TimeOfDay
    element :end_time, type: TimeOfDay
  end

  class ServiceJourney < LinkSequence
    element :description
    element :transport_mode
    element :transport_submode

    collection :notice_assignments, type: Content.new(:notice_assignment, type: NoticeAssignment)

    element :departure_time, type: Time
    element :departure_day_offset, type: Integer

    collection :day_types, type: Content.new(:day_type_ref, type: DayType, content_type: :reference)

    reference :service_journey_pattern_ref, type: JourneyPattern
    reference :journey_pattern_ref, type: JourneyPattern

    def journey_pattern_ref
      @journey_pattern_ref || @service_journey_pattern_ref
    end

    # reference :line_ref, type: Line
    reference :operator_ref, type: Operator

    element :public_code

    collection :train_numbers, type: Content.new(:train_number_ref, type: String, content_type: :reference)
    collection :passing_times, type: Content.new(:timetabled_passing_time, type: TimetabledPassingTime)
  end

  class VehicleJourneyStopAssignment < DataManagedObject
    reference :scheduled_stop_point_ref, type: ScheduledStopPoint
    reference :stop_place_ref, type: StopPlace
    reference :quay_ref, type: Quay

    collection :vehicle_journey_refs, type: Content.new(:vehicle_journey_ref, type: ServiceJourney, content_type: :reference)
  end

  class OperatingPeriod < DataManagedObject
    element :from_date, type: ::Time
    element :to_date, type: ::Time

    def time_range
      return nil unless from_date && to_date && from_date <= to_date
      Range.new(from_date, to_date)
    end

    def date_range
      current_time_range = time_range
      return nil unless current_time_range

      Range.new current_time_range.min.to_date, current_time_range.max.to_date
    end

    def time_range=(range)
      self.from_date, self.to_date = range&.min, range&.max
    end
  end

  class UicOperatingPeriod < OperatingPeriod
    element :valid_day_bits
  end

  class DayTypeAssignment < DataManagedObject
    attribute :order, type: Integer

    element :name
    reference :operating_period_ref, type: OperatingPeriod
    element :date, type: Date
    reference :day_type_ref, type: DayType
    element :is_available

    def available?
      is_available == "true"
    end
  end

  class ServiceCalendar < DataManagedObject
    element :name
    element :from_date, type: Date
    element :to_date, type: Date
  end

  class RoutingConstraintZone < DataManagedObject
    element :name
    collection :members, type: Content.new(:scheduled_stop_point_ref, type: ScheduledStopPoint, content_type: :reference)
    element :zone_use
    collection :lines, type: Content.new(:line_ref, type: Line, content_type: :reference)
  end

  # TODO: Include Projection super class
  class TopographicProjection < DataManagedObject
  end

  class FareZone < DataManagedObject
    element :name

    # TODO: Manage TopographicProjections (and other kinds of Projection ?)
    collection :projections, type: Content.new(:topographic_projection_ref, type: TopographicProjection, content_type: :reference)
  end

  class Frame < EntityInVersion
    element :name
    reference :type_of_frame_ref, type: String

    def type_of_frame
      return type_of_frame_ref if type_of_frame_ref.is_a?(String)

      type_of_frame_ref&.ref
    end
  end

  class CompositeFrame < Frame; end
  class GeneralFrame < Frame; end
  class ResourceFrame < Frame; end
  class SiteFrame < Frame; end
  class ServiceFrame < Frame; end
  class ServiceCalendarFrame < Frame; end

end

module GML
  class Pos
    attr_accessor :x, :y, :srs_name

    def initialize(attributes = {})
      attributes.each { |k,v| send "#{k}=", v }
    end
  end
end
