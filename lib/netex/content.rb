module Netex
  class Content
    attr_accessor :name, :content_type

    def initialize(name, options = {})
      @name = name
      @content_type = :element
      options.each { |k,v| send "#{k}=", v }
    end

    def attribute?
      @content_type == :attribute
    end

    def element?
      @content_type == :element
    end

    def collection?
      @content_type == :collection
    end

    def resource?
      @is_a_resource ||= type.is_a?(Class) && (type <= Resource)
    end

    def create_xml_name
      lower = attribute? || collection? || name == :is_available
      self.class.xmlify(name, lower: lower).to_sym
    end

    def xml_name
      @xml_name ||= create_xml_name
    end

    def type=(type)
      case type
      when Class, Content
        @type = type
      else
        @type_name = type
      end
    end

    def resolve_type
      self.class.const_get "Netex::#{@type_name}"
    end

    def type
      @type ||= resolve_type
    end

    # Returns true when Content is defined like this:
    #
    # element :operator_ref, type: Reference
    # collection :additional_operators, type: Content.new(:operator_ref, type: Reference)
    def reference?
      if content_type == :reference
        return true
      end

      if type.respond_to?(:reference?)
        return type.reference?
      end

      false
    end

    IGNORED_VALUES = [nil, "", []].freeze

    def value(resource)
      value = resource.send(name)
      return nil if value.respond_to?(:empty?) and value.empty?
      value
    end

    def change_value(resource, value)
      value = nil if IGNORED_VALUES.include?(value)
      value = nil if value.respond_to?(:empty?) && value.empty?

      resource.send "#{name}=", value
    end

    # TODO specify to a collection ..
    def add_value(resource, value)
      return if IGNORED_VALUES.include?(value)
      return if value.respond_to?(:empty?) && value.empty?

      resource.send(name) << value
    end

    def xml_value(resource)
      raw_value = value(resource)

      case raw_value
      when nil
        nil
      when ::Time
        Netex::Target::Builder.format_time raw_value
      when ::Date
        Netex::Target::Builder.format_date raw_value
      when TimeOfDay
        raw_value.to_hms
      else
        raw_value.to_s
      end
    end

    def self.xmlify(string, lower: false)
      parts = string.to_s.split('_')

      transformed_parts = []
      if lower
        transformed_parts << parts.shift
      end

      transformed_parts.concat parts.map(&:capitalize)
      transformed_parts.join
    end

    def self.underscored(string)
      string.gsub(/([a-z]+)([A-Z])/, '\1_\2').downcase
    end

  end
end
