# frozen_string_literal: true

# Examples:
#
# * `chouette:JourneyPattern:1:LOC`
# * `FR:SMIRT:MonomodalStopPlace:454e74:`
# * `GE:79189:Line:78-a-1:`
# * `NO::Route:985a:ENTUR`
# * `FR::MonomodalStopPlace:43289:FR1`
# * `UK:: GeneralStopPlace:199G98765431:NAPTAN`
#
module Netex
  class ObjectId

    attr_accessor :country, :local, :type, :technical, :provider

    def initialize(country, local, type, technical, provider = nil)
      @country, @local, @type, @technical, @provider = country, local, type, technical, provider
      @string_format = create_string_format

      freeze
    end

    def change(country: nil, local: nil, type: nil, technical: nil, provider: nil)
      country ||= self.country
      local ||= self.local
      type ||= self.type
      technical ||= self.technical
      provider ||= self.provider

      if technical.is_a?(Array)
        technical = technical.join("-")
      end

      self.class.new country, local, type, technical, provider
    end

    # Merge with another identifier.
    #
    # The new identifier uses:
    # * the given type
    # * the two joined technical parts. If the other identifier has no technical part, the whole identifier is used.
    #
    def merge(other, type: nil, provider: nil)
      other_technical = other.respond_to?(:technical) ? other.technical : other
      change technical: [ technical, other_technical ], type: type, provider: provider
    end

    def to_s
      @string_format
    end

    def inspect
      "<#Netex::ObjectId #{to_s}>"
    end

    def ==(other)
      other.is_a?(self.class) &&
        country == other.country &&
        local == other.local &&
        type == other.type &&
        technical == other.technical &&
        provider == other.provider
    end

    def self.technical(value)
      # Try to convert into a ObjectId
      value = parse(value) || value
      value.respond_to?(:technical) ? value.technical : value
    end

    def self.merge(base, other, type:, provider: nil)
      base, other = parse(base), parse(other) || other
      base.merge(other, type: type, provider: provider) if base && other
    end

    def self.parse(definition)
      return if definition.nil?
      return definition if definition.is_a?(self)

      definition = definition.to_s

      parts = definition.split(":")
      case definition.count(":")
      when 4
        new(*parts)
      when 3
        new(nil, *parts)
      end
    end

    private

    def create_string_format
      if country
        "#{country}:#{local}:#{type}:#{technical}:#{provider}"
      else
        "#{local}:#{type}:#{technical}:#{provider}"
      end
    end

  end
end
