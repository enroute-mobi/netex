# frozen_string_literal: true

module Netex
  module Source
    def self.new(options = {})
      Base.new options
    end

    def self.read(filename, options = {})
      source = new
      source.read filename, **options

      if block_given?
        yield source
        source.close
      else
        source
      end
    end

    def self.file_type(filename)
      File.binread(filename, 2) == "PK" ? :zip : :xml
    end

    class Base
      attr_accessor :offline_threshold, :include_raw_xml

      def initialize(options = {})
        @offline_threshold = 5000
        @include_raw_xml = false

        options.each { |k, v| send "#{k}=", v }
      end

      def stored_resource?(resource)
        !collection_for(resource.class).nil?
      end

      def add(resource, tags = {})
        return unless stored_resource?(resource)

        resource.tags.merge! tags

        transformers.each do |transformer|
          Netex.logger.debug { "Source#add Transform with #{transformer.inspect}: #{resource.inspect}" }

          resource = transformer.transform resource
          return if resource.nil?
        end

        resource = embedding.process(resource)
        return if resource.nil?

        store resource
      end

      def store(resource)
        collection = collection_for resource.class
        return unless collection

        Netex.logger.info { "Source#store Store resource: #{resource.inspect}" }

        collection << resource

        offline_collection!(resource.class) if offline_required?(collection)

        resource
      end

      def embedding
        @embedding ||= Embedding.new(self)
      end

      # Manage embedded resources into a given NeTEx Resource
      class Embedding
        attr_reader :source

        def initialize(source)
          @source = source
        end
        delegate :add, :store, to: :source

        def embedded_resources_parent
          @embedded_resources_parent ||= {}
        end

        def embedded_resources_waiting_parent
          @embedded_resources_waiting_parent ||= {}
        end

        def resource_key(resource)
          [resource.class, resource.id]
        end

        def reference_key(reference)
          [reference.type, reference.ref]
        end

        def wait_embedded_resource(embedded_reference, parent_id)
          Netex.logger.debug do
            "Source::Embedding Register parent_id '#{parent_id}' for #{reference_key(embedded_reference)}"
          end
          embedded_resources_parent[reference_key(embedded_reference)] = parent_id
        end

        def wait_parent(embedded_resource)
          embedded_resources_waiting_parent[resource_key(embedded_resource)] = embedded_resource
        end

        def process(resource)
          # Add embedded resources (like Quays or StopPlaceEntrances) as standalone resources
          process_embedded_resources resource

          # Detect embedded resource references (like StopPlaceEntranceRef)
          process_embedded_references resource

          # Ignore Netex::Resource without id (not Netex::Entity)
          return resource unless resource.respond_to?(:id)

          # This resource can be a waiting embedded resource
          resource.tags[:parent_id] ||= embedded_resources_parent.delete(resource_key(resource))

          # This resource may need to wait a parent
          if resource.embedded_referencable? && !resource.has_tag?(:parent_id)
            Netex.logger.debug { "Source::Embedding Wait parent for #{resource.inspect}" }
            wait_parent resource
            return nil
          end

          resource
        end

        def process_embedded_resources(resource)
          resource.embedded_resources.each do |embedded_resource|
            Netex.logger.debug { "Source::Embedding Add embedded resource: #{embedded_resource.inspect}" }
            add embedded_resource, parent_id: resource.id
          end
        end

        def process_embedded_references(resource)
          resource.embedded_references.each do |embedded_reference|
            # Can match a resource which is waiting a parent_id tag
            waiting_resource = embedded_resources_waiting_parent.delete reference_key(embedded_reference)
            if waiting_resource
              Netex.logger.debug { "Source::Embedding Found waiting resource #{resource.inspect}" }
              add waiting_resource, parent_id: resource.id
            else
              # Register reference as associated to this parent
              wait_embedded_resource embedded_reference, resource.id
            end
          end
        end

        def drain
          # Store resources waiting parent (which will never come)
          embedded_resources_waiting_parent.delete_if do |_, resource|
            Netex.logger.debug { "Source::Embedding#drain Store waiting resource #{resource.inspect}" }
            store resource
          end
        end
      end

      # Invokes when read / all parsings are done (.. like before first collection access)
      def after_read
        embedding.drain
      end

      def transformers
        @transformers ||= []
      end

      def close
        collections.each(&:close)
      end

      def collection_for(resource_class)
        # To regroup all Frames in a single collection
        if resource_class <= Netex::Frame
          resource_class = Netex::Frame
        elsif resource_class <= Netex::OperatingPeriod
          resource_class = Netex::OperatingPeriod
        elsif resource_class <= Netex::JourneyPattern
          resource_class = Netex::JourneyPattern
        end

        _collections[resource_class.name_of_class]
      end

      def offline_collection!(resource_class)
        Netex.logger.info { "Source: switch #{resource_class} collection to offline" }

        collection = collection_for(resource_class)
        @collections[resource_class.name_of_class] = OfflineCollection.new(collection)
      end

      def offline_required?(collection)
        collection.memory? &&
          offline_threshold >= 0 && collection.count > offline_threshold
      end

      def resource_count
        after_read

        collections.sum(&:count)
      end

      def each_resource(except: [], &block)
        after_read

        collections(except: except).each do |collection|
          collection.each(&block)
        end
      end

      def resources(**options)
        enum_for :each_resource, **options
      end

      SUPPORTED_RESOURCES = %i[
        StopPlace Quay StopPlaceEntrance Line Operator Network RoutePoint
        Notice ServiceJourney Route Frame PointOfInterest Direction FareZone
        DayType DayTypeAssignment OperatingPeriod PassengerStopAssignment ScheduledStopPoint
        RoutingConstraintZone JourneyPattern DestinationDisplay VehicleJourneyStopAssignment
      ].freeze

      def self.supported_resources
        SUPPORTED_RESOURCES
      end

      # Define stop_places, lines, etc methods to access collections
      supported_resources.each do |resource_class_name|
        resource_class = Netex::Resource.for resource_class_name
        define_method("#{resource_class.name_of_class}s") do
          after_read

          collection_for resource_class
        end
      end

      def collections(except: [])
        return _collections.values if except.empty?

        except_resource_names_of_class = except.map(&:name_of_class)

        _collections.reject do |resource_name_of_class, _|
          except_resource_names_of_class.include?(resource_name_of_class)
        end.values
      end

      def _collections
        @collections ||= self.class.supported_resources.map do |resource_class_name|
          resource_class = Netex::Resource.for resource_class_name
          [resource_class.name_of_class, MemoryCollection.new]
        end.to_h
      end

      private :_collections

      def parse(input, tags = {})
        handler = Sax.new(include_raw_xml: include_raw_xml) do |resource|
          resource.tags.merge! tags
          add resource
        end
        Ox.sax_parse handler, input
      end

      # Read with this Source the file with the given filename
      def read(filename, **options)
        Reader.for(self, filename, **options).read
        after_read
      end

      def validity_period
        @validity_period ||= ValidityPeriodBuilder.new(self).build
      end

      # Used to compute Source Validity Period
      # from Operating Periods and included dates
      class ValidityPeriodBuilder
        def initialize(source)
          @source = source
        end

        attr_reader :source
        attr_accessor :min, :max

        def range
          min..max if min && max
        end

        def build
          add operating_period_ranges
          add day_type_assignment_ranges

          range
        end

        def operating_period_ranges
          source.operating_periods.map(&:date_range)
        end

        def day_type_assignments_with_date
          source.day_type_assignments.select do |assignment|
            assignment.date && assignment.available?
          end
        end

        def day_type_assignment_ranges
          day_type_assignments_with_date.map do |assignment|
            Range.new assignment.date, assignment.date
          end
        end

        def add(*ranges)
          ranges.flatten.each do |range|
            self.min = [min, range.min].compact.min
            self.max = [max, range.max].compact.max
          end
        end
      end
    end
  end
end
