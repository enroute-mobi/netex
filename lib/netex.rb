require 'tempfile'
require 'ox'
require 'forwardable'
require 'zip'
require 'time'

require 'active_support/core_ext/class/attribute'
require 'active_support/core_ext/module/delegation'
require 'active_support/concern'

require 'netex/version'
require 'netex/object_id'
require 'netex/time'
require 'netex/cache'
require 'netex/time_of_day'
require 'netex/content'
require 'netex/resource'
require 'netex/source'
require 'netex/source/recorder'
require 'netex/source/collection'
require 'netex/source/mapper'
require 'netex/source/reader'
require 'netex/source/sax'

require 'netex/target'
require 'netex/target/builder'
require 'netex/target/builder_book'
require 'netex/transformers'
require 'netex/target/default_profile'
require 'netex/target/reference_scope'
require 'netex/target/section'
require 'netex/target/frames'
require 'netex/target/document'
require 'netex/target/fragment'
require 'netex/target/container'
require 'netex/profile'
require 'netex/profile/french'
require 'netex/profile/european'
require 'netex/profile/raw'
require 'netex/profile/idfm'
require 'netex/profile/idfm/full'
require 'netex/profile/idfm/iboo'
require 'netex/profile/idfm/icar'
require 'netex/offline'

require 'netex/source/tagger'

require 'geo'

module Netex
  class << self
    def logger
      @logger ||= default_logger
    end

    def default_logger
      if ENV['NETEX_DEBUG'] == 'true'
        require 'logger'
        Logger.new $stdout, formatter: ->(severity, _, _, msg) { "#{severity[0]} #{msg}\n" }
      else
        NullLogger.new
      end
    end

    attr_writer :logger
  end

  class NullLogger
    def unknown(*_args); end
    %i[fatal error warn info debug].each do |level|
      define_method(level) { |*args| }
      define_method("#{level}?") { false }
    end
  end

  class Error < StandardError; end
  # Your code goes here...
end
