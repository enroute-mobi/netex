#!env ruby

require 'netex'

KERNEL_PAGE_SIZE = `getconf PAGESIZE`.chomp.to_i rescue 4096
STATM_PATH       = "/proc/#{Process.pid}/statm"
STATM_FOUND      = File.exist?(STATM_PATH)

def current_usage
  STATM_FOUND ? (File.read(STATM_PATH).split(' ')[1].to_i * KERNEL_PAGE_SIZE) / 1024 / 1024.0 : 0
end

offline_threshold = ENV.fetch('OFFLINE_THRESHOLD', '5000').to_i
puts "Offline threshold: #{offline_threshold}"

def profiling(name, &block)
  unless ENV['PROFILING'] == 'true'
    yield
    return
  end

  FileUtils.mkdir_p "tmp"
  require 'stackprof'
  file = "tmp/#{name}.dump"
  # bundle exec stackprof --d3-flamegraph #{file} > flamegraph.html
  StackProf.run(mode: :cpu, raw: true, out: file) do
    yield
  end
  puts "Profiling available in #{file}"
end

count = (ARGV.first || 100000).to_i
target = Netex::Target.build("output.xml")

start = Time.now
profiling "target-add" do
  stop_place = Netex::StopPlace.new

  puts "Add #{count} StopPlaces"
  count.times do |n|
    stop_place.id = "#{n+1}"
    stop_place.name = "Name #{n+1}"
    target.add stop_place
  end
end
puts "add: #{Time.now - start} seconds"

GC.start
puts "memory after add: #{current_usage} MB"

start = Time.now

profiling "target-close" do
  target.close
end

puts "close: #{Time.now - start} seconds"
GC.start
puts "memory after close: #{current_usage} MB"
