# NeTEx

## What is a NeTEx file

NeTEx (Network Exchange) is a reference format for data exchange, collective transport, defined at European level. It provides a description of network topology, schedules, pricing datas and access to information services.

A NeTEx file is simply a unique or group of XML files, that must be compliant with a (complete) XSD schema.  

For French readers, you’ll find a lot of resources on [this page](http://www.normes-donnees-tc.org/format-dechange/donnees-theoriques/netex/).

You can read the European NeTEx profil which gives a good overview on what can contain a NeTEx file [here](https://blog.enroute.mobi/app/uploads/2019/02/prCEN-TS-16614-PI-Profile-FV-E-2018-Final-Draft-1.pdf).

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'netex'
```

And then execute:

```console
$ bundle
```

Or install it yourself as:

```console
$ gem install netex
```

## Read and write NeTEx files

This gem read and write NeTEx data (CEN Technical Standard for exchanging Public Transport schedules and related data).

## License

The gem is available as open source under the terms of the [Apache License Version 2.0](http://www.apache.org/licenses/LICENSE-2.0).
