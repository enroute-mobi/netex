# frozen_string_literal: true

unless ENV['NO_RCOV']
  require 'simplecov'

  SimpleCov.start do
    add_filter 'vendor'
    add_filter 'spec'

    if ENV['CODACY_API_TOKEN']
      require 'simplecov-cobertura'
      formatter SimpleCov::Formatter::CoberturaFormatter
    end

    enable_coverage :branch
  end
end

require 'netex'
require 'logger'
require 'rexml/document'
require 'diffy'

module Netex
  def self.debug_logger
    @debug_logger ||=
      begin
        formatter = proc do |severity, _, _, msg|
          "#{severity[0]} #{msg}\n"
        end
        Logger.new $stdout, formatter: formatter
      end
  end

  def self.debug
    old_logger = Netex.logger
    Netex.logger = debug_logger
    result = yield
    Netex.logger = old_logger
    result
  end
end

RSpec.configure do |c|
  c.around(:example) do |example|
    if example.metadata[:debug]
      Netex.debug { example.run }
    else
      example.run
    end
  end
end

module Netex
  module Source
    class Mock
      def initialize
        @collections = Hash.new { |h, name| h[name] = MockCollection.new }
      end

      def method_missing(name, **arguments)
        if arguments.empty?
          @collections[name]
        else
          super
        end
      end

      def respond_to_missing?(name, **arguments)
        arguments.empty? || super
      end

      class MockCollection
        def initialize(resources = [])
          @resources = resources
        end

        include Enumerable
        def each(&block)
          @resources.each(&block)
        end

        def find(resource_id)
          @resources.find { |resource| resource.id == resource_id }
        end

        def add(resource)
          @resources << resource
        end
        alias << add
      end
    end
  end
end

module Zip
  class Test
    def buffer
      @buffer ||= StringIO.new
    end

    def load_entries
      entries = {}

      buffer.rewind
      Zip::InputStream.open buffer do |io|
        while (native_entry = io.get_next_entry)
          entry = Entry.new native_entry, io.read
          entries[entry.name] = entry
        end
      end

      entries
    end

    def entries
      @entries ||= load_entries
    end

    def entry(name)
      entries.fetch name
    end

    def content(name)
      entry(name).content
    end

    class Entry
      def initialize(native_entry, content)
        @native_entry = native_entry
        @content = content
      end

      def name
        @native_entry.name
      end

      attr_reader :content
    end
  end
end

module Netex
  module Profile
    class Test < Netex::Profile::Base
      def initialize(container_type: :zip, builder_book: nil, &block)
        super()

        self.container_type = container_type
        self.builder_book = builder_book

        @document_proc = block
      end

      def transform(resource)
        resource
      end

      def document_for(resource)
        instance_exec resource, &@document_proc
      end
    end
  end
end

module Netex
  module Target
    class BuilderBook
      class Test < Netex::Target::BuilderBook
        def initialize(&block)
          super()

          @builder_proc = block
        end

        def builder(resource)
          @builder_proc.call(resource) || super(resource)
        end
      end
    end
  end
end

RSpec::Matchers.define_negated_matcher :not_change, :change

RSpec::Matchers.define :be_same_xml do |expected|
  def normalize(xml)
    formatter = REXML::Formatters::Pretty.new
    formatter.compact = true

    output = StringIO.new
    formatter.write(REXML::Document.new(xml), output)
    output.string
  end

  match do |xml|
    normalize(xml) == normalize(expected)
  end

  def diff(xml)
    Diffy::Diff.new(normalize(expected), normalize(xml))
  end

  failure_message do |xml|
    "doesn't match expected XML:\n#{diff(xml)}"
  end

  failure_message_when_negated do |_xml|
    'match not expected XML'
  end

  description do
    "be same xml content than #{normalize(expected)}"
  end
end
