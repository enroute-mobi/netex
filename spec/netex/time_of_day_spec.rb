require "spec_helper"

RSpec.describe Netex::TimeOfDay do
  describe ".parse" do
    subject { Netex::TimeOfDay.parse(definition) }
    context "when given definition is '09:15'" do
      let(:definition) { '09:15' }
      it { is_expected.to have_attributes(hour: 9, minute: 15, second: 0)}
    end
    context "when given definition is '09:15:30'" do
      let(:definition) { '09:15:30' }
      it { is_expected.to have_attributes(hour: 9, minute: 15, second: 30)}
    end
    context "when given definition is '09'" do
      let(:definition) { '09' }
      it { is_expected.to have_attributes(hour: 9, minute: 0, second: 0)}
    end
    context "when given definition is ''" do
      let(:definition) { '' }
      it { is_expected.to be_nil }
    end
    context "when given definition is nil" do
      let(:definition) { nil }
      it { is_expected.to be_nil }
    end
    context "when given definition is 'dummy'", pending: 'Improve TimeOfDay parser' do
      let(:definition) { 'dummy' }
      it { is_expected.to be_nil }
    end
    describe "#to_hms" do
      subject { Netex::TimeOfDay.new(hour: "09", minute: "15", second: "30").to_hms  }
      it { is_expected.to eq('09:15:30') }
    end
  end

end
