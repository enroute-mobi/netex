require 'spec_helper'

RSpec.describe Netex::Time do
  describe '#to_xml_value' do
    subject { time.to_xml_value }

    context 'for hour:1, minute:59, second:0' do
      let(:time) { Netex::Time.new 1, 59 }
      it { is_expected.to eq '01:59:00' }
    end

    context 'for hour:1, minute:59, second:0, timezone:+02:00' do
      let(:time) { Netex::Time.new 1, 59, 0, '+02:00' }
      it { is_expected.to eq '01:59:00+02:00' }
    end
  end

  describe '.parse' do
    subject { Netex::Time.parse definition }

    context 'with definition "12:13:14"' do
      let(:definition) { '12:13:14' }
      it { is_expected.to eq(Netex::Time.new(12, 13, 14)) }
    end
  end
end
