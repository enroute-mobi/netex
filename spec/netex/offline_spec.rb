# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Netex::Offline::Hash do
  let(:hash) { described_class.new(Tempfile.new) }

  describe '#[]' do
    subject { hash['key'] }

    context 'when "key" is associated to "value"' do
      before { hash['key'] = 'dummy' }

      it { is_expected.to eq('dummy') }
    end

    context 'when the given key is unknown' do
      it { is_expected.to be_nil }
    end
  end

  describe '#[]=' do
    context "no value is associated to the key 'key'" do
      it "changes the associated value to 'value'" do
        expect { hash['key'] = 'value' }.to change { hash['key'] }.from(nil).to('value')
      end
    end

    context "a value 'value' is already associated to the key 'key'" do
      before { hash['key'] = 'value' }

      it "changes the associated value to 'new_value'" do
        expect { hash['key'] = 'new_value' }.to change { hash['key'] }.from('value').to('new_value')
      end
    end
  end

  describe '#keys' do
    subject { hash.keys }

    context 'when no key is defined' do
      it { is_expected.to be_empty }
    end

    context 'when a key "key" is defined' do
      before { hash['key'] = 'value' }

      it { is_expected.to contain_exactly('key') }
    end

    context 'when 1000 keys are associated' do
      let(:keys) { 1000.times.map { |i| "key-#{i}" } }

      before do
        keys.each_with_index do |key, i|
          hash[key] = "value-#{i}"
        end
      end

      it 'returns exactly all keys' do
        is_expected.to match_array(keys)
      end
    end
  end

  describe '#values' do
    subject { hash.values.to_a }

    context 'when no key is defined' do
      it { is_expected.to be_empty }
    end

    context 'when "key" is associated to "value"' do
      before { hash['key'] = 'value' }

      it { is_expected.to contain_exactly('value') }
    end

    context 'when "key" is associated to "value" and then "new_value"' do
      before { hash['key'] = 'value' }
      before { hash['key'] = 'new_value' }

      it { is_expected.to contain_exactly('value', 'new_value') }
    end

    context 'when 1000 keys/values are associated' do
      let(:values) { 1000.times.map { |i| "values-#{i}" } }

      before do
        values.each_with_index do |value, i|
          hash["key-#{i}"] = value
        end
      end

      it 'returns exactly all values' do
        is_expected.to match_array(values)
      end
    end
  end

  describe '#count' do
    subject { hash.count }

    context 'when no key is defined' do
      it { is_expected.to be_zero }
    end

    context 'when "key" is associated to "value"' do
      before { hash['key'] = 'value' }

      it { is_expected.to eq(1) }
    end

    context 'when 1000 keys/values are associated' do
      before do
        1000.times do |i|
          hash["key-#{i}"] = "values-#{i}"
        end
      end

      it 'returns exactly all values' do
        is_expected.to eq(1000)
      end
    end
  end
end
