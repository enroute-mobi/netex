# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Netex::Source::Tagger::Line do
  subject(:tagger) { described_class.new(source) }
  let(:source) { Netex::Source::Mock.new }

  describe '#lines' do
    subject { tagger.lines }

    context 'when Source#lines returns a Line line-1 named "Line Sample"' do
      before do
        source.lines << line
      end

      let(:line) { Netex::Line.new id: 'line-1', name: 'Line Sample' }

      it { is_expected.to contain_exactly(an_object_having_attributes(id: 'line-1', name: 'Line Sample')) }

      context 'the returned Line' do
        subject { tagger.lines.first }

        it { is_expected.to have_tag(:line_id, 'line-1') }
        it { is_expected.to have_tag(:line_name, 'Line Sample') }
      end
    end

    context 'when Source#lines returns a Line associated to an Operator operator-1 "Operator Sample"' do
      before do
        source.lines << line
        source.operators << operator
      end

      let(:line) { Netex::Line.new id: 'line-1', name: 'Line Sample', operator_ref: operator.to_reference }
      let(:operator) { Netex::Operator.new id: 'operator-1', name: 'Operator Sample' }

      context 'the returned Line' do
        subject { tagger.lines.first }

        it { is_expected.to have_tag(:line_id, 'line-1') }
        it { is_expected.to have_tag(:line_name, 'Line Sample') }

        it { is_expected.to have_tag(:operator_id, 'operator-1') }
        it { is_expected.to have_tag(:operator_name, 'Operator Sample') }
      end
    end
  end

  describe '#routes' do
    subject { tagger.routes }

    context 'when Source#routes returns a Route id named "Sample"' do
      before do
        source.routes << route
      end

      let(:route) { Netex::Route.new id: 'id', name: 'Sample' }

      it { is_expected.to contain_exactly(an_object_having_attributes(id: 'id', name: 'Sample')) }

      context 'when Route is associated to a Line line-1 "Line Sample"' do
        before do
          source.lines << line
          route.line_ref = line.to_reference
        end

        let(:line) { Netex::Line.new id: 'line-1', name: 'Line Sample' }

        context 'the returned Route' do
          subject { tagger.routes.first }

          it { is_expected.to have_tag(:line_id, 'line-1') }
          it { is_expected.to have_tag(:line_name, 'Line Sample') }
        end
      end
    end
  end

  describe '#journey_patterns' do
    subject { tagger.journey_patterns }

    context 'when Source#journey_patterns returns a JourneyPattern id named "Sample"' do
      before do
        source.journey_patterns << journey_pattern
      end

      let(:journey_pattern) { Netex::JourneyPattern.new id: 'id', name: 'Sample' }

      it { is_expected.to contain_exactly(an_object_having_attributes(id: 'id', name: 'Sample')) }

      context 'when JourneyPattern is associated via a Route to a Line line-1 "Line Sample"' do
        before do
          source.lines << line
          source.routes << route
          journey_pattern.route_ref = route.to_reference
        end

        let(:route) { Netex::Route.new id: 'route-1', line_ref: line.to_reference }
        let(:line) { Netex::Line.new id: 'line-1', name: 'Line Sample' }

        context 'the returned Journey Pattern' do
          subject { tagger.journey_patterns.first }

          it { is_expected.to have_tag(:line_id, 'line-1') }
          it { is_expected.to have_tag(:line_name, 'Line Sample') }
        end
      end
    end
  end

  describe '#service_journeys' do
    subject { tagger.service_journeys }

    context 'when Source#service_journeys returns a ServiceJourney id named "Sample"' do
      before do
        source.service_journeys << service_journey
      end

      let(:service_journey) { Netex::ServiceJourney.new id: 'id', name: 'Sample' }

      it { is_expected.to contain_exactly(an_object_having_attributes(id: 'id', name: 'Sample')) }

      context 'when ServiceJourney is associated via a Journey Pattern and a Route to a Line line-1 "Line Sample"' do
        before do
          source.lines << line
          source.routes << route
          source.journey_patterns << journey_pattern

          service_journey.journey_pattern_ref = journey_pattern.to_reference
        end

        let(:journey_pattern) { Netex::JourneyPattern.new id: 'journey-pattern-1', route_ref: route.to_reference }
        let(:route) { Netex::Route.new id: 'route-1', line_ref: line.to_reference }
        let(:line) { Netex::Line.new id: 'line-1', name: 'Line Sample' }

        context 'the returned Service Journey' do
          subject { tagger.service_journeys.first }

          it { is_expected.to have_tag(:line_id, 'line-1') }
          it { is_expected.to have_tag(:line_name, 'Line Sample') }
        end
      end
    end
  end
end
