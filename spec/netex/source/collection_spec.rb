# frozen_string_literal: true

require 'spec_helper'

RSpec.shared_examples 'a resource collection' do
  def resource(attributes = {})
    indexes = attributes.delete(:indexes)
    Netex::Quay.new(attributes).tap do |resource|
      resource.indexes.merge! indexes if indexes
    end
  end

  describe '#count' do
    subject { collection.count }

    context 'when no resource has been added' do
      it { is_expected.to be_zero }
    end

    context 'when one resource has been added without identifier' do
      before { collection.add resource(id: nil) }

      it { is_expected.to eq(1) }
    end

    context 'when one resource has been added with identifier' do
      before { collection.add resource(id: 'test') }

      it { is_expected.to eq(1) }
    end

    context 'when 10000 resources has been added' do
      before { 10_000.times { collection.add resource } }

      it { is_expected.to eq(10_000) }
    end
  end

  describe '#find' do
    subject { collection.find identifier }
    let(:identifier) { 'target' }

    context 'when the given identifier is nil' do
      let(:identifier) { nil }
      before { collection.add resource(id: nil) }

      it { is_expected.to be_nil }
    end

    context 'when no resource is the given identifier' do
      it { is_expected.to be_nil }
    end

    context 'when a resource is associated to the given identifier' do
      before { collection.add resource(id: identifier) }

      it { is_expected.to having_attributes(id: identifier) }
    end

    context 'when two resources are associated to the given identifier' do
      before do
        collection.add resource(id: identifier, name: 'First')
        collection.add resource(id: identifier, name: 'Second')
      end

      it { is_expected.to having_attributes(id: identifier, name: 'Second') }
    end
  end

  describe '#each' do
    subject { collection.each.to_a }

    context 'when contains 5 resources without identifier' do
      before { 5.times { collection.add resource } }

      it { is_expected.to have_attributes(size: 5) }
    end

    context 'when contains 5 resources with the same identifier' do
      before { 5.times { collection.add resource(id: 'same') } }

      it { is_expected.to have_attributes(size: 5) }
    end

    context 'when contains 10 000 resources' do
      let(:identifiers) { 10_000.times.map { |n| "identifier-#{n}" } }
      before { identifiers.each { |id| collection.add resource(id: id) } }

      it { is_expected.to contain_exactly(*identifiers.map { |id| an_object_having_attributes(id: id) }) }
    end
  end

  describe '#find_by' do
    subject { collection.find_by(search).to_a }
    let(:search) { { key: :value } }

    context 'when no resource is associated to the given attribute/value' do
      it { is_expected.to be_empty }
    end

    context 'when a resource is associated to {key: :value}' do
      before { collection.add resource(id: 'target', indexes: search) }

      it { is_expected.to contain_exactly(an_object_having_attributes(id: 'target')) }
    end

    context "when a resource isn't associated to {key: :value}" do
      before { collection.add resource(id: 'wrong', indexes: { key: :other }) }

      it { is_expected.to be_empty }
    end

    context 'when two resources is associated to {key: :value}' do
      before do
        %w[first second].each do |identifier|
          collection.add resource(id: identifier, indexes: search)
        end
      end

      def expected_resources
        %w[first second].map do |identifier|
          an_object_having_attributes(id: identifier)
        end
      end

      it { is_expected.to contain_exactly(*expected_resources) }
    end
  end
end

RSpec.describe Netex::Source::MemoryCollection do
  subject(:collection) { described_class.new }

  it_behaves_like 'a resource collection'

  describe '#memory?' do
    subject { collection.memory? }

    it { is_expected.to be_truthy }
  end
end

RSpec.describe Netex::Source::OfflineCollection do
  subject(:collection) { described_class.new }

  it_behaves_like 'a resource collection'

  describe '#memory?' do
    subject { collection.memory? }

    it { is_expected.to be_falsy }
  end

  describe 'create from an existing_collection' do
    let(:existing_collection) { Netex::Source::MemoryCollection.new }
    subject(:collection) { described_class.new existing_collection }

    context 'when existing resource contains a resource' do
      let(:resource) { Netex::Quay.new id: 'existing' }
      before { existing_collection.add resource }

      it { is_expected.to include(an_object_having_attributes(id: resource.id)) }
    end

    context 'when existing resource contains a indexed resource' do
      subject { collection.find_by(search).to_a }

      let(:search) { { key: 'value' } }

      let(:resource) { Netex::Quay.new(id: 'existing').tap { |r| r.indexes.merge! search } }
      before { existing_collection.add resource }

      it { is_expected.to match_array(have_attributes(id: resource.id)) }
    end
  end

end
