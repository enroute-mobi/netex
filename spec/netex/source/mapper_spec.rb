# frozen_string_literal: true

require 'spec_helper'

describe Netex::Source::Mapper::Resource do
  describe '#mapper_class_for' do
    subject { described_class.mapper_class_for(resource_class) }

    context 'when resource class is Netex::Quay' do
      let(:resource_class) { Netex::Quay }
      it { is_expected.to eq(Netex::Source::Mapper::Resource::Quay) }
    end

    context 'when resource class is Netex::Line' do
      let(:resource_class) { Netex::Line }
      it { is_expected.to eq(Netex::Source::Mapper::Resource::Line) }
    end
    context 'when resource class is Netex::Location' do
      let(:resource_class) { Netex::Location }
      it { is_expected.to eq(Netex::Source::Mapper::Resource::Location) }
    end
  end
end

describe Netex::Source::Mapper::Frame do
  describe '#frame_class' do
    subject { described_class.frame_class(name) }

    context "when name is 'GeneralFrame'" do
      let(:name) { 'GeneralFrame' }
      it { is_expected.to eq(Netex::GeneralFrame) }
    end
    context 'when name is :GeneralFrame' do
      let(:name) { :GeneralFrame }
      it { is_expected.to eq(Netex::GeneralFrame) }
    end
    context 'when name is :DummyFrame' do
      let(:name) { :DummyFrame }
      it { is_expected.to eq(Netex::Frame) }
    end
    context 'when name is :Quay' do
      let(:name) { :Quay }
      it { is_expected.to be_nil }
    end
  end
end
