# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Netex::Source::Recorder do
  subject(:recorder) { Netex::Source::Recorder.new }

  describe '#text' do
    context 'when given text is "dummy"' do
      it { expect { recorder.text('dummy') }.to change(recorder, :raw_xml).to('dummy') }
    end

    context 'when given text is "with &"' do
      it { expect { recorder.text('with &') }.to change(recorder, :raw_xml).to('with &amp;') }
    end
  end
end
