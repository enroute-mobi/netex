require 'spec_helper'

RSpec.describe Netex::Target do
  subject(:target) { Netex::Target::Base.new StringIO.new }

  context 'without given profile' do
    it 'uses a Netex::Target::DefaultProfile' do
      expect(target.profile).to be_a(Netex::Target::DefaultProfile)
    end
  end

  describe 'DefaultProfile' do
    subject(:default_profile) { Netex::Target::DefaultProfile.new }

    let(:resource) { double }

    it 'uses a default document for any resource' do
      expect(default_profile.document_for(resource)).to be(default_profile.default_document)
    end

    describe '#documents' do
      it 'returns the default document' do
        expect(default_profile.documents).to eq([default_profile.default_document])
      end
    end

    describe '#create_container' do
      let(:output) { double }
      it 'returns an Container::XML' do
        expect(default_profile.create_container(output)).to be_a(Netex::Target::Container::XML)
      end
    end
  end

  describe '#document_for_resource' do
    let(:resource) { double }
    let(:profile) { double }
    let(:document) { double }

    before { allow(target).to receive(:profile).and_return(profile) }

    it 'uses the profile to select the document' do
      expect(profile).to receive(:document_for).with(resource).and_return(document)
      expect(target.document_for_resource(resource)).to be(document)
    end
  end

  describe '#documents' do
    let(:profile) { double }
    let(:documents) { [double] }

    before { allow(target).to receive(:profile).and_return(profile) }

    it 'uses the profile to select the document' do
      expect(profile).to receive(:documents).and_return(documents)
      expect(target.documents).to eq(documents)
    end
  end

  context 'with default configuration' do
    it 'creates a single XML file' do
      output = StringIO.new
      target = Netex::Target.build(output, participant_ref: 'test',
                                           publication_timestamp: Time.parse('2030-01-01 12:00 UTC'))
      target.add Netex::StopPlace.new(name: 'first')
      target.add Netex::StopPlace.new(name: 'second')

      target.close

      expected_xml = <<~XML
        <?xml version="1.0" encoding="UTF-8"?>
        <PublicationDelivery xmlns="http://www.netex.org.uk/netex">
          <PublicationTimestamp>2030-01-01T12:00:00Z</PublicationTimestamp>
          <ParticipantRef>test</ParticipantRef>
          <dataObjects>
            <SiteFrame id="test:SiteFrame:1" version="any">
              <stopPlaces>
                <StopPlace version="any">
          <Name>first</Name>
        </StopPlace>
        <StopPlace version="any">
          <Name>second</Name>
        </StopPlace>
              </stopPlaces>
            </SiteFrame>
          </dataObjects>
        </PublicationDelivery>
      XML

      expect(output.string).to eq(expected_xml)
    end

    it 'can embed quays into StopPlace resources' do
      output = StringIO.new
      target = Netex::Target.build(output, participant_ref: 'test',
                                           publication_timestamp: Time.parse('2030-01-01 12:00 UTC'))
      target.add Netex::Quay.new(id: 'child').with_tag(parent_id: 'parent')
      target.add Netex::StopPlace.new(id: 'parent')

      target.close

      expected_xml = <<~XML
        <?xml version="1.0" encoding="UTF-8"?>
        <PublicationDelivery xmlns="http://www.netex.org.uk/netex">
          <PublicationTimestamp>2030-01-01T12:00:00Z</PublicationTimestamp>
          <ParticipantRef>test</ParticipantRef>
          <dataObjects>
            <SiteFrame id="test:SiteFrame:1" version="any">
              <stopPlaces>
                <StopPlace id="parent" version="any">
          <quays>
            <Quay id="child" version="any"/>
          </quays>
        </StopPlace>
              </stopPlaces>
            </SiteFrame>
          </dataObjects>
        </PublicationDelivery>
      XML

      expect(output.string).to eq(expected_xml)
    end

    it 'can embed entrances into StopPlace resources' do
      output = StringIO.new
      target = Netex::Target.build(output, participant_ref: 'test',
                                           publication_timestamp: Time.parse('2030-01-01 12:00 UTC'))
      target.add Netex::StopPlaceEntrance.new(id: 'child').with_tag(parent_id: 'parent')
      target.add Netex::StopPlace.new(id: 'parent')

      target.close

      expected_xml = <<~XML
        <?xml version="1.0" encoding="UTF-8"?>
        <PublicationDelivery xmlns="http://www.netex.org.uk/netex">
          <PublicationTimestamp>2030-01-01T12:00:00Z</PublicationTimestamp>
          <ParticipantRef>test</ParticipantRef>
          <dataObjects>
            <SiteFrame id="test:SiteFrame:1" version="any">
              <stopPlaces>
                <StopPlace id="parent" version="any">
          <entrances>
            <StopPlaceEntrance id="child" version="any"/>
          </entrances>
        </StopPlace>
              </stopPlaces>
            </SiteFrame>
          </dataObjects>
        </PublicationDelivery>
      XML

      expect(output.string).to eq(expected_xml)
    end
  end

  context 'with a Profile with use a zip container' do
    it 'creates a zip file' do
      profile = Netex::Profile::Test.new(container_type: :zip) do |resource|
        document "stops-#{resource.name}.xml"
      end

      zip_file = Zip::Test.new
      target = Netex::Target.build zip_file.buffer, profile: profile, publication_timestamp: ::Time.utc(2021)
      target.add Netex::StopPlace.new(name: 'first')
      target.add Netex::StopPlace.new(name: 'second')

      target.close

      expected_first_xml = <<~XML
        <?xml version="1.0" encoding="UTF-8"?>
        <PublicationDelivery xmlns="http://www.netex.org.uk/netex">
          <PublicationTimestamp>2021-01-01T00:00:00Z</PublicationTimestamp>
          <ParticipantRef>enRoute</ParticipantRef>
          <dataObjects>
            <SiteFrame id="enRoute:SiteFrame:1" version="any">
              <stopPlaces>
                <StopPlace version="any">
          <Name>first</Name>
        </StopPlace>
              </stopPlaces>
            </SiteFrame>
          </dataObjects>
        </PublicationDelivery>
      XML

      expected_second_xml = <<~XML
        <?xml version="1.0" encoding="UTF-8"?>
        <PublicationDelivery xmlns="http://www.netex.org.uk/netex">
          <PublicationTimestamp>2021-01-01T00:00:00Z</PublicationTimestamp>
          <ParticipantRef>enRoute</ParticipantRef>
          <dataObjects>
            <SiteFrame id="enRoute:SiteFrame:1" version="any">
              <stopPlaces>
                <StopPlace version="any">
          <Name>second</Name>
        </StopPlace>
              </stopPlaces>
            </SiteFrame>
          </dataObjects>
        </PublicationDelivery>
      XML

      output_first_xml = zip_file.content('stops-first.xml')
      output_second_xml = zip_file.content('stops-second.xml')

      expect(output_first_xml).to eq(expected_first_xml)
      expect(output_second_xml).to eq(expected_second_xml)
    end
  end

  describe '#empty?' do
    subject { target.empty? }

    context 'no Resource is added' do
      it { is_expected.to be_truthy }
    end

    context 'when a Resource is added' do
      before { target.add Netex::StopPlace.new }

      it { is_expected.to be_falsy }
    end
  end

  describe '#resource_count' do
    subject { target.resource_count }

    context 'no Resource is added' do
      it { is_expected.to be_zero }
    end

    context 'when a Resource is added' do
      before { target.add Netex::StopPlace.new }

      it { is_expected.to eq(1) }
    end

    context 'when 100 Resources are added' do
      before { 100.times { target.add Netex::StopPlace.new } }

      it { is_expected.to eq(100) }
    end
  end

  describe 'after_add' do
    let(:target) { Netex::Target.build(StringIO.new, after_add: after_add) }
    let(:after_add) { Proc.new {} }
    let(:resource) { Netex::StopPlace.new }

    context "when Target#add is invoked" do
      it do
        expect(after_add).to receive(:call).with(resource)
        target.add resource
      end
    end
  end

  it 'avoids tenth of a second in Time' do
    output = StringIO.new
    target = Netex::Target.build(output, participant_ref: 'test',
                                 publication_timestamp: Time.parse('2030-01-01 12:00 UTC'))
    target.add Netex::StopPlace.new(id: 'parent', changed: Time.parse('2030-01-01 12:00 UTC'))
    target.close

    expected_xml = <<~XML
        <?xml version="1.0" encoding="UTF-8"?>
        <PublicationDelivery xmlns="http://www.netex.org.uk/netex">
          <PublicationTimestamp>2030-01-01T12:00:00Z</PublicationTimestamp>
          <ParticipantRef>test</ParticipantRef>
          <dataObjects>
            <SiteFrame id="test:SiteFrame:1" version="any">
              <stopPlaces>
                <StopPlace id="parent" changed="2030-01-01T12:00:00Z" version="any"/>
              </stopPlaces>
            </SiteFrame>
          </dataObjects>
        </PublicationDelivery>
      XML

    expect(output.string).to be_same_xml(expected_xml)
  end
end
