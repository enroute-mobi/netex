# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Netex::Profile do
  describe '.classify' do
    {
      'european' => 'European',
      'european/strict' => 'European::Strict',
      'two_words' => 'TwoWords'
    }.each do |from, to|
      it "returns '#{to}' for '#{from}'" do
        expect(Netex::Profile.classify(from)).to eq(to)
      end
    end
  end

  describe '.create' do
    subject { Netex::Profile.create(type) }

    context 'for :european' do
      let(:type) { :european }
      it { is_expected.to be_a(Netex::Profile::European) }
    end

    context 'for :french' do
      let(:type) { :french }
      it { is_expected.to be_a(Netex::Profile::French) }
    end

    context "for 'idfm/full'" do
      let(:type) { 'idfm/full' }
      it { is_expected.to be_a(Netex::Profile::IDFM::Full) }
    end

    context "for 'idfm/iboo'" do
      let(:type) { 'idfm/iboo' }
      it { is_expected.to be_a(Netex::Profile::IDFM::IBOO) }
    end

    context "for 'idfm/icar'" do
      let(:type) { 'idfm/icar' }
      it { is_expected.to be_a(Netex::Profile::IDFM::ICAR) }
    end
  end

  describe 'custom BuilderBook' do
    let(:reference_builder_class) do
      Class.new(Netex::Target::Builder) do
        def build_version
          if resource.local?
            xml_element[:version] = resource.version_or_any
          else
            xml_element << "version=\"#{resource.version_or_any}\""
          end
        end
      end
    end

    let(:builder_book) do
      Netex::Target::BuilderBook::Test.new do |resource|
        reference_builder_class.new(resource) if resource.is_a?(Netex::Reference)
      end
    end

    let(:profile) do
      Netex::Profile::Test.new(container_type: :zip, builder_book: builder_book) do |_resource|
        document 'test.xml' do |document|
          document.frames << Netex::Target::GeneralFrame.new.tap do |frame|
            frame.sections.create accept: Netex::Reference
          end
        end
      end
    end

    let(:zip_file) { Zip::Test.new }

    let(:target) { Netex::Target.build zip_file.buffer, profile: profile, publication_timestamp: ::Time.utc(2021) }

    let(:xml_content) do
      target.add Netex::Reference.new('local', type: String, version: 'dummy', local: true)
      target.add Netex::Reference.new('non-local', type: String, version: 'dummy', local: false)
      target.close
      zip_file.content('test.xml')
    end

    it 'creates XML with the specified BuilderBook' do
      expect(xml_content).to include('<Reference ref="non-local">version="dummy"</Reference>')
    end
  end
end
