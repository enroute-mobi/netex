require "spec_helper"

RSpec.describe Netex::Target::Builder do

  def builder(resource)
    Netex::Target::Builder.for resource
  end

  def to_xml(resource)
    builder(resource).to_xml
  end

  describe "keyList xml" do

    it "outputs keyList tag" do
      expected_xml =<<~XML
      <StopPlace version="any">
        <keyList>
          <KeyValue typeOfKey="test">
            <Key>key</Key>
            <Value>value</Value>
          </KeyValue>
        </keyList>
      </StopPlace>
      XML
      expected_xml.strip!

      resource = Netex::StopPlace.new
      resource.key_list = [ Netex::KeyValue.new(key: 'key', value: 'value', type_of_key: 'test') ]

      expect(to_xml(resource)).to eq(expected_xml)
    end

    it "ignores empty key_list" do
      expected_xml = '<StopPlace version="any"/>'
      resource = Netex::StopPlace.new

      [ nil, "" ].each do |value|
        resource.key_list = [ Netex::KeyValue.new(key: "key", value: value) ]
        expect(to_xml(resource)).to eq(expected_xml)
      end
    end

  end

  describe "collection raw_content" do
    it "outputs raw content tag" do
      expected_xml =<<~XML
      <StopPlace version="any">
        <entrances>
          <dummy/>
        </entrances>
      </StopPlace>
      XML
      expected_xml.strip!

      resource = Netex::StopPlace.new
      resource.entrances.raw_content = "<dummy/>"

      expect(to_xml(resource)).to eq(expected_xml)
    end
  end

  describe "data_source_ref attribute" do

    it "outputs as 'dataSourceRef' XML attribute" do
      expected_xml = %{<StopPlace dataSourceRef="test" version="any"/>}

      resource = Netex::StopPlace.new(data_source_ref: "test")
      expect(to_xml(resource)).to eq(expected_xml)
    end

  end

  describe "StopPlace xml" do

    it "outputs id tag" do
      expected_xml = '<StopPlace id="test" version="any"/>'
      expect(to_xml(Netex::StopPlace.new(id: 'test'))).to eq(expected_xml)
    end

    it "creates Name tag" do
      expected_xml =<<~XML
      <StopPlace version="any">
        <Name>test</Name>
      </StopPlace>
      XML
      expected_xml.strip!

      expect(to_xml(Netex::StopPlace.new(name: 'test'))).to eq(expected_xml)
    end

    it "creates TransportMode tags" do
      expected_xml =<<~XML
      <StopPlace version="any">
        <TransportMode>bus</TransportMode>
        <BusSubmode>regionalBus</BusSubmode>
      </StopPlace>
      XML
      expected_xml.strip!

      stopPlace = Netex::StopPlace.new(transport_mode: 'bus', transport_submode: 'regionalBus')
      expect(to_xml(stopPlace)).to eq(expected_xml)
    end

    it "creates Centroid tag with gml:pos" do
      expected_xml =<<~XML
      <StopPlace version="any">
        <Centroid version="any">
          <Location>
            <gml:pos srsName="EPSG:2154">655945.0 6865765.5</gml:pos>
          </Location>
        </Centroid>
      </StopPlace>
      XML
      expected_xml.strip!

      centroid = Netex::Point.new(location: Netex::Location.new(coordinates: GML::Pos.new(x: 655945.0, y: 6865765.5, srs_name: 'EPSG:2154')))
      stopPlace = Netex::StopPlace.new(centroid:  centroid)
      expect(to_xml(stopPlace)).to eq(expected_xml)
    end

    it "creates Quay collection tag" do
      expected_xml =<<~XML
      <StopPlace version="any">
        <quays>
          <Quay id="first" version="any"/>
          <Quay id="second" version="any"/>
        </quays>
      </StopPlace>
      XML
      expected_xml.strip!

      quays = %w{first second}.map { |id| Netex::Quay.new id: id }
      stopPlace = Netex::StopPlace.new(quays: quays)
      expect(to_xml(stopPlace)).to eq(expected_xml)
    end

    it "creates Entrance collection tag" do
      expected_xml =<<~XML
      <StopPlace version="any">
        <entrances>
          <StopPlaceEntranceRef ref="first"/>
          <StopPlaceEntranceRef ref="second"/>
        </entrances>
      </StopPlace>
      XML
      expected_xml.strip!

      references = %w{first second}.map { |id| Netex::Reference.new id, type: 'StopPlaceEntranceRef' }
      stopPlace = Netex::StopPlace.new(entrances: references)
      expect(to_xml(stopPlace)).to eq(expected_xml)
    end

    it "ignores empty PostalAddress" do 
      expected_xml = '<StopPlace version="any"/>'

      stopPlace = Netex::StopPlace.new(postal_address: Netex::PostalAddress.new(id: 'empty'))
      expect(to_xml(stopPlace)).to eq(expected_xml)
    end

  end

  describe "Line xml" do

    it "outputs id tag" do
      expected_xml = '<Line id="test" version="any"/>'
      expect(to_xml(Netex::Line.new(id: 'test'))).to eq(expected_xml)
    end

    it "creates Name tag" do
      expected_xml =<<~XML
      <Line version="any">
        <Name>test</Name>
      </Line>
      XML
      expected_xml.strip!

      expect(to_xml(Netex::Line.new(name: 'test'))).to eq(expected_xml)
    end

    it "creates Presentation tag" do
      expected_xml =<<~XML
      <Line version="any">
        <TransportMode>bus</TransportMode>
        <Presentation>
          <Colour>test</Colour>
        </Presentation>
      </Line>
      XML
      expected_xml.strip!

      line = Netex::Line.new(presentation: Netex::Presentation.new(colour: 'test'), transport_mode: 'bus')
      expect(to_xml(line)).to eq(expected_xml)
    end

    it "creates AlternativePresentation tag" do
      expected_xml =<<~XML
      <Line version="any">
        <AlternativePresentation>
          <Colour>test</Colour>
        </AlternativePresentation>
      </Line>
      XML
      expected_xml.strip!

      line = Netex::Line.new(alternative_presentation: Netex::Presentation.new(colour: 'test'))
      expect(to_xml(line)).to eq(expected_xml)
    end

    it "creates TransportMode tags" do
      expected_xml =<<~XML
      <Line version="any">
        <TransportMode>bus</TransportMode>
      </Line>
      XML
      expected_xml.strip!

      line = Netex::Line.new(transport_mode: 'bus')
      expect(to_xml(line)).to eq(expected_xml)
    end

    context 'when transport mode is bus' do
      it "creates BusSubMode tag" do
        expected_xml =<<~XML
        <Line version="any">
          <TransportMode>bus</TransportMode>
          <TransportSubmode>
            <BusSubmode>regionalBus</BusSubmode>
          </TransportSubmode>
        </Line>
        XML
        expected_xml.strip!

        line = Netex::Line.new(transport_mode: 'bus', transport_submode: 'regionalBus')
        expect(to_xml(line)).to eq(expected_xml)
      end
    end

    context 'when transport mode is cableway' do
      it "creates TelecabinSubmode tag" do
        expected_xml =<<~XML
        <Line version="any">
          <TransportMode>cableway</TransportMode>
          <TransportSubmode>
            <TelecabinSubmode>cableCar</TelecabinSubmode>
          </TransportSubmode>
        </Line>
        XML
        expected_xml.strip!

        line = Netex::Line.new(transport_mode: 'cableway', transport_submode: 'cableCar')
        expect(to_xml(line)).to eq(expected_xml)
      end
    end

    it "creates OperatorRef tag" do
      expected_xml =<<~XML
      <Line version="any">
        <OperatorRef ref="test"/>
      </Line>
      XML
      expected_xml.strip!

      line = Netex::Line.new(operator_ref: Netex::Reference.new("test", type: 'Operator'))
      expect(to_xml(line)).to eq(expected_xml)
    end

    it "creates additionalOperators tag" do
      expected_xml =<<~XML
      <Line version="any">
        <additionalOperators>
          <OperatorRef ref="first"/>
          <OperatorRef ref="second"/>
        </additionalOperators>
      </Line>
      XML
      expected_xml.strip!

      additional_operators = [
        Netex::Reference.new("first", type: 'Operator'),
        Netex::Reference.new("second", type: 'Operator')
      ]
      line = Netex::Line.new(additional_operators: additional_operators)
      expect(to_xml(line)).to eq(expected_xml)
    end

    it "creates noticeAssignments tag" do
      expected_xml =<<~XML
      <Line version="any">
        <noticeAssignments>
          <NoticeAssignment id="first" version="any" order="0">
            <NoticeRef ref="first"/>
          </NoticeAssignment>
          <NoticeAssignment id="second" version="any" order="1">
            <NoticeRef ref="second"/>
          </NoticeAssignment>
        </noticeAssignments>
      </Line>
      XML
      expected_xml.strip!

      notice_assignments = []
      %w{first second}.each_with_index do |id, order|
        notice_assignments << Netex::NoticeAssignment.new(
          {
            id: id,
            order: order,
            notice_ref: Netex::Reference.new(id, type: 'Operator')
          }
        )
      end
      line = Netex::Line.new(notice_assignments: notice_assignments)
      expect(to_xml(line)).to eq(expected_xml)
    end

  end

  describe 'Notice xml' do
    it 'creates TypeOfNoticeRef' do
      expected_xml =<<~XML
      <Notice version="any">
        <TypeOfNoticeRef ref="test"/>
      </Notice>
      XML

      notice = Netex::Notice.new(type_of_notice_ref: Netex::Reference.new('test', type: String))
      expect(to_xml(notice)).to be_same_xml(expected_xml)
    end
  end

  describe "ServiceJourneyPattern xml" do

    it "create a ServiceJourneyPattern tag" do
      expected_xml = <<~XML
<ServiceJourneyPattern version="any">
  <Name>Test</Name>
  <RouteRef ref="route:test"/>
  <DestinationDisplayRef ref="destination-display:test"/>
  <pointsInSequence>
    <StopPointInJourneyPattern id="point:1" version="any" order="1">
      <ScheduledStopPointRef ref="scheduled-stop-point:1"/>
      <ForAlighting>true</ForAlighting>
      <ForBoarding>true</ForBoarding>
    </StopPointInJourneyPattern>
    <StopPointInJourneyPattern id="point:2" version="any" order="2">
      <ScheduledStopPointRef ref="scheduled-stop-point:2"/>
      <ForAlighting>true</ForAlighting>
      <ForBoarding>true</ForBoarding>
    </StopPointInJourneyPattern>
  </pointsInSequence>
</ServiceJourneyPattern>
      XML
      expected_xml.strip!

      journey_pattern = Netex::ServiceJourneyPattern.new
      journey_pattern.name = "Test"
      journey_pattern.route_ref = Netex::Reference.new "route:test", type: "Route"
      journey_pattern.destination_display_ref =
        Netex::Reference.new "destination-display:test", type: "DestinationDisplay"

      journey_pattern.points_in_sequence = 2.times.map do |n|
        n = n+1
        Netex::StopPointInJourneyPattern.new(id: "point:#{n}", order: n).tap do |point|
          point.scheduled_stop_point_ref = Netex::Reference.new "scheduled-stop-point:#{n}", type: "ScheduledStopPoint"
          point.for_alighting = point.for_boarding = true
        end
      end

      expect(to_xml(journey_pattern)).to eq(expected_xml)
    end

  end

  describe "ServiceJourney xml" do

    it "create a ServiceJourney tag" do
      expected_xml = <<~XML
<ServiceJourney id="test" version="any">
  <DepartureTime>05:30:00</DepartureTime>
  <dayTypes>
    <DayTypeRef ref="weekday"/>
    <DayTypeRef ref="saturday"/>
  </dayTypes>
  <JourneyPatternRef ref="JourneyPattern:1"/>
  <PublicCode>ABCD</PublicCode>
  <trainNumbers>
    <TrainNumberRef ref="PACO"/>
  </trainNumbers>
  <passingTimes>
    <TimetabledPassingTime version="any" id="1">
      <StopPointInJourneyPatternRef ref="point:1"/>
      <DepartureTime>05:30:00</DepartureTime>
    </TimetabledPassingTime>
    <TimetabledPassingTime version="dummy" id="2">
      <StopPointInJourneyPatternRef ref="point:2"/>
      <ArrivalTime>05:31:00</ArrivalTime>
      <DepartureTime>05:32:00</DepartureTime>
      <DepartureDayOffset>1</DepartureDayOffset>
    </TimetabledPassingTime>
    <TimetabledPassingTime version="any" id="3">
      <StopPointInJourneyPatternRef ref="point:3"/>
      <ArrivalTime>05:33:00</ArrivalTime>
      <ArrivalDayOffset>1</ArrivalDayOffset>
    </TimetabledPassingTime>
  </passingTimes>
</ServiceJourney>
      XML
      expected_xml.strip!

      service_journey = Netex::ServiceJourney.new(id: 'test')
      service_journey.departure_time = Netex::Time.new(5, 30)

      service_journey.day_types = []
      service_journey.day_types << Netex::Reference.new('weekday', type: 'DayType')
      service_journey.day_types << Netex::Reference.new('saturday', type: 'DayType')

      service_journey.journey_pattern_ref = Netex::Reference.new('JourneyPattern:1', type: 'JourneyPattern')

      service_journey.public_code = "ABCD"
      service_journey.train_numbers << Netex::Reference.new('PACO', type: String)

      service_journey.passing_times = []
      service_journey.passing_times << Netex::TimetabledPassingTime.new.tap do |passing_time|
        passing_time.id = '1'
        passing_time.departure_time = Netex::Time.new(5, 30)
        passing_time.stop_point_in_journey_pattern_ref = Netex::Reference.new('point:1', type: 'StopPointInJourneyPattern')
      end
      service_journey.passing_times << Netex::TimetabledPassingTime.new.tap do |passing_time|
        passing_time.id = '2'
        passing_time.version = 'dummy'

        passing_time.arrival_time = Netex::Time.new(5, 31)
        passing_time.departure_time = Netex::Time.new(5, 32)
        passing_time.departure_day_offset = 1
        passing_time.stop_point_in_journey_pattern_ref = Netex::Reference.new('point:2', type: 'StopPointInJourneyPattern')
      end
      service_journey.passing_times << Netex::TimetabledPassingTime.new.tap do |passing_time|
        passing_time.id = '3'
        passing_time.arrival_time = Netex::Time.new(5, 33)
        passing_time.arrival_day_offset = 1
        passing_time.stop_point_in_journey_pattern_ref = Netex::Reference.new('point:3', type: 'StopPointInJourneyPattern')
      end

      expect(to_xml(service_journey)).to eq(expected_xml)
    end

  end


  describe "OperatingPeriod xml" do

    let(:operating_period) { Netex::OperatingPeriod.new }

    context "when from/to dates are Ruby Times" do

      before do
        operating_period.from_date = Time.parse('2030-01-01 00:00:00+0100')
        operating_period.to_date = Time.parse('2030-12-31 23:59:59+0100')
      end

      it "formats From/To XML DateTimes with UTC (Z) format" do
        expected_xml = <<~XML
<OperatingPeriod version="any">
  <FromDate>2029-12-31T23:00:00Z</FromDate>
  <ToDate>2030-12-31T22:59:59Z</ToDate>
</OperatingPeriod>
        XML
        expected_xml.strip!

        expect(to_xml(operating_period)).to eq(expected_xml)
      end

    end

    context "when from/to dates are Ruby Dates" do

      before do
        operating_period.from_date = Date.parse('2030-01-01')
        operating_period.to_date = Date.parse('2030-12-31')
      end

      it "formats From/To XML DateTimes with 00:00:00 time part" do
        expected_xml = <<~XML
<OperatingPeriod version="any">
  <FromDate>2030-01-01T00:00:00</FromDate>
  <ToDate>2031-01-01T00:00:00</ToDate>
</OperatingPeriod>
        XML
        expected_xml.strip!

        expect(to_xml(operating_period)).to eq(expected_xml)
      end

    end

  end

  describe "RoutingConstraintZone xml" do
    let(:routing_constraint_zone) { Netex::RoutingConstraintZone.new }

    it "create a RoutingConstraintZone tag" do
      expected_xml = <<~XML
<RoutingConstraintZone version="any">
  <Name>RoutingConstraintZone Example</Name>
  <members>
    <ScheduledStopPointRef ref="ScheduledStopPoint:A"/>
    <ScheduledStopPointRef ref="ScheduledStopPoint:B"/>
  </members>
  <ZoneUse>cannotBoardAndAlightInSameZone</ZoneUse>
  <lines>
    <LineRef ref="Line:1"/>
    <LineRef ref="Line:2"/>
  </lines>
</RoutingConstraintZone>
      XML
      expected_xml.strip!

      routing_constraint_zone.name = "RoutingConstraintZone Example"
      routing_constraint_zone.members = [
        Netex::Reference.new("ScheduledStopPoint:A", type: 'ScheduledStopPoint'),
        Netex::Reference.new("ScheduledStopPoint:B", type: 'ScheduledStopPoint')
      ]

      routing_constraint_zone.zone_use = 'cannotBoardAndAlightInSameZone'

      routing_constraint_zone.lines = [
        Netex::Reference.new("Line:1", type: 'Line'),
        Netex::Reference.new("Line:2", type: 'Line')
      ]

      expect(to_xml(routing_constraint_zone)).to eq(expected_xml)
    end

  end

  describe "StopPlaceEntrance xml" do
    let(:entrance) { Netex::StopPlaceEntrance.new }

    it "create a RoutingConstraintZone tag" do
      expected_xml = <<~XML
<StopPlaceEntrance version="any">
  <Name>StopPlaceEntrance example</Name>
  <ShortName>42</ShortName>
  <Description>Dummy</Description>
  <Centroid version="any">
    <Location>
      <Longitude>2.2922926</Longitude>
      <Latitude>48.8583736</Latitude>
    </Location>
  </Centroid>
  <PostalAddress version="any">
    <Town>Palaiseau</Town>
    <PostalRegion>91477</PostalRegion>
  </PostalAddress>
  <EntranceType>Opening</EntranceType>
  <IsEntry>true</IsEntry>
  <IsExit>true</IsExit>
</StopPlaceEntrance>
      XML
      expected_xml.strip!

      entrance.name = "StopPlaceEntrance example"
      entrance.short_name = "42"
      entrance.description = "Dummy"

      entrance.centroid = Netex::Point.new(
        location: Netex::Location.new(
          latitude: 48.8583736,
          longitude: 2.2922926)
      )

      entrance.postal_address =
        Netex::PostalAddress.new(
          town: "Palaiseau",
          postal_region: "91477")
      entrance.entrance_type = "Opening"
      entrance.is_entry = true
      entrance.is_exit = true

      expect(to_xml(entrance)).to eq(expected_xml)
    end
  end

  describe "DayTypeAssignment xml" do
    let(:assignment) { Netex::DayTypeAssignment.new }

    it "create a DayTypeAssignment tag" do
      expected_xml = <<~XML
<DayTypeAssignment id="test" version="any" order="0">
  <Date>2007-06-04</Date>
  <DayTypeRef ref="chouette:TimeTable:7a9f4e22-0ddc-487e-81eb-fe5b50b52de9:LOC"/>
  <isAvailable>false</isAvailable>
</DayTypeAssignment>
      XML
      expected_xml.strip!

      assignment.id = "test"
      assignment.order = 0
      assignment.date = Date.parse("2007-06-04")
      assignment.day_type_ref = Netex::Reference.new("chouette:TimeTable:7a9f4e22-0ddc-487e-81eb-fe5b50b52de9:LOC", type: 'DayType')
      assignment.is_available = false

      expect(to_xml(assignment)).to eq(expected_xml)
    end

  end

  describe "VehicleJourneyStopAssignment xml" do
    let(:assignment) { Netex::VehicleJourneyStopAssignment.new }

    it "create a VehicleJourneyStopAssignment tag" do
      expected_xml = <<~XML
<VehicleJourneyStopAssignment id="test" version="any">
  <ScheduledStopPointRef ref="scheduled_stop_point-1"/>
  <StopPlaceRef ref="stop_place-1"/>
  <QuayRef ref="quay-1"/>
  <VehicleJourneyRef ref="vehicule_journey-1"/>
  <VehicleJourneyRef ref="vehicule_journey-2" version="any"/>
</VehicleJourneyStopAssignment>
      XML
      expected_xml.strip!

      assignment.id = "test"
      assignment.scheduled_stop_point_ref = Netex::Reference.new("scheduled_stop_point-1", type: Netex::ScheduledStopPoint)
      assignment.quay_ref = Netex::Reference.new("quay-1", type: Netex::Quay)
      assignment.stop_place_ref = Netex::Reference.new("stop_place-1", type: Netex::StopPlace)
      assignment.vehicle_journey_refs << Netex::Reference.new("vehicule_journey-1", type: Netex::ServiceJourney)
      assignment.vehicle_journey_refs << Netex::Reference.new("vehicule_journey-2", type: Netex::ServiceJourney, local: true)

      expect(to_xml(assignment)).to eq(expected_xml)
    end

  end

  describe "Notice xml" do

    it "supports PublicCode" do
      expected_xml =<<~XML
      <Notice version="any">
        <PublicCode>test</PublicCode>
      </Notice>
      XML
      expected_xml.strip!

      expect(to_xml(Netex::Notice.new(public_code: 'test'))).to eq(expected_xml)
    end

  end

  describe ".builder_class_for" do
    subject { described_class.builder_class_for(resource) }

    context "when resource is a Netex::Presentation" do
      let(:resource) { Netex::Presentation.new }
      it { is_expected.to eq(Netex::Target::Builder) }
    end

    context "when resource is a Netex::Line" do
      let(:resource) { Netex::Line.new }
      it { is_expected.to eq(Netex::Target::Builder::Line) }
    end
    context "when resource is a Netex::StopPlace" do
      let(:resource) { Netex::StopPlace.new }
      it { is_expected.to eq(Netex::Target::Builder::StopPlace) }
    end
    context "when resource is a Netex::Quay" do
      let(:resource) { Netex::Quay.new }
      xit { is_expected.to eq(Netex::Target::Builder::StopPlace) }
    end

  end

  describe Netex::Target::Builder::TransportSubmode do
    describe '.submode_tag' do
      subject { described_class.submode_tag(transport_mode) }

      context 'for transport mode "bus"' do
        let(:transport_mode) { 'bus' }
        it { is_expected.to eq('BusSubmode') }
      end

      context 'for transport mode "cableway"' do
        let(:transport_mode) { 'cableway' }
        it { is_expected.to eq('TelecabinSubmode') }
      end
    end
  end

end
