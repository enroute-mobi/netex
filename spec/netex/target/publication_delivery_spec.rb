require 'spec_helper'

RSpec.describe Netex::Target::PublicationDelivery do

  subject(:publication_delivery) { Netex::Target::PublicationDelivery.new }

  describe '#to_xml_element' do

    subject { Ox.dump(publication_delivery.to_xml_element, encoding: "UTF-8").strip }

    context 'when participant_ref is "dummy"' do
      before { publication_delivery.participant_ref = 'dummy' }

      let(:expected_xml) do
        <<~XML
        <PublicationDelivery xmlns="http://www.netex.org.uk/netex">
          <ParticipantRef>dummy</ParticipantRef>
        </PublicationDelivery>
        XML
      end

      it { is_expected.to be_same_xml(expected_xml) }
    end

    context 'when participant_ref is not a string (like 42)' do
      before { publication_delivery.participant_ref = 42 }

      let(:expected_xml) do
        <<~XML
        <PublicationDelivery xmlns="http://www.netex.org.uk/netex">
          <ParticipantRef>42</ParticipantRef>
        </PublicationDelivery>
        XML
      end

      it { is_expected.to be_same_xml(expected_xml) }
    end

  end

end
