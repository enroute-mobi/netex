require "spec_helper"

RSpec.describe Netex::Target::ReferenceScope::Base do
  describe "#mark_local_references" do
    let(:reference) { Netex::Reference.new 'dummy', type: 'Quay' }
    let(:resource) { double "Resource", references: [ reference ] }

    it "makes local the given reference if it has been seen" do
      allow(subject).to receive(:local?).with(reference).and_return(true)
      expect { subject.mark_local_references(resource) }.to change(reference, :local?).to(true)
    end
  end
end

RSpec.describe Netex::Target::ReferenceScope::ByClass do
  let(:scope) { Netex::Target::ReferenceScope::ByClass.new }

  describe "#local?" do
    subject { scope.local? reference }
    let(:reference) { Netex::Reference.new 'dummy', type: 'Quay' }

    context "when a Resource with the Reference class/type has been seen" do
      let(:resource) { Netex::Quay.new }
      before { scope.view resource }

      it { is_expected.to be(true) }
    end

    context "when no Resource with the Reference class/type has been seen" do
      it { is_expected.to be(false) }
    end
  end

  describe "#view" do
    context "when the Resource can referencable elements" do
      let(:resource) do
        Netex::ServiceJourneyPattern.new(
          points_in_sequence: [
            Netex::StopPointInJourneyPattern.new
          ]
        )
      end

      let(:service_journey_pattern_ref) { Netex::Reference.new 'dummy', type: 'ServiceJourneyPattern' }
      let(:stop_point_in_journey_pattern_ref) { Netex::Reference.new 'dummy', type: 'StopPointInJourneyPattern' }

      it "makes local the ServiceJourneyPattern" do
        expect { scope.view(resource) }.to change { scope.local?(service_journey_pattern_ref) }
      end

      it "makes local each StopPointInJourneyPattern" do
        expect { scope.view(resource) }.to change { scope.local?(stop_point_in_journey_pattern_ref) }
      end
    end

  end
end
