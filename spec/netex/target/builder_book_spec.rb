require "spec_helper"

RSpec.describe Netex::Target::BuilderBook do

  describe "#to_xml" do

    it "selects a custom Builder when needed" do
      resource = Netex::OperatingPeriod.new from_date: Date.parse('2030-01-01')

      expected_xml = <<~XML
<OperatingPeriod version="any">
  <FromDate>2030-01-01T00:00:00</FromDate>
</OperatingPeriod>
      XML
      expected_xml.strip!

      expect(subject.to_xml(resource)).to eq(expected_xml)
    end

    it 'defines the Resource #raw_xml with XML payload' do
      resource = Netex::OperatingPeriod.new from_date: Date.parse('2030-01-01')
      result = subject.to_xml(resource)
      expect(resource.raw_xml).to eq(result)
    end

  end

end
