require "spec_helper"

RSpec.describe Netex::Target::Fragment do

  describe Netex::Target::Fragment::Mark do
    def create_mark(id = nil)
      Netex::Target::Fragment::Mark.new id
    end
    def parse(line)
      Netex::Target::Fragment::Mark.parse line
    end

    subject(:mark) { create_mark }

    describe "by default" do
      it "the identifier is the Mark object_id" do
        expect(mark.send(:identifier)).to eq(mark.object_id)
      end
    end

    describe "#to_text" do
      it "returns a text which can be included in file" do
        expect(create_mark(0).to_text).to eq("Netex::Document Fragment Mark [0]")
      end
    end

    describe ".parse" do
      it "returns nil if the line doesn't match a mark text" do
        expect(parse("<StopPlace>")).to be_nil
      end

      it "returns a Mark instance if the line matchs a mark text" do
        mark = parse("Netex::Document Fragment Mark [42]")
        expect(mark).to_not be_nil
        expect(mark.send(:identifier)).to eq(42)
      end

      it "returns an equivalent Mark when parsing a Mark text" do
        expect(parse(mark.to_text)).to eq(mark)
      end

    end

  end

end
