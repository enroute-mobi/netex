require "spec_helper"

RSpec.describe Netex::Target::Frame do

  subject(:frame) { Netex::Target::Frame.new }

  describe "sections" do

    describe "new section" do
      subject(:new_section) { frame.sections.create }

      it "uses by default the Frame builder book" do
        is_expected.to have_attributes(builder_book: frame.builder_book)
      end
    end
  end
end
