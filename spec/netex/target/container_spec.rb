require "spec_helper"

RSpec.describe Netex::Target::Container::Base do

  describe '#open_output' do

    context 'when the output is an IO' do
      let(:output) { StringIO.new }
      let(:container) { Netex::Target::Container::Base.new output }

      it 'invokes the block with the output' do
        expect { |b| container.open_output(&b) }.to yield_with_args(output)
      end
    end

    context 'when the output is a String (a filename)' do
      let(:tempfile) { Tempfile.new.tap { |f| f.close } }
      let(:output) { tempfile.path }
      let(:container) { Netex::Target::Container::Base.new output }
      let(:expected_content) { 'test' }

      it 'invokes the block with a File to write into the output filename' do
        container.open_output { |io| io.write expected_content }
        expect(File.read(output)).to eq(expected_content)
      end
    end

  end

end

RSpec.describe Netex::Target::Container::XML do

  let(:output) { StringIO.new }
  subject(:container) { Netex::Target::Container::XML.new output }

  let(:document) { double }
  before { container.documents = [document] }

  describe "#single_document" do

    it "returns the first document" do
      expect(container.single_document).to be(document)
    end

    it "raises an error when several documents are present" do
      container.documents = [double, double]
      expect { container.single_document }.to raise_error(RuntimeError)
    end

    it "raises an error when no document is present" do
      container.documents = []
      expect { container.single_document }.to raise_error(RuntimeError)
    end

  end

  describe "#write" do

    let(:document_content) { 'test' }

    before do
      allow(document).to receive(:write) do |io|
        io.write document_content
      end
    end

    it "write the single document in the container output" do
      container.write
      expect(output.string).to eq(document_content)
    end

  end

end

RSpec.describe Netex::Target::Container::Zip do


  describe "#write" do

    let(:zip_output) { Zip::Test.new }
    subject(:container) { Netex::Target::Container::Zip.new zip_output.buffer }

    let(:document) { double filename: 'test.xml' }
    before { container.documents = [document] }

    let(:document_content) { 'test' }

    before do
      allow(document).to receive(:write) do |io|
        io.write document_content
      end
    end

    it "write the document in a Zip entry" do
      container.write
      expect(zip_output.content(document.filename)).to eq(document_content)
    end

  end

  context "when using a Tempfile" do

    subject(:temp_file) { Tempfile.new }
    subject(:container) { Netex::Target::Container::Zip.new temp_file }

    before do
      container.documents = 10.times.map do |n|
         Netex::Target::Document.new(filename: "test-#{n}.xml")
      end
    end

    it "should create a valid ZIP file" do
      container.write

      temp_file.close

      # If the Zip OutputStream wasn't correctly flush,
      # the resulted zip file is invalid and can't be read by Zip::File
      expect(Zip::File.new(temp_file.path).entries).to have_attributes(size: container.documents.size)
    end

  end

end
