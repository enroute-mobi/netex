# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Netex::Resource do
  describe '.for' do
    subject { described_class.for(name) }

    context "when name is 'Quay'" do
      let(:name) { 'Quay' }
      it { is_expected.to eq(Netex::Quay) }
    end
    context "when name is 'Line'" do
      let(:name) { 'Line' }
      it { is_expected.to eq(Netex::Line) }
    end
    context "when name is 'dummy'" do
      let(:name) { 'dummy' }
      it { is_expected.to be_nil }
    end
  end

  describe '.resource_class' do
    subject { described_class.resource_class(name) }

    context "when name is 'Quay'" do
      let(:name) { 'Quay' }
      it { is_expected.to eq(Netex::Quay) }
    end
    context "when name is 'Line'" do
      let(:name) { 'Line' }
      it { is_expected.to eq(Netex::Line) }
    end
    context "when name is 'Frame'" do
      let(:name) { 'Frame' }
      it { is_expected.to eq(Netex::Frame) }
    end
    context "when name is 'dummy'" do
      let(:name) { 'dummy' }
      it { is_expected.to be_nil }
    end
    context "when name is 'Source'" do
      let(:name) { 'Source' }
      it { is_expected.to be_nil }
    end
  end

  describe '#tags' do
    it 'returns empty tag collection by default' do
      expect(Netex::Resource.new.tags).to be_empty
    end

    it 'allows to define a new tag' do
      subject.tags[:test] = 'ok'
      expect(subject.tags).to eq(test: 'ok')
    end

    it 'returns all defined tags' do
      subject.tags[:first] = 1
      subject.tags[:second] = 2
      subject.tags[:third] = 3
      expect(subject.tags).to eq(first: 1, second: 2, third: 3)
    end
  end

  describe '#tag' do
    it 'returns when the tag is defined' do
      expect(subject.tag(:dummy)).to be_nil
    end

    it 'returns the value of a defined tag' do
      subject.tags[:test] = 'value'
      expect(subject.tag(:test)).to eq('value')
    end

    it 'transforms key as symbol' do
      subject.tags[:test] = 'value'
      expect(subject.tag('test')).to eq('value')
    end
  end

  describe '#has_tag?' do
    let(:tag) { :test }
    subject { resource.has_tag?(tag) }

    context 'when the Resource has a value for the given tag' do
      let(:resource) { Netex::Resource.new tags: { tag => 'present' } }

      it { is_expected.to be_truthy }
    end

    context 'when the Resource has a nil value for the given tag' do
      let(:resource) { Netex::Resource.new tags: { tag => nil } }

      it { is_expected.to be_falsy }
    end

    context "when the Resource hasn't a value for the given tag" do
      let(:resource) { Netex::Resource.new tags: { other: 'dummy' } }

      it { is_expected.to be_falsy }
    end
  end

  describe '#with_tag' do
    let(:tag) { :test }
    let(:resource) { Netex::Resource.new }

    it 'defines the specified tag' do
      expect { resource.with_tag(tag => 'dummy') }.to change(resource, :tags).from({}).to(tag => 'dummy')
    end

    context 'when the given value is nil' do
      it 'ignores the tag' do
        expect { resource.with_tag(tag => nil) }.to_not change(resource, :tags).from({})
      end
    end
  end

  describe '#reference_contents' do
    it 'provides all Contents of the Resource which contains References' do
      expected = %i[operator_ref additional_operators represented_by_group_ref type_of_line_ref routes]
      expect(Netex::Line.reference_contents.map(&:name)).to eq(expected)
    end
  end

  describe '#references' do
    it 'provides all References defined in the resource' do
      line = Netex::Line.new
      line.operator_ref = Netex::Reference.new('dummy', type: 'Operator')
      line.additional_operators = [Netex::Reference.new('second', type: 'Operator')]

      expect(line.references).to match_array(line.additional_operators + [line.operator_ref])
    end

    context 'when resource is Route' do
      subject(:references) { route.references }
      let(:route) do
        Netex::Route.new(
          line_ref: Netex::Reference.new('line_id', type: 'Line'),
          direction_ref: Netex::Reference.new('direction_id', type: 'Direction'),
          inverse_route_ref: Netex::Reference.new('inverse_route_id', type: 'Route'),
          points_in_sequence: [
            Netex::PointOnRoute.new(route_point_ref: Netex::Reference.new('route_point_id', type: 'RoutePoint'))
          ]
        )
      end

      it 'includes the LineRef' do
        is_expected.to include(route.line_ref)
      end

      it 'includes the DirectionRef' do
        is_expected.to include(route.direction_ref)
      end

      it 'includes the InverseRouteRef' do
        is_expected.to include(route.inverse_route_ref)
      end

      it 'includes the RoutePointRef of each PointOnRoute' do
        is_expected.to include(*route.points_in_sequence.map(&:route_point_ref))
      end
    end

    context 'when resource is RoutePoint' do
      subject(:references) { route_point.references }
      let(:route_point) do
        Netex::RoutePoint.new(
          projections: [
            Netex::PointProjection.new(
              project_to_point_ref: Netex::Reference.new('point_id', type: 'Point')
            )
          ]
        )
      end

      it 'includes the ProjectToPointRef of each projection' do
        is_expected.to include(*route_point.projections.map(&:project_to_point_ref))
      end
    end

    context 'when resource is ServiceJourney' do
      subject(:references) { service_journey.references }
      let(:service_journey) do
        Netex::ServiceJourney.new(
          day_types: [Netex::Reference.new('day_type_id', type: 'DayType')],
          passing_times: [
            Netex::TimetabledPassingTime.new(
              stop_point_in_journey_pattern_ref: Netex::Reference.new(
                'stop_point_in_journey_pattern_id',
                type: 'StopPointInJourneyPattern'
              )
            )
          ]
        )
      end

      it 'includes the DayTypeRefs' do
        is_expected.to include(*service_journey.day_types)
      end
      it 'includes the StopPointInJourneyPatternRef of each TimetabledPassingTime' do
        is_expected.to include(*service_journey.passing_times.map(&:stop_point_in_journey_pattern_ref))
      end
    end

    context 'with StopPlace entrances' do
      self::XML = <<~XML
<StopPlace id="FR:StopPlace:IDFM_63923:" version="any">
  <Name>Viroflay Rive Gauche</Name>
  <Centroid>
    <Location>
      <gml:pos srsName="EPSG:2154">639143.5 6855957.500000002</gml:pos>
    </Location>
  </Centroid>
  <entrances>
    <StopPlaceEntrance id="FR:StopPlaceEntrance:IDFM_StopPlaceEntrance_50170311:" version="any">
      <Name>Entrée / Sortie</Name>
      <Centroid>
        <Location>
          <gml:pos srsName="EPSG:2154">639082 6856011</gml:pos>
        </Location>
      </Centroid>
      <IsEntry>true</IsEntry>
      <IsExit>true</IsExit>
    </StopPlaceEntrance>
    <StopPlaceEntrance id="FR:StopPlaceEntrance:IDFM_StopPlaceEntrance_50170312:" version="any">
      <Name>Entrée / Sortie</Name>
      <Centroid>
        <Location>
          <gml:pos srsName="EPSG:2154">639135 6855971.000000001</gml:pos>
        </Location>
      </Centroid>
      <IsEntry>true</IsEntry>
      <IsExit>true</IsExit>
    </StopPlaceEntrance>
    <StopPlaceEntrance id="FR:StopPlaceEntrance:IDFM_StopPlaceEntrance_50170880:" version="any">
      <Name>r. de Riesseuc</Name>
      <Centroid>
        <Location>
          <gml:pos srsName="EPSG:2154">639131 6855946.000000001</gml:pos>
        </Location>
      </Centroid>
      <IsEntry>true</IsEntry>
      <IsExit>true</IsExit>
    </StopPlaceEntrance>
    <StopPlaceEntrance id="FR:StopPlaceEntrance:IDFM_StopPlaceEntrance_50170881:" version="any">
      <Name>r. de Riesseuc</Name>
      <Centroid>
        <Location>
          <gml:pos srsName="EPSG:2154">639120 6855973.000000001</gml:pos>
        </Location>
      </Centroid>
      <IsEntry>true</IsEntry>
      <IsExit>true</IsExit>
    </StopPlaceEntrance>
  </entrances>
  <TransportMode>rail</TransportMode>
  <StopPlaceType>railStation</StopPlaceType>
</StopPlace>
      XML

      let(:xml) { self.class::XML }

      let(:source) { Netex::Source.new }
      before { source.parse(StringIO.new(xml)) }
      let(:stop_place) { source.stop_places.first }

      subject { stop_place.references }

      it { is_expected.to be_empty }
    end

  end

  describe '#referencables' do
    subject { resource.referencables }
    context 'when the Resource has no referencable element' do
      let(:resource) { Netex::Quay.new }

      it 'returns the Resource itself' do
        is_expected.to contain_exactly(resource)
      end
    end

    context 'when the Resource has referencable elements (for example ServiceJourneyPattern)' do
      let(:resource) do
        Netex::ServiceJourneyPattern.new(
          points_in_sequence: [
            Netex::StopPointInJourneyPattern.new,
            Netex::StopPointInJourneyPattern.new
          ]
        )
      end

      it 'includes the Resource itself (ServiceJourneyPattern)' do
        is_expected.to include(resource)
      end
      it 'includes the referencable elements (StopPointInJourneyPatterns)' do
        is_expected.to include(*resource.points_in_sequence)
      end
    end
  end

  describe 'embedded_resources' do
    subject { resource.embedded_resources }

    context 'when Resource is a Netex::Resource' do
      let(:resource) { Netex::Resource.new }
      it { is_expected.to be_empty }
    end

    context 'when Resource is a Netex::Site' do
      let(:resource) { Netex::Site.new }

      it 'includes Site entrances' do
        resource.entrances = [double]
        is_expected.to match_array(resource.entrances)
      end

      it 'excludes References' do
        resource.entrances = [Netex::Reference.new('test', type: 'StopPlaceEntrance')]
        is_expected.to be_empty
      end
    end

    context 'when Resource is a Netex::StopPlace' do
      let(:resource) { Netex::StopPlace.new }

      it 'includes StopPlace quays' do
        resource.quays = [double]
        is_expected.to match_array(resource.quays)
      end
    end
  end

  describe '#empty?' do
    subject { resource.empty? }

    context 'when the Resource only contains tags' do
      let(:resource) { Netex::Resource.new tags: { name: 'value' } }
      it { is_expected.to be_truthy }
    end

    context 'when the Resource only contains an id' do
      let(:resource) { Netex::Entity.new id: 'dummy' }
      it { is_expected.to be_truthy }
    end

    context 'when the Resource contains a defined element' do
      let(:resource) { Netex::EntityInVersion.new status: 'test' }
      it { is_expected.to be_falsy }
    end
  end

  describe Netex::Entity do
    subject { resource.respond_to? attribute }

    let(:resource) { described_class.new }

    describe '#created' do
      let(:attribute) { :created }

      it { is_expected.to be_truthy }
    end

    describe '#changed' do
      let(:attribute) { :changed }

      it { is_expected.to be_truthy }
    end
  end

  describe Netex::StopPlace do
    it 'has a version' do
      subject.version = 'dummy'
      expect(subject.version).to eq('dummy')
    end
  end

  describe Netex::PassengerStopAssignment do
    it 'has a scheduled stop point reference' do
      subject.scheduled_stop_point_ref = Netex::Reference.new('dummy', type: 'ScheduledStopPoint')
      expect(subject.scheduled_stop_point_ref.ref).to eq('dummy')
    end
  end
end

RSpec.describe Netex::Timeband do
  let(:resource) { Netex::Timeband.new }

  describe '#start_time' do
    subject { resource.start_time }
    context "when given start_time is '09:15'" do
      before { resource.start_time = '09:15' }
      it { is_expected.to have_attributes(hour: 9, minute: 15) }
    end
    context "when given start_time is ''" do
      before { resource.start_time = '' }
      it { is_expected.to be_nil }
    end
    context 'when given start_time is nil' do
      before { resource.start_time = nil }
      it { is_expected.to be_nil }
    end
    context "when given start_time is 'abc'", pending: 'Improve TimeOfDay parser' do
      before { resource.start_time = 'abc' }
      it { is_expected.to be_nil }
    end
  end

  describe '#end_time' do
    subject { resource.end_time }
    context "when given end_time is '09:15'" do
      before { resource.end_time = '09:15' }
      it { is_expected.to have_attributes(hour: 9, minute: 15) }
    end
    context "when given end_time is ''" do
      before { resource.end_time = '' }
      it { is_expected.to be_nil }
    end
    context 'when given end_time is nil' do
      before { resource.end_time = nil }
      it { is_expected.to be_nil }
    end
    context "when given end_time is 'abc'", pending: 'Improve TimeOfDay parser' do
      before { resource.end_time = 'abc' }
      it { is_expected.to be_nil }
    end
  end
end

RSpec.describe Netex::OperatingPeriod do
  subject(:resource) { Netex::OperatingPeriod.new }

  describe '#from_date' do
    subject { resource.from_date }

    context "when a String date '2030-01-15T00:00:00+0900' is provided" do
      before { resource.from_date = '2030-01-15T00:00:00+0900' }

      it { is_expected.to eq(Time.new(2030, 0o1, 15, 0, 0, 0, '+09:00')) }
    end
  end

  describe '#to_date' do
    subject { resource.to_date }

    context "when a String date '2030-01-15T00:00:00+0900' is provided" do
      before { resource.to_date = '2030-01-15T00:00:00+0900' }

      it { is_expected.to eq(Time.new(2030, 0o1, 15, 0, 0, 0, '+09:00')) }
    end
  end

  describe '#time_range' do
    subject { resource.time_range }

    context "when from_date and to_date are '2030-01-15T00:00:00+0100' and '2030-07-15T00:00:00+0200'" do
      before do
        resource.from_date = from
        resource.to_date = to
      end

      let(:from) { Time.parse('2030-01-15T00:00:00+0100') }
      let(:to) { Time.parse('2030-07-15T00:00:00+0200') }

      it { is_expected.to eq(from..to) }
    end

    context 'when from_date is not defined' do
      before { resource.to_date = to }

      let(:to) { Time.parse('2030-07-15T00:00:00+0200') }

      it { is_expected.to be_nil }
    end

    context 'when to_date is not defined' do
      before { resource.from_date = from }

      let(:from) { Time.parse('2030-01-15T00:00:00+0100') }

      it { is_expected.to be_nil }
    end
  end

  describe '#date_range' do
    subject { resource.date_range }

    context "when time_range is '2030-01-15T00:00:00+0100' and '2030-07-15T00:00:00+0200'" do
      before do
        time_range = Range.new(Time.parse(from_definition), Time.parse(to_definition))
        allow(resource).to receive(:time_range).and_return(time_range)
      end

      let(:from_definition) { '2030-01-15T00:00:00+0100' }
      let(:to_definition) { '2030-07-15T00:00:00+0200' }

      it { is_expected.to eq(Date.parse(from_definition)..Date.parse(to_definition)) }
    end

    context "when time_range isn't defined" do
      before { allow(resource).to receive(:time_range).and_return(nil) }
      it { is_expected.to be_nil }
    end
  end
end

RSpec.describe Netex::DayTypeAssignment do
  subject(:resource) { Netex::DayTypeAssignment.new }

  describe '#available?' do
    subject { resource.available? }

    context "when is_available is 'true'" do
      before { resource.is_available = 'true' }
      it { is_expected.to be_truthy }
    end

    context "when is_available is 'false'" do
      before { resource.is_available = 'false' }
      it { is_expected.to be_falsy }
    end

    context "when is_available isn't defined" do
      before { resource.is_available = nil }
      it { is_expected.to be_falsy }
    end
  end
end

RSpec.describe Netex::Place do
  subject(:place) { Netex::Place.new }

  describe '#latitude=' do
    it { expect { place.latitude = 42 }.to change(place, :latitude).from(nil).to(42) }
  end
  describe '#longitude=' do
    it { expect { place.longitude = 42 }.to change(place, :longitude).from(nil).to(42) }
  end
end
