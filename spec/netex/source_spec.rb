# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Netex::Source do
  def self.with_xml(xml, &block)
    context "when xml is '#{xml}'" do
      let(:xml) { xml }
      class_exec(&block)
    end
  end

  subject(:source) { Netex::Source::Base.new }

  describe '.file_type' do
    subject { Netex::Source.file_type filename }

    context 'when the given filename is a ZIP file' do
      let(:filename) { 'spec/fixtures/sample.zip' }
      it { is_expected.to eq(:zip) }
    end

    context 'when the given filename is an XML file' do
      let(:filename) { 'spec/fixtures/sample.xml' }
      it { is_expected.to eq(:xml) }
    end
  end

  describe Netex::Source::Mapper::Base do
    describe '.underscore' do
      {
        'Town' => 'town',
        'ContactDetails' => 'contact_details',
        'AddressLine1' => 'address_line_1'
      }.each do |from, to|
        it "transforms '#{from}' into '#{to}'" do
          expect(Netex::Source::Mapper::Base.underscore(from)).to eq(to)
        end
      end
    end
  end

  describe 'include raw xml' do
    let(:xml) do
      %(<Line id='test'><Name>Test</Name><noticeAssignments><NoticeAssignmentView id='1' order='0'><MarkUrl>https://example.com?arg1=1&amp;arg2=2</MarkUrl></NoticeAssignmentView></noticeAssignments></Line>)
    end

    let(:resource) do
      source.parse(StringIO.new(xml))
      source.resources.to_a.last
    end

    it "by default the returned Netex::Resources don't contain their XML raw definition" do
      expect(resource.raw_xml).to be_nil
    end

    context 'with include_raw_xml option' do
      subject { resource.raw_xml }

      before { source.include_raw_xml = true }

      it 'the returned Netex::Resources contain their XML raw definition' do
        is_expected.to eq(xml)
      end

      context 'when XML contains an unsupported tag' do
        let(:xml) do
          '<Quay><dummy>Test</dummy></Quay>'
        end

        it 'the returned Netex::Resources contain their XML raw definition' do
          is_expected.to eq(xml)
        end
      end

      context 'when XML describes a StopPlace' do
        let(:xml) do
          <<~XML
            <StopPlace dataSourceRef="FR1-ARRET_AUTO" version="1105523-1105404" created="2014-12-29T01:00:00Z" changed="2019-04-24T12:42:54Z" id="FR::monomodalStopPlace:47009:FR1">
              <keyList>
                <KeyValue typeOfKey="ALTERNATE_IDENTIFIER">
                  <Key>TLB_LIST_ID</Key>
                  <Value>59_180701</Value>
                </KeyValue>
              </keyList>
              <Name>Palaiseau</Name>
              <Centroid>
                <Location>
                  <gml:pos srsName="EPSG:2154">644498.1000000015 6846568.579599999</gml:pos>
                </Location>
              </Centroid>
              <PostalAddress version="any" id="FR1:PostalAddress:47009:">
                <Town>Palaiseau</Town>
                <PostalRegion>91477</PostalRegion>
              </PostalAddress>
              <ParentSiteRef ref="FR::multimodalStopPlace:63175:FR1" />
              <entrances>
                <StopPlaceEntranceRef ref="FR::StopPlaceEntrance:476153:FR1" />
                <StopPlaceEntranceRef ref="FR::StopPlaceEntrance:476150:FR1" />
              </entrances>
              <StopPlaceType>railStation</StopPlaceType>
            </StopPlace>
          XML
        end

        let(:recorded_xml) do
          %(
            <StopPlace dataSourceRef='FR1-ARRET_AUTO' version='1105523-1105404' created='2014-12-29T01:00:00Z' changed='2019-04-24T12:42:54Z' id='FR::monomodalStopPlace:47009:FR1'>
              <keyList><KeyValue typeOfKey='ALTERNATE_IDENTIFIER'><Key>TLB_LIST_ID</Key><Value>59_180701</Value></KeyValue></keyList>
              <Name>Palaiseau</Name>
              <Centroid><Location><gml:pos srsName='EPSG:2154'>644498.1000000015 6846568.579599999</gml:pos></Location></Centroid>
              <PostalAddress version='any' id='FR1:PostalAddress:47009:'><Town>Palaiseau</Town><PostalRegion>91477</PostalRegion></PostalAddress>
              <ParentSiteRef ref='FR::multimodalStopPlace:63175:FR1'></ParentSiteRef>
              <entrances>
                <StopPlaceEntranceRef ref='FR::StopPlaceEntrance:476153:FR1'></StopPlaceEntranceRef>
                <StopPlaceEntranceRef ref='FR::StopPlaceEntrance:476150:FR1'></StopPlaceEntranceRef>
              </entrances>
              <StopPlaceType>railStation</StopPlaceType>
            </StopPlace>
          )
        end

        def normalized_xml(xml)
          xml.gsub(/\\n/, '').gsub(/>\s*/, '>').gsub(/\s*</, '<').gsub('"', "'")
        end

        it 'the returned Netex::Resources contain their XML raw definition' do
          is_expected.to eq(normalized_xml(recorded_xml))
        end
      end
    end
  end

  describe 'Empty Resource' do
    context "when parsing '<StopPlace></StopPlace>'" do
      let(:xml) do
        '<StopPlace></StopPlace>'
      end

      subject do
        source.parse(StringIO.new(xml))
        source.stop_places.first
      end

      it 'reads the defined resource without error' do
        is_expected.to an_object_having_attributes(id: nil)
      end
    end
  end

  describe 'key list' do
    context "when parsing
          <keyList>
            <KeyValue><Key>key1</Key><Value>value1</Value></KeyValue>
            <KeyValue><Key>key2</Key><Value>value2</Value></KeyValue>
          </keyList>" do
      let(:xml) do
        %(<StopPlace>
          <keyList>
            <KeyValue><Key>key1</Key><Value>value1</Value></KeyValue>
            <KeyValue><Key>key2</Key><Value>value2</Value></KeyValue>
          </keyList>
        </StopPlace>)
      end

      let(:resource) do
        source.parse(StringIO.new(xml))
        source.stop_places.first
      end

      subject { resource.key_list }

      it do
        is_expected.to contain_exactly(an_object_having_attributes(key: 'key1', value: 'value1'),
                                       an_object_having_attributes(key: 'key2', value: 'value2'))
      end
    end

    context "when parsing
          <keyList>
            <KeyValue typeOfKey='test'><Key>key</Key><Value>value</Value></KeyValue>
          </keyList>" do
      let(:xml) do
        %(<StopPlace>
          <keyList>
            <KeyValue typeOfKey="test"><Key>key1</Key><Value>value1</Value></KeyValue>
          </keyList>
        </StopPlace>)
      end

      let(:resource) do
        source.parse(StringIO.new(xml))
        source.stop_places.first
      end

      subject { resource.key_list }

      it { is_expected.to contain_exactly(an_object_having_attributes(type_of_key: 'test')) }
    end

    context 'when parsing <Unknown><keyList><KeyValue><Key>test</Key><Value>dummy</Value></KeyValue></keyList></Unknown>' do
      let(:xml) do
        %(<Unknown><keyList><KeyValue><Key>test</Key><Value>dummy</Value></KeyValue></keyList></Unknown>)
      end

      it { expect { source.parse(StringIO.new(xml)) }.to_not raise_error }
    end
  end

  def sample_file(type = 'xml')
    File.expand_path("../../fixtures/sample.#{type}", __FILE__)
  end

  context 'with the sample file' do
    let(:input) { File.new(sample_file) }

    before { source.parse(input) }

    it 'reads two StopPlaces' do
      expect(source.stop_places.count).to eq(2)
    end

    describe 'First StopPlace' do
      let(:stop_area) { source.stop_places.find('FR::multimodalStopPlace:424920:FR1') }

      expected_attributes = {
        name: 'Petits Ponts',
        version: '811108',
        transport_mode: 'tram',
        transport_submode: 'regionalTram'
      }

      expected_attributes.each do |attribute_name, value|
        it "#{attribute_name} is #{value.inspect}" do
          expect(stop_area.send(attribute_name)).to eq(value)
        end
      end
    end

    describe 'First Quay' do
      let(:stop_area) { source.quays.find('FR::Quay:10997:FR1') }

      expected_attributes = {
        name: 'Centre - Quai',
        version: '10997-851943',
        transport_mode: 'bus',
        transport_submode: 'regionalBus'
      }

      expected_attributes.each do |attribute_name, value|
        it "#{attribute_name} is #{value.inspect}" do
          expect(stop_area.send(attribute_name)).to eq(value)
        end
      end

      describe '#accessibility_assessment' do
        let(:accessibility_assessment) { stop_area.accessibility_assessment }

        describe '#mobility_impaired_access' do
          it { expect(accessibility_assessment.mobility_impaired_access).to eq('unknown') }
        end

        describe '#description' do
          it { expect(accessibility_assessment.description).to eq('Description Sample') }
        end

        describe '#accessibility_limitation' do
          let(:accessibility_limitation) { accessibility_assessment.limitations.first }

          expected_attributes = {
            wheelchair_access: 'true',
            step_free_access: 'false',
            escalator_free_access: 'true',
            lift_free_access: 'partial',
            audible_signals_available: 'partial',
            visual_signs_available: 'true'
          }

          expected_attributes.each do |attribute_name, value|
            it "#{attribute_name} is #{value.inspect}" do
              expect(accessibility_limitation.send(attribute_name)).to eq(value)
            end
          end
        end
      end
    end

    describe 'First Line' do
      let(:line) { source.lines.find('FR1:Line:C01642:') }

      expected_attributes = {
        name: 'N134',
        version: 'any',
        transport_mode: 'bus',
        transport_submode: 'nightBus',
        public_code: nil,
        private_code: '800987012'
      }

      expected_attributes.each do |attribute_name, value|
        it "#{attribute_name} is #{value.inspect}" do
          expect(line.send(attribute_name)).to eq(value)
        end
      end

      describe 'valid_between' do
        it "from_date is #{Time.parse('2014-07-16T00:00:00')}" do
          expect(line.valid_between.from_date).to eq(Time.parse('2014-07-16T00:00:00'))
        end

        it 'to_date is nil' do
          expect(line.valid_between.to_date).to be_nil
        end
      end

      it "operator_ref is a Reference to 'FR1:Operator:800:LOC'" do
        expect(line.operator_ref).to eq(Netex::Reference.new('FR1:Operator:800:LOC', type: Netex::Operator,
                                                                                     version: 'any'))
      end

      it "additional_operators is one Reference to 'FR1:Operator:800:LOC'" do
        expect(line.additional_operators).to eq([Netex::Reference.new('FR1:Operator:800:LOC', type: Netex::Operator,
                                                                                              version: 'any')])
      end

      it "represented_by_group_ref is a Reference to 'FR1:Network:55:LOC'" do
        expect(line.represented_by_group_ref).to eq(Netex::Reference.new('FR1:Network:55:LOC',
                                                                         type: Netex::GroupOfLine, version: 'any'))
      end

      describe 'presentation' do
        expected_attributes = {
          colour: 'aaaaaa',
          colour_name: 'RGB:170 170 170',
          text_colour: '000000'
        }

        expected_attributes.each do |attribute_name, value|
          it "#{attribute_name} is #{value.inspect}" do
            expect(line.presentation.send(attribute_name)).to eq(value)
          end
        end
      end

      describe 'alternative_presentation' do
        expected_attributes = {
          colour_name: 'CMYK:0 0 0 33',
          text_colour: '000000'
        }

        expected_attributes.each do |attribute_name, value|
          it "#{attribute_name} is #{value.inspect}" do
            expect(line.alternative_presentation.send(attribute_name)).to eq(value)
          end
        end
      end

      describe 'notice_assignments' do
        it 'contains a single NoticeAssignment' do
          expect(line.notice_assignments.size).to eq(1)
        end

        it "contains a NoticeAssignment with id='FR1:NoticeAssignment:C01642:', version='any' and order='0'" do
          expect(line.notice_assignments.first).to have_attributes(id: 'FR1:NoticeAssignment:C01642:', version: 'any',
                                                                   order: '0')
        end

        it "contains a NoticeAssignment with NoticeRef 'FR1:Notice:C01642:'" do
          expect(line.notice_assignments.first.notice_ref).to eq(Netex::Reference.new('FR1:Notice:C01642:',
                                                                                      type: Netex::Notice, version: 'any'))
        end
      end
    end

    describe 'First ServiceJourney' do
      subject(:service_journey) { source.service_journeys.first }
      it { is_expected.to have_attributes(name: '2338795-RESEAU2019-Multi_di-Dimanche-02') }

      describe 'day_types' do
        subject { service_journey.day_types }
        it { is_expected.to have_attributes(size: 2) }
      end

      describe 'passing_times' do
        subject { service_journey.passing_times }
        it { is_expected.to have_attributes(size: 3) }
      end
    end
  end

  describe 'Reference parsing' do
    subject(:reference) do
      source = Netex::Source.new
      source.parse(StringIO.new("<Route>#{xml}</Route"))
      source.routes.first.line_ref
    end

    with_xml '<LineRef ref="test"/>' do
      it { is_expected.to have_attributes(ref: 'test') }
    end
    with_xml '<LineRef ref="test" version="any"/>' do
      it { is_expected.to have_attributes(ref: 'test', version: 'any') }
    end
    with_xml '<LineRef ref="test" version="dummy"/>' do
      it { is_expected.to have_attributes(ref: 'test', version: 'dummy') }
    end
    with_xml '<LineRef ref="test">version="dummy"</LineRef>' do
      it { is_expected.to have_attributes(ref: 'test', version: 'dummy') }
    end
    with_xml '<LineRef ref="test" versionRef="dummy"/>' do
      it { is_expected.to have_attributes(ref: 'test', version_ref: 'dummy') }
    end
  end

  describe 'lang attribute' do
    subject(:text) do
      source = Netex::Source.new
      source.parse(StringIO.new("<Route>#{xml}</Route"))
      source.lines.first.name
    end

    with_xml '<Line><Name lang="de">dummy</Name></Line>' do
      it { is_expected.to have_attributes(language: 'de') }
    end
  end

  describe 'StopPlace parsing' do
    def parse(xml)
      source = Netex::Source.new
      source.parse(StringIO.new(xml))
      source.stop_places.first
    end

    it 'reads id attribute' do
      expected_value = 'test'
      stop_place = parse("<StopPlace id='#{expected_value}'/>")
      expect(stop_place.id).to eq(expected_value)
    end

    it 'reads <Name> tag' do
      expected_value = 'test'
      stop_place = parse("<StopPlace><Name>#{expected_value}</Name></StopPlace>")
      expect(stop_place.name).to eq(expected_value)
    end

    it 'reads <StopPlaceType> tag' do
      expected_value = 'test'
      stop_place = parse("<StopPlace><StopPlaceType>#{expected_value}</StopPlaceType></StopPlace>")
      expect(stop_place.stop_place_type).to eq(expected_value)
    end

    it 'reads a single <placeTypes/TypeOfPlaceRef> tag' do
      expected_value = 'test'
      stop_place = parse("<StopPlace><placeTypes><TypeOfPlaceRef ref='#{expected_value}'/></placeTypes></StopPlace>")
      expect(stop_place.type_of_place).to eq(expected_value)
    end

    it 'reads multiple <placeTypes/TypeOfPlaceRef> tags' do
      expected_values = %w[type1 type2]
      xml_types = expected_values.map { |type| "<TypeOfPlaceRef ref='#{type}'/>" }.join
      stop_place = parse("<StopPlace><placeTypes>#{xml_types}</placeTypes></StopPlace>")

      expect(stop_place.place_types.map(&:ref)).to eq(expected_values)
    end

    it 'reads <PostalAddress> tag' do
      expected_town = 'test'
      expected_region = '42'

      stop_place = parse("<StopPlace><PostalAddress><Town>#{expected_town}</Town><PostalRegion>#{expected_region}</PostalRegion></PostalAddress></StopPlace>")

      expect(stop_place.postal_address.town).to eq(expected_town)
      expect(stop_place.postal_address.postal_region).to eq(expected_region)
    end

    it 'reads <Centroid><Location> tag' do
      expected_x = 123.456
      expected_y = 456.123
      expected_srs_name = 'EPSG:2154'

      stop_place = parse("<StopPlace><Centroid><Location><gml:pos srsName='#{expected_srs_name}'>#{expected_x} #{expected_y}</gml:pos></Location></Centroid></StopPlace>")
      expect(stop_place.centroid.location.coordinates.x).to eq(expected_x)
      expect(stop_place.centroid.location.coordinates.y).to eq(expected_y)
      expect(stop_place.centroid.location.coordinates.srs_name).to eq(expected_srs_name)
    end

    it 'reads <Centroid><Location><Latitude/Longitude> tag' do
      expected_latitude = 42
      expected_longitude = 2

      stop_place = parse("<StopPlace><Centroid><Location><Longitude>#{expected_longitude}</Longitude><Latitude>#{expected_latitude}</Latitude></Location></Centroid></StopPlace>")

      expect(stop_place.latitude).to eq(expected_latitude)
      expect(stop_place.longitude).to eq(expected_longitude)
    end

    it 'reads empty <Centroid><Location><Latitude/Longitude> tag' do
      stop_place = parse('<StopPlace><Name>Dummy</Name><Centroid><Location><Longitude></Longitude><Latitude></Latitude></Location></Centroid></StopPlace>')

      expect(stop_place.latitude).to be_nil
      expect(stop_place.longitude).to be_nil
    end
  end

  describe 'Line parsing' do
    let(:source) do
      Netex::Source.new.tap { |source| source.parse(StringIO.new(xml)) }
    end

    with_xml '<Line><TransportMode>cableway</TransportMode><TransportSubmode><TelecabinSubmode>cableCar</TelecabinSubmode></TransportSubmode></Line>' do
      describe 'Line' do
        subject { source.lines.first }

        it { is_expected.to have_attributes(transport_mode: 'cableway') }
        it { is_expected.to have_attributes(transport_submode: 'cableCar') }
      end
    end

    with_xml '<Line><routes><RouteRef ref="dummy"/></routes></Line>' do
      describe 'Line' do
        subject { source.lines.first }

        it { is_expected.to have_attributes(routes: [ an_object_having_attributes(ref: "dummy") ]) }
      end
    end
  end

  describe 'Entrances parsing' do
    let(:source) do
      Netex::Source.new.tap { |source| source.parse(StringIO.new(xml)) }
    end

    with_xml '<StopPlaceEntrance id="orphan"/>' do
      describe 'Source stop_place_entrances' do
        subject { source.stop_place_entrances }

        it { is_expected.to contain_exactly(an_object_having_attributes(id: 'orphan')) }
      end
    end

    with_xml '<StopPlace id="parent"><entrances><StopPlaceEntrance id="child" version="test"></entrances></StopPlace>' do
      describe 'StopPlace entrances' do
        let(:stop_place) { source.stop_places.first }

        subject { stop_place.entrances }
        it { is_expected.to contain_exactly(an_object_having_attributes(id: 'child')) }
      end

      describe 'Source stop_place_entrances' do
        subject { source.stop_place_entrances }

        it { is_expected.to contain_exactly(an_object_having_attributes(id: 'child')) }
      end

      describe 'Entrance' do
        subject { source.stop_place_entrances.first }

        it { is_expected.to have_attributes(tags: a_hash_including(parent_id: 'parent')) }
      end
    end

    with_xml %(<StopPlaceEntrance id="child"/>
      <StopPlace id="parent"><entrances><StopPlaceEntranceRef ref="child"/></entrances></StopPlace>) do
      describe 'StopPlace entrances' do
        let(:stop_place) { source.stop_places.first }

        subject { stop_place.entrances }
        it { is_expected.to contain_exactly(an_object_having_attributes(ref: 'child')) }
      end

      describe 'Source stop_place_entrances' do
        subject { source.stop_place_entrances }

        it { is_expected.to contain_exactly(an_object_having_attributes(id: 'child')) }
      end

      describe 'Entrance' do
        subject { source.stop_place_entrances.first }

        it { is_expected.to have_attributes(tags: a_hash_including(parent_id: 'parent')) }
      end
    end

    with_xml %(<StopPlace id="parent"><entrances><StopPlaceEntranceRef ref="child"/></entrances></StopPlace>
      <StopPlaceEntrance id="child"/>) do
      describe 'StopPlace entrances' do
        let(:stop_place) { source.stop_places.first }

        subject { stop_place.entrances }
        it { is_expected.to contain_exactly(an_object_having_attributes(ref: 'child')) }
      end

      describe 'Source stop_place_entrances' do
        subject { source.stop_place_entrances }

        it { is_expected.to contain_exactly(an_object_having_attributes(id: 'child')) }
      end

      describe 'Entrance' do
        subject { source.stop_place_entrances.first }

        it { is_expected.to have_attributes(tags: a_hash_including(parent_id: 'parent')) }
      end
    end
  end

  describe 'Quays parsing' do
    let(:source) do
      Netex::Source.new.tap { |source| source.parse(StringIO.new(xml)) }
    end

    with_xml '<Quay id="orphan"/>' do
      describe 'Source quays' do
        subject { source.quays }

        it { is_expected.to contain_exactly(an_object_having_attributes(id: 'orphan')) }
      end
    end

    with_xml '<StopPlace id="parent"><quays><Quay id="child" version="test"/></quays></StopPlace>' do
      describe 'StopPlace quays' do
        let(:stop_place) { source.stop_places.first }

        subject { stop_place.quays }
        it { is_expected.to contain_exactly(an_object_having_attributes(id: 'child')) }
      end

      describe 'Source quays' do
        subject { source.quays }

        it { is_expected.to contain_exactly(an_object_having_attributes(id: 'child')) }
      end

      describe 'Quay' do
        subject { source.quays.first }

        it { is_expected.to have_attributes(tags: a_hash_including(parent_id: 'parent')) }
      end
    end
  end

  describe 'Operator parsing' do
    def parse(xml)
      source = Netex::Source.new
      source.parse(StringIO.new(xml))
      source.operators.first
    end

    it 'reads <Name> tag' do
      expected_value = 'test'
      operator = parse("<Operator><Name>#{expected_value}</Name></Operator>")
      expect(operator.name).to eq(expected_value)
    end

    it 'reads <ContactDetails> tag' do
      expected_contact_person = 'test'
      expected_email = 'contact@test'

      operator = parse("<Operator><ContactDetails><ContactPerson>#{expected_contact_person}</ContactPerson><Email>#{expected_email}</Email></ContactDetails></Operator>")

      expect(operator.contact_details.contact_person).to eq(expected_contact_person)
      expect(operator.contact_details.email).to eq(expected_email)
    end

    it 'reads <Address> tag' do
      expected_town = 'test'
      expected_post_code = '42'
      expected_address_line_1 = 'address line'

      operator = parse("<Operator><Address><AddressLine1>#{expected_address_line_1}</AddressLine1><Town>#{expected_town}</Town><PostCode>#{expected_post_code}</PostCode></Address></Operator>")

      expect(operator.address.address_line_1).to eq(expected_address_line_1)
      expect(operator.address.town).to eq(expected_town)
      expect(operator.address.post_code).to eq(expected_post_code)
    end
  end

  describe 'PassengerStopAssignment parsing' do
    subject(:passenger_stop_assignment) do
      source = Netex::Source.new
      source.parse(StringIO.new(xml))
      source.passenger_stop_assignments.first
    end

    with_xml '<PassengerStopAssignment id="test"/>' do
      it { is_expected.to have_attributes(id: 'test') }
    end
  end

  describe 'VehicleJourneyStopAssignment parsing' do
    subject(:assignment) do
      source = Netex::Source.new
      source.parse(StringIO.new(xml))
      source.vehicle_journey_stop_assignments.first
    end

    with_xml '<VehicleJourneyStopAssignment id="test"/>' do
      it { is_expected.to have_attributes(id: 'test') }
    end

    with_xml '<VehicleJourneyStopAssignment><VehicleJourneyRef ref="target"/></VehicleJourneyStopAssignment>' do
      let(:expected_vehicle_journey_ref) { an_object_having_attributes(ref: 'target') }

      it { is_expected.to have_attributes(vehicle_journey_refs: include(expected_vehicle_journey_ref)) }
    end
  end

  describe 'RoutingConstraintZone parsing' do
    subject(:routing_constraint_zone) do
      source = Netex::Source.new
      source.parse(StringIO.new(xml))
      source.routing_constraint_zones.first
    end

    with_xml '<RoutingConstraintZone id="test"/>' do
      it { is_expected.to have_attributes(id: 'test') }
    end

    with_xml '<RoutingConstraintZone><Name>dummy</Name></RoutingConstraintZone>' do
      it { is_expected.to have_attributes(name: 'dummy') }
    end

    with_xml '<RoutingConstraintZone><members><ScheduledStopPointRef ref="A"/></members></RoutingConstraintZone>' do
      it { is_expected.to have_attributes(members: containing_exactly(an_object_having_attributes(ref: 'A'))) }
    end

    with_xml '<RoutingConstraintZone><lines><LineRef ref="1"/></lines></RoutingConstraintZone>' do
      it { is_expected.to have_attributes(lines: containing_exactly(an_object_having_attributes(ref: '1'))) }
    end
  end

  describe 'DayType parsing' do
    let(:day_type) do
      source = Netex::Source.new
      source.parse(StringIO.new(xml))
      source.day_types.first
    end

    let(:xml) do
      <<~XML
        <DayType>
          <properties>
            <PropertyOfDay>
              <DaysOfWeek>Monday Tuesday</DaysOfWeek>
            </PropertyOfDay>
            <PropertyOfDay>
              <DaysOfWeek>Wednesday</DaysOfWeek>
            </PropertyOfDay>
            <PropertyOfDay>
              <DaysOfWeek>thursday friday</DaysOfWeek>
            </PropertyOfDay>
          </properties>
        </DayType>
      XML
    end

    describe '#monday' do
      subject { day_type.monday? }
      it { is_expected.to be_truthy }
    end

    describe '#tuesday' do
      subject { day_type.tuesday? }
      it { is_expected.to be_truthy }
    end

    describe '#wednesday' do
      subject { day_type.wednesday? }
      it { is_expected.to be_truthy }
    end

    describe '#thursday' do
      subject { day_type.thursday? }
      it { is_expected.to be_truthy }
    end

    describe '#friday' do
      subject { day_type.friday? }
      it { is_expected.to be_truthy }
    end

    describe '#saturday' do
      subject { day_type.saturday? }
      it { is_expected.to be_falsy }
    end

    describe '#sunday' do
      subject { day_type.sunday? }
      it { is_expected.to be_falsy }
    end
  end

  describe 'Frame parsing' do
    let(:frame) do
      source = Netex::Source.new
      source.parse(StringIO.new(xml))
      source.frames.first
    end

    with_xml "<GeneralFrame>
  <ValidBetween>
    <FromDate>2030-01-01T08:00:00</FromDate>
    <ToDate>2030-01-31T23:00:00</ToDate>
  </ValidBetween>
</GeneralFrame>" do
      describe 'ValidBetween' do
        subject { frame.valid_between }
        it do
          is_expected.to have_attributes(
            from_date: Time.parse('2030-01-01T08:00:00'),
            to_date: Time.parse('2030-01-31T23:00:00')
          )
        end
      end
    end

    with_xml '<GeneralFrame><TypeOfFrameRef ref="value"/></GeneralFrame>' do
      describe 'TypeOfFrameRef' do
        subject { frame.type_of_frame_ref }
        it { is_expected.to have_attributes(ref: 'value') }
      end
    end
  end

  context 'remove empty resources' do
    let(:xml) do
      %(
       <Operator version="any"
       dataSourceRef="FR1:OrganisationalUnit::"
       id="FR1:Operator:088:LOC">
         <BrandingRef ref="" />
         <Name>VEXIN BUS</Name>
         <ContactDetails>
           <ContactPerson></ContactPerson>
           <Email></Email>
           <Phone></Phone>
           <Url></Url>
           <FurtherDetails></FurtherDetails>
         </ContactDetails>
         <Address>
           <HouseNumber></HouseNumber>
           <AddressLine1></AddressLine1>
           <Street></Street>
           <Town></Town>
           <PostCode></PostCode>
           <PostCodeExtension></PostCodeExtension>
         </Address>
       </Operator>
    )
    end

    let(:resource) do
      source = Netex::Source.new
      source.parse(StringIO.new(xml))
      source.operators.first
    end

    it 'remove entities without defined attributes' do
      expect(resource).to have_attributes(contact_details: nil, address: nil)
    end
  end

  describe '#PointOfInterest' do
    let(:xml) do # rubocop:disable Metrics/BlockLength
      %(
        <PointOfInterest version="any" id="1">
          <validityConditions>
            <AvailabilityCondition version="any" id="1">
              <dayTypes>
                <DayType version="any" id="1">
                  <properties>
                    <PropertyOfDay>
                      <DaysOfWeek>Monday Tuesday Wednesday Thursday Friday</DaysOfWeek>
                    </PropertyOfDay>
                  </properties>
                </DayType>
              </dayTypes>
              <timebands>
                <Timeband version="any" id="1">
                  <StartTime>08:30:00</StartTime>
                  <EndTime>17:30:00</EndTime>
                </Timeband>
              </timebands>
            </AvailabilityCondition>

            <AvailabilityCondition version="any" id="2">
              <dayTypes>
                <DayType version="any" id="2">
                  <Name>Working day</Name>
                  <properties>
                    <PropertyOfDay>
                      <DaysOfWeek>Saturday Sunday</DaysOfWeek>
                    </PropertyOfDay>
                  </properties>
                </DayType>
              </dayTypes>
              <timebands>
                <Timeband version="any" id="2">
                  <StartTime>10:30:00</StartTime>
                  <EndTime>12:30:00</EndTime>
                </Timeband>
              </timebands>
            </AvailabilityCondition>
          </validityConditions>

          <keyList>
            <KeyValue typeOfKey="ALTERNATE_IDENTIFIER">
              <Key>osm</Key>
              <Value>7817817891</Value>
            </KeyValue>
          </keyList>

          <Name>Frampton Football Stadium</Name>

          <Centroid>
            <Location>
              <Longitude>-180</Longitude>
              <Latitude>-90</Latitude>
            </Location>
          </Centroid>

          <Url>http://www.barpark.co.uk</Url>
          <PostalAddress version="any" id="2">
            <CountryName>France</CountryName>
            <AddressLine1>23 Foo St</AddressLine1>
            <Town>Frampton</Town>
            <PostCode>FGR 1JS</PostCode>
          </PostalAddress>
          <OperatingOrganisationView>
            <ContactDetails>
              <Email>ola@nordman.no</Email>
              <Phone>815 00 888</Phone>
            </ContactDetails>
          </OperatingOrganisationView>

          <classifications>
            <PointOfInterestClassificationView>
              <Name>Category 2</Name>
            </PointOfInterestClassificationView>
          </classifications>
        </PointOfInterest>
      )
    end

    let(:point_of_interest) do
      source = Netex::Source.new
      source.parse(StringIO.new(xml))
      source.point_of_interests.first
    end

    it 'should support validityConditions' do
      expect(point_of_interest.validity_conditions).not_to be_empty
    end

    it 'should support OperatingOrganisationView' do
      expect(point_of_interest.operating_organisation_view).not_to be_empty
    end

    it 'should support OperatingOrganisationView' do
      expect(point_of_interest.classifications).not_to be_empty
    end
  end

  describe '#read' do
    let(:source) { Netex::Source.new }

    def tempfile(file)
      tempfile = Tempfile.new
      FileUtils.cp file, tempfile.path
      tempfile.path
    end

    context 'with an XML file' do
      before { source.read sample_file }

      it 'parse the whole file' do
        expect(source.resource_count).to eq(6)
      end
    end

    context 'with an XML file without extension' do
      before { source.read tempfile(sample_file), type: :xml }

      it 'parse the whole file' do
        expect(source.resource_count).to eq(6)
      end
    end

    context 'with an ZIP file' do
      before { source.read sample_file(:zip) }

      it 'parse all XML files' do
        expect(source.resource_count).to eq(6)
      end

      it 'tags all resources with filename where the resource is read' do
        expect(source.resources).to all(have_tag(:filename, 'sample.xml'))
      end
    end

    context 'with an ZIP file without extension' do
      before { source.read tempfile(sample_file(:zip)), type: :zip }

      it 'parse all XML files' do
        expect(source.resource_count).to eq(6)
      end

      it 'tags all resources with filename where the resource is read' do
        expect(source.resources).to all(have_tag(:filename, 'sample.xml'))
      end
    end
  end

  describe '#UicOperatingPeriod' do
    subject(:period) do
      source = Netex::Source.new
      source.parse(StringIO.new(xml))
      source.operating_periods.first
    end

    with_xml %(<UicOperatingPeriod> <FromDate>2030-01-01T00:00:00</FromDate></UicOperatingPeriod>) do
      it { expect(period.from_date.to_date).to eq Date.parse('2030-01-01T00:00:00') }
    end

    with_xml %(<UicOperatingPeriod> <ToDate>2030-01-01T00:00:00</ToDate></UicOperatingPeriod>) do
      it { expect(period.to_date.to_date).to eq Date.parse('2030-01-01T00:00:00') }
    end

    with_xml %(<UicOperatingPeriod><ValidDayBits>11111</ValidDayBits></UicOperatingPeriod>) do
      it { is_expected.to have_attributes(valid_day_bits: '11111') }
    end
  end

  describe 'JourneyPattern and ServiceJourneyPattern parsing' do
    subject(:journey_pattern) do
      source = Netex::Source.new
      source.parse(StringIO.new(xml))
      source.journey_patterns.first
    end

    with_xml %(<JourneyPattern id="test"/>) do
      it { is_expected.to have_attributes(id: 'test') }
      it { is_expected.to be_a(Netex::JourneyPattern) }
    end

    with_xml %(<ServiceJourneyPattern id="test"/>) do
      it { is_expected.to have_attributes(id: 'test') }
      it { is_expected.to be_a(Netex::ServiceJourneyPattern) }
    end
  end

  describe Netex::DestinationDisplay do
    subject(:destination_display) do
      source = Netex::Source.new
      source.parse(StringIO.new(xml))
      source.destination_displays.first
    end

    with_xml %(<DestinationDisplay id="test"/>) do
      it { is_expected.to have_attributes(id: 'test') }
    end

    with_xml %(<DestinationDisplay><FrontText>Destination Sample</FrontText></DestinationDisplay>) do
      it { is_expected.to have_attributes(front_text: 'Destination Sample') }
    end
  end

  describe Netex::RoutePoint do
    subject(:route_point) do
      source = Netex::Source.new
      source.parse(StringIO.new(xml))
      source.route_points.first
    end

    with_xml %(<RoutePoint id="test"/>) do
      it { is_expected.to have_attributes(id: 'test') }
    end

    with_xml %(
      <RoutePoint>
        <projections>
          <PointProjection id="1"><ProjectToPointRef ref="1"/></PointProjection>
          <PointProjection id="2"><ProjectToPointRef ref="2"/></PointProjection>
        </projections>
      </RoutePoint>
    ) do
      def expected_point_projections(*ids)
        ids.map do |id|
          an_object_having_attributes id: id, project_to_point_ref: an_object_having_attributes(ref: id)
        end
      end

      it {
        is_expected.to have_attributes(projections: a_collection_including(*expected_point_projections('1', '2')))
      }
    end
  end

  describe Netex::Direction do
    subject(:direction) do
      source = Netex::Source.new
      source.parse(StringIO.new(xml))
      source.directions.first
    end

    with_xml %(<Direction id="test"/>) do
      it { is_expected.to have_attributes(id: 'test') }
    end

    with_xml %(<Direction><Name>Direction Sample</Name></Direction>) do
      it { is_expected.to have_attributes(name: 'Direction Sample') }
    end
  end

  describe Netex::ScheduledStopPoint do
    subject(:destination_display) do
      source = Netex::Source.new
      source.parse(StringIO.new(xml))
      source.scheduled_stop_points.first
    end

    with_xml %(<ScheduledStopPoint id="test"/>) do
      it { is_expected.to have_attributes(id: 'test') }
    end
  end

  context 'for unsupported Resource' do
    subject(:resource) do
      source = Netex::Source.new
      source.parse(StringIO.new(xml))
      source.resources.first
    end

    with_xml %(<String><Name>Unsupported</Name><String>) do
      it { is_expected.to be_nil }
    end

    with_xml %(<Address><Name>Unsupported</Name><Address>) do
      it { is_expected.to be_nil }
    end
  end

  context 'when several Resources share the same identifiers' do
    subject(:resources) do
      source = Netex::Source.new
      source.parse(StringIO.new(xml))
      source.quays
    end

    def a_resource_named(name)
      an_object_having_attributes name: name
    end

    with_xml %(<root><Quay><Name>First</Name></Quay><Quay><Name>Second</Name></Quay></root>) do
      it { is_expected.to contain_exactly(a_resource_named('First'), a_resource_named('Second')) }
    end

    with_xml %(<root><Quay id='1'><Name>First</Name></Quay><Quay id='1'><Name>Second</Name></Quay></root>) do
      it { is_expected.to contain_exactly(a_resource_named('First'), a_resource_named('Second')) }
    end
  end

  describe 'indexes' do
    let(:xml) do
      <<~XML
        <DayTypeAssignment id="first">
          <DayTypeRef ref="target"/>
        </DayTypeAssignment>
        <DayTypeAssignment id="second">
          <DayTypeRef ref="target"/>
        </DayTypeAssignment>
        <DayTypeAssignment id="wrong">
          <DayTypeRef ref="other"/>
        </DayTypeAssignment>
      XML
    end

    before do
      source.transformers << Netex::Transformer::Indexer.new(Netex::DayTypeAssignment, by: :day_type_ref)
      source.parse(StringIO.new(xml))
    end

    subject { source.day_type_assignments.find_by(day_type_ref: 'target') }

    def expected_resources
      %w[first second].map do |identifier|
        an_object_having_attributes(id: identifier)
      end
    end

    it { is_expected.to match_array(expected_resources) }
  end

  describe 'ServiceJourney parsing' do
    subject(:direction) do
      source = Netex::Source.new
      source.parse(StringIO.new(xml))
      source.service_journeys.first
    end

    with_xml %(<ServiceJourney><ServiceJourneyPatternRef ref="test"/></ServiceJourney>) do
      it { is_expected.to have_attributes(service_journey_pattern_ref: an_object_having_attributes(ref: 'test')) }
    end

    with_xml %(<ServiceJourney><ServiceJourneyPatternRef ref="test"/></ServiceJourney>) do
      it { is_expected.to have_attributes(journey_pattern_ref: an_object_having_attributes(ref: 'test')) }
    end

    with_xml %(<ServiceJourney><JourneyPatternRef ref="test"/></ServiceJourney>) do
      it { is_expected.to have_attributes(journey_pattern_ref: an_object_having_attributes(ref: 'test')) }
    end
  end

  describe Netex::FareZone do
    subject(:fare_zone) do
      source = Netex::Source.new
      source.parse(StringIO.new(xml))
      source.fare_zones.first
    end

    with_xml %(
      <FareZone id="sample">
        <keyList>
          <KeyValue typeOfKey="ALTERNATE_IDENTIFIER">
            <Key>test</Key>
            <Value>test-1</Value>
          </KeyValue>
          <KeyValue typeOfKey="ALTERNATE_IDENTIFIER">
            <Key>test2</Key>
            <Value>test-2</Value>
          </KeyValue>
        </keyList>
        <Name>Fare Zone Sample</Name>
        <projections>
          <TopographicProjectionRef ref="tp1">A</TopographicProjectionRef>
          <TopographicProjectionRef ref="tp2">B</TopographicProjectionRef>
        </projections>
      </FareZone>
    ) do
      it { is_expected.to have_attributes(id: 'sample') }
      it { is_expected.to have_attributes(name: 'Fare Zone Sample') }
      it do
        expected_resources = %w[tp1 tp2].map do |identifier|
          an_object_having_attributes(ref: identifier)
        end

        expect(subject.projections).to match_array(expected_resources)
      end
    end
  end
end
