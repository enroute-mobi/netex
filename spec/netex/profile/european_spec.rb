require "spec_helper"

RSpec.describe Netex::Profile::European do

  describe "#container_zip" do

    subject { Netex::Profile::European.new.container_type }

    it { is_expected.to be(:zip) }

  end

  describe "output XML" do
    let(:profile) { Netex::Profile::European.new }
    let(:zip_file) { Zip::Test.new }
    let(:target) { Netex::Target.build zip_file.buffer, profile: profile, publication_timestamp: ::Time.utc(2030) }

    describe "for PassingTimes" do
      subject { zip_file.content('common.xml') }

      before do
        target.add Netex::TimetabledPassingTime.new(
          id: 1,
          departure_time: Netex::Time.new(5, 30),
          stop_point_in_journey_pattern_ref: Netex::Reference.new('point:1', type: 'StopPointInJourneyPattern')
        ).with_tag(parent_id: "parent")

        target.add Netex::TimetabledPassingTime.new(
          id: 2,
          arrival_time: Netex::Time.new(5, 31),
          departure_time: Netex::Time.new(5, 32),
          departure_day_offset: 1,
          stop_point_in_journey_pattern_ref: Netex::Reference.new('point:2', type: 'StopPointInJourneyPattern')
        ).with_tag(parent_id: "parent")

        target.add Netex::ServiceJourney.new(
          id: 'parent',
          departure_time: Netex::Time.new(5, 30),
        )

        target.close
      end

      it do
        expected_xml =<<~XML
       <?xml version="1.0" encoding="UTF-8"?>
       <PublicationDelivery xmlns="http://www.netex.org.uk/netex">
         <PublicationTimestamp>2030-01-01T00:00:00Z</PublicationTimestamp>
         <ParticipantRef>enRoute</ParticipantRef>
         <dataObjects>
           <TimetableFrame id="enRoute:TimetableFrame:1" version="any">
             <vehicleJourneys>
               <ServiceJourney id="parent" version="any">
         <DepartureTime>05:30:00</DepartureTime>
         <passingTimes>
           <TimetabledPassingTime version="any" id="1">
         <StopPointInJourneyPatternRef ref="point:1"/>
         <DepartureTime>05:30:00</DepartureTime>
       </TimetabledPassingTime><TimetabledPassingTime version="any" id="2">
         <StopPointInJourneyPatternRef ref="point:2"/>
         <ArrivalTime>05:31:00</ArrivalTime>
         <DepartureTime>05:32:00</DepartureTime>
         <DepartureDayOffset>1</DepartureDayOffset>
       </TimetabledPassingTime>
         </passingTimes>
       </ServiceJourney>
             </vehicleJourneys>
           </TimetableFrame>
         </dataObjects>
       </PublicationDelivery>
        XML

        is_expected.to eq(expected_xml)
      end
    end

    describe "for StopPlaceEntrance" do
      subject { zip_file.content('stops.xml') }

      before do
        target.add Netex::StopPlaceEntrance.new(id: 1).with_tag(parent_id: "parent")
        target.add Netex::StopPlaceEntrance.new(id: 2).with_tag(parent_id: "parent")
        target.add Netex::StopPlace.new id: "parent"
        target.close
      end

      it do
        expected_xml =<<~XML
        <?xml version="1.0" encoding="UTF-8"?>
        <PublicationDelivery xmlns="http://www.netex.org.uk/netex">
          <PublicationTimestamp>2030-01-01T00:00:00Z</PublicationTimestamp>
          <ParticipantRef>enRoute</ParticipantRef>
          <dataObjects>
            <SiteFrame id="enRoute:SiteFrame:1" version="any">
              <stopPlaces>
                <StopPlace id="parent" version="any">
          <entrances>
            <StopPlaceEntrance id="1" version="any"/><StopPlaceEntrance id="2" version="any"/>
          </entrances>
        </StopPlace>
              </stopPlaces>
            </SiteFrame>
          </dataObjects>
        </PublicationDelivery>
        XML

        is_expected.to eq(expected_xml)
      end
    end

    describe "for Quay" do
      subject { zip_file.content('stops.xml') }

      before do
        target.add Netex::Quay.new(id: 1).with_tag parent_id: "parent"
        target.add Netex::Quay.new(id: 2).with_tag parent_id: "parent"
        target.add Netex::StopPlace.new id: "parent"

        target.close
      end

      it do
        expected_xml =<<~XML
        <?xml version="1.0" encoding="UTF-8"?>
        <PublicationDelivery xmlns="http://www.netex.org.uk/netex">
          <PublicationTimestamp>2030-01-01T00:00:00Z</PublicationTimestamp>
          <ParticipantRef>enRoute</ParticipantRef>
          <dataObjects>
            <SiteFrame id="enRoute:SiteFrame:1" version="any">
              <stopPlaces>
                <StopPlace id="parent" version="any">
          <quays>
            <Quay id="1" version="any"/><Quay id="2" version="any"/>
          </quays>
        </StopPlace>
              </stopPlaces>
            </SiteFrame>
          </dataObjects>
        </PublicationDelivery>
        XML

        is_expected.to eq(expected_xml)
      end
    end

    describe "for orphan Quay" do
      subject { zip_file.content('stops.xml') }

      before do
        target.add Netex::Quay.new(id: 1)
        target.close
      end

      it do
        expected_xml =<<~XML
        <?xml version="1.0" encoding="UTF-8"?>
        <PublicationDelivery xmlns="http://www.netex.org.uk/netex">
          <PublicationTimestamp>2030-01-01T00:00:00Z</PublicationTimestamp>
          <ParticipantRef>enRoute</ParticipantRef>
          <dataObjects>
            <SiteFrame id="enRoute:SiteFrame:1" version="any">
              <stopPlaces>
                <StopPlace id="1" version="any">
          <placeTypes>
            <TypeOfPlaceRef ref="quay"/>
          </placeTypes>
        </StopPlace>
              </stopPlaces>
            </SiteFrame>
          </dataObjects>
        </PublicationDelivery>
        XML

        is_expected.to eq(expected_xml)
      end
    end
  end

  describe "#document_for" do

    subject { Netex::Profile::European.new.document_for(resource).filename }

    context "when the resource is a StopPlace" do

      let(:resource) { Netex::StopPlace.new }

      it { is_expected.to be(Netex::Profile::European::STOPS_FILE) }

    end

    context "when the resource as a line_id tag" do

      let(:resource) { Netex::Line.new tags: { line_id: 'test' } }

      it { is_expected.to eq('line-test.xml') }

    end

    context "by default" do

      let(:resource) { Netex::DayType.new }

      it { is_expected.to be(Netex::Profile::European::COMMON_FILE) }

    end


  end

end
