# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Netex::Profile::IDFM::Base do
  let(:profile) { described_class.new }

  let(:resource) do
    Netex::UicOperatingPeriod.new(
      id: 1,
      from_date: '2030-01-01',
      to_date: '2030-10-01',
      valid_day_bits: '11111'
    )
  end

  describe '#transform' do
    subject { profile.transform(resource) }

    it { is_expected.to be_a_kind_of(Netex::OperatingPeriod) }

    it { is_expected.to_not be_a_kind_of(Netex::UicOperatingPeriod) }

    context 'when a tag is defined on UicOperatingPeriod' do
      before { resource.with_tag(test: 'ok') }

      it { is_expected.to have_tag(:test, 'ok') }
    end
  end
end
