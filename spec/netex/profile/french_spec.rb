# frozen_string_literal: true

require "spec_helper"

RSpec.describe Netex::Profile::French do

  let(:profile) { Netex::Profile::French.new }

  describe "#container_zip" do
    subject { profile.container_type }
    it { is_expected.to be(:zip) }
  end

  describe "output XML" do
    let(:zip_file) { Zip::Test.new }
    let(:target) { Netex::Target.build zip_file.buffer, profile: profile, publication_timestamp: ::Time.utc(2021) }

    describe 'LINE file' do
      subject { zip_file.content('LINE-sample.xml') }

      before { target.add Netex::Line.new(id: "sample", name: "Line Sample") }
      before { target.add Netex::ServiceJourneyPattern.new(id: "journey-pattern-1", name: "Journey Pattern Sample").with_tag(line_id: 'sample') }
      before { target.add Netex::ServiceJourney.new(id: "vehicle-journey-1", name: "Vehicle Journey Sample").with_tag(line_id: 'sample') }
      before { target.close }

      it do
        expected_xml = <<~XML
          <?xml version="1.0" encoding="UTF-8"?>
          <PublicationDelivery xmlns="http://www.netex.org.uk/netex" version="1.09:FR-NETEX-2.1-1.0">
            <PublicationTimestamp>2021-01-01T00:00:00Z</PublicationTimestamp>
            <ParticipantRef>enRoute</ParticipantRef>
            <dataObjects>
              <CompositeFrame id='FR:CompositeFrame:NETEX_LIGNE:LOC' version='1.09:FR-NETEX-2.1-1.0'>
                <Name>Line Sample</Name>
                <TypeOfFrameRef ref='FR:TypeOfFrame:NETEX_LIGNE:'/>
                <frames>
                  <GeneralFrame id="FR:GeneralFrame:NETEX_LIGNE:LOC" version="1.09:FR-NETEX-2.1-1.0">
                    <TypeOfFrameRef ref="FR:TypeOfFrame:NETEX_LIGNE:"/>
                    <members>
                      <Line id="sample" version="any">
                        <Name>Line Sample</Name>
                      </Line>
                    </members>
                  </GeneralFrame>
                  <GeneralFrame id='FR:GeneralFrame:NETEX_RESEAU:LOC' version='1.09:FR-NETEX-2.1-1.0'>
                    <TypeOfFrameRef ref='FR:TypeOfFrame:NETEX_RESEAU:'/>
                    <members>
                      <ServiceJourneyPattern id='journey-pattern-1' version='any'>
                        <Name>Journey Pattern Sample</Name>
                      </ServiceJourneyPattern>
                    </members>
                  </GeneralFrame>
                  <GeneralFrame id='FR:GeneralFrame:NETEX_HORAIRE:LOC' version='1.09:FR-NETEX-2.1-1.0'>
                    <TypeOfFrameRef ref='FR:TypeOfFrame:NETEX_HORAIRE:'/>
                     <members>
                       <ServiceJourney id='vehicle-journey-1' version='any'>
                         <Name>Vehicle Journey Sample</Name>
                       </ServiceJourney>
                     </members>
                   </GeneralFrame>
                  </frames>
              </CompositeFrame>
            </dataObjects>
          </PublicationDelivery>
        XML
        is_expected.to be_same_xml(expected_xml)
      end
    end

    describe 'RESOURCE file' do
      subject { zip_file.content('RESOURCE.xml') }

      before { target.add Netex::UicOperatingPeriod.new(id: 'sample', from_date: Date.parse('2030-01-01'), valid_day_bits: '10101') }
      before { target.close }

      it do
        expected_xml = <<~XML
          <?xml version="1.0" encoding="UTF-8"?>
          <PublicationDelivery xmlns="http://www.netex.org.uk/netex" version="1.09:FR-NETEX-2.1-1.0">
            <PublicationTimestamp>2021-01-01T00:00:00Z</PublicationTimestamp>
            <ParticipantRef>enRoute</ParticipantRef>
            <dataObjects>
              <GeneralFrame id="FR:GeneralFrame:NETEX_CALENDRIER:LOC" version="1.09:FR-NETEX-2.1-1.0">
                <TypeOfFrameRef ref="FR:TypeOfFrame:NETEX_CALENDRIER:"/>
                <members>
                  <UicOperatingPeriod id='sample' version='any'>
                    <FromDate>2030-01-01T00:00:00</FromDate>
                    <ValidDayBits>10101</ValidDayBits>
                  </UicOperatingPeriod>
                </members>
              </GeneralFrame>
            </dataObjects>
          </PublicationDelivery>
        XML
        is_expected.to be_same_xml(expected_xml)
      end
    end

    describe "when resources are provided in a random order" do
      describe "STOP.xml" do
        subject { zip_file.content('STOP.xml') }

        before { target.add Netex::Quay.new id: "1", name: "First" }
        before { target.add Netex::StopPlace.new id: "2", name: "Second" }
        before { target.add Netex::Quay.new id: "3", name: "Third" }
        before { target.close }

        it do
          expected_xml =<<~XML
            <?xml version="1.0" encoding="UTF-8"?>
            <PublicationDelivery xmlns="http://www.netex.org.uk/netex" version="1.09:FR-NETEX-2.1-1.0">
              <PublicationTimestamp>2021-01-01T00:00:00Z</PublicationTimestamp>
              <ParticipantRef>enRoute</ParticipantRef>
              <dataObjects>
                <GeneralFrame id="FR:GeneralFrame:NETEX_ARRET:LOC" version="1.09:FR-NETEX-2.1-1.0">
                  <TypeOfFrameRef ref="FR:TypeOfFrame:NETEX_ARRET:"/>
                  <members>
                    <StopPlace id="2" version="any">
                      <Name>Second</Name>
                    </StopPlace>
                    <Quay id="1" version="any">
                      <Name>First</Name>
                    </Quay>
                    <Quay id="3" version="any">
                      <Name>Third</Name>
                    </Quay>
                  </members>
                </GeneralFrame>
              </dataObjects>
            </PublicationDelivery>
          XML
          is_expected.to be_same_xml(expected_xml)
        end
      end
    end
  end
end
