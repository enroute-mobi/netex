# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Netex::Profile::IDFM::ICAR do
  subject(:profile) { described_class.new }

  describe '#transform' do
    subject(:transformed_resource) { profile.transform resource }

    context 'when the resource is not a Quay (like a StopPlace)' do
      let(:resource) { Netex::StopPlace.new }

      it { is_expected.to be_nil }
    end

    context 'when the resource is a Quay' do
      let(:resource) { Netex::Quay.new }

      it { is_expected.to eq(resource) }

      describe '#key_list' do
        subject { transformed_resource.key_list }

        context 'when a KeyValue with a typeOfKey different than ALTERNATE_IDENTIFIER or ALTERNATE_NAME' do
          before { resource.key_list << Netex::KeyValue.new(type_of_key: 'dummy') }

          it { is_expected.to be_empty }
        end

        context 'when a KeyValue has key ACS_ID and type ALTERNATE_IDENTIFIER' do
          let(:key_value) { Netex::KeyValue.new key: 'ACS_ID', type_of_key: 'ALTERNATE_IDENTIFIER' }
          before { resource.key_list << key_value }

          it { is_expected.to contain_exactly(key_value) }
        end

        context 'when a KeyValue has key ACS_NAME and type ALTERNATE_NAME' do
          let(:key_value) { Netex::KeyValue.new key: 'ACS_NAME', type_of_key: 'ALTERNATE_NAME' }
          before { resource.key_list << key_value }

          it { is_expected.to contain_exactly(key_value) }
        end

        context 'when a KeyValue has key external and type ALTERNATE_IDENTIFIER (like a Chouette code)' do
          before { resource.key_list << Netex::KeyValue.new(key: 'external', type_of_key: 'ALTERNATE_IDENTIFIER') }

          it { is_expected.to be_empty }
        end
      end

      describe '#postal_address' do
        subject { transformed_resource.postal_address }

        %i[post_code town country_name].each do |attribute|
          context "when #{attribute} is defined" do
            before { resource.postal_address = Netex::PostalAddress.new(attribute => 'dummy') }

            it { is_expected.to have_attributes(attribute => nil) }
          end
        end

        %i[address_line_1 postal_region].each do |attribute|
          context "when #{attribute} is defined" do
            before { resource.postal_address = Netex::PostalAddress.new(attribute => 'dummy') }

            it { is_expected.to have_attributes(attribute => 'dummy') }
          end
        end
      end

      describe '#accessibility_assessment limitation' do
        subject { transformed_resource.accessibility_assessment.limitation }

        before do
          resource.accessibility_assessment = Netex::AccessibilityAssessment.new(
            limitations: [Netex::AccessibilityLimitation.new(attribute => 'true')]
          )
        end

        %i[step_free_access escalator_free_access lift_free_access].each do |attribute|
          context "when #{attribute} is defined" do
            let(:attribute) { attribute }

            it { is_expected.to have_attributes(attribute => nil) }
          end
        end

        %i[wheelchair_access audible_signals_available visual_signs_available].each do |attribute|
          context "when #{attribute} is defined" do
            let(:attribute) { attribute }

            it { is_expected.to have_attributes(attribute => 'true') }
          end
        end
      end

      describe '#centroid location coordinates' do
        subject { transformed_resource.centroid.location.coordinates }

        context 'when centroid latitude/longitude is 48.8583701,2.2919064' do
          before do
            resource.centroid = Netex::Point.new(
              location: Netex::Location.new(longitude: 2.2919064, latitude: 48.8583701)
            )
          end

          it { is_expected.to have_attributes(x: a_value_within(0.1).of(596_725.60)) }
          it { is_expected.to have_attributes(y: a_value_within(0.1).of(2_428_893.7)) }
          it { is_expected.to have_attributes(srs_name: 'EPSG:27572') }
        end
      end
    end
  end

  describe '#code_space' do
    subject { profile.code_space }

    context 'when a value is specified ("dummy")' do
      before { profile.code_space = 'dummy' }
      it { is_expected.to eq('dummy') }
    end

    context 'by defaut' do
      it { is_expected.to eq('CODE_SPACE') }
    end
  end
end

RSpec.describe Netex::Profile::IDFM::ICAR::Document::Stops do
  subject(:document) do
    described_class.new(
      code_space: 'test',
      participant_ref: 'participant-ref',
      publication_timestamp: Time.parse('2030-01-01 12:00 UTC'),
      file_type: :total,
      site_id: 42,
      site_name: 'Test',
      builder_book: Netex::Profile::IDFM::BuilderBook.new
    )
  end

  describe '#filename' do
    subject { document.filename }

    it { is_expected.to eq("ARRET_42_Test_T_20300101T120000Z.xml") }
  end

  describe 'XML sample' do
    subject { Ox.dump(document.to_xml_document, encoding: 'UTF-8').strip }

    # let(:quay) do
    #   Netex::Quay.new(
    #     id: 'FR::Quay:50121060:FR1',
    #     name: 'Gare Epinay-sous-Sénart',
    #     private_code: 'TESTALIM14',
    #     centroid: Netex::Point.new(
    #       location: Netex::Location.new(longitude: 2.5141073, latitude: 48.691504)
    #     ),
    #     transport_mode: 'bus',
    #     key_list: [
    #       Netex::KeyValue.new(
    #         key: 'other', value: 'dummy', type_of_key: 'ALTERNATE_IDENTIFIER'
    #       ),
    #       Netex::KeyValue.new(
    #         key: 'ACS_ID', value: '123', type_of_key: 'ALTERNATE_IDENTIFIER'
    #       ),
    #       Netex::KeyValue.new(
    #         key: 'ACS_NAMEA', value: 'EPINAYSENART', type_of_key: 'ALTERNATE_IDENTIFIER'
    #       )
    #     ],
    #     postal_address: Netex::PostalAddress.new(
    #       id: 'FR::PostalAddress:50121060:FR1',
    #       address_line_1: 'rue de la Forêt',
    #       post_code: '91860',
    #       town: 'Epinay-sous-Sénart',
    #       postal_region: '91215',
    #       country_name: 'France'
    #     ),
    #     accessibility_assessment: Netex::AccessibilityAssessment.new(
    #       id: 'FR::AccessibilityAssessment:50121060:FR1',
    #       mobility_impaired_access: 'unknown',
    #       limitations: [
    #         Netex::AccessibilityLimitation.new(
    #           wheelchair_access: 'true',
    #           step_free_access: 'unknown',
    #           escalator_free_access: 'true',
    #           lift_free_access: 'true',
    #           audible_signals_available: 'true',
    #           visual_signs_available: 'false'
    #         )
    #       ]
    #     )
    #   )
    # end

    let(:quay) do
      Netex::Quay.new(
        id: 'FR::Quay:50121060:FR1',
        name: 'Gare Epinay-sous-Sénart',
        private_code: 'TESTALIM14',
        centroid: Netex::Point.new(
          location: Netex::Location.new(
            coordinates: GML::Pos.new(
              srs_name: 'EPSG:27572',
              x: 613_081,
              y: 2_410_342
            )
          )
        ),
        transport_mode: 'bus',
        key_list: [
          Netex::KeyValue.new(
            key: 'ACS_ID', value: '123', type_of_key: 'ALTERNATE_IDENTIFIER'
          ),
          Netex::KeyValue.new(
            key: 'ACS_NAMEA', value: 'EPINAYSENART', type_of_key: 'ALTERNATE_NAME'
          )
        ],
        postal_address: Netex::PostalAddress.new(
          id: 'FR::PostalAddress:50121060:FR1',
          address_line_1: 'rue de la Forêt',
          postal_region: '91215'
        ),
        accessibility_assessment: Netex::AccessibilityAssessment.new(
          id: 'FR::AccessibilityAssessment:50121060:FR1',
          mobility_impaired_access: 'unknown',
          limitations: [
            Netex::AccessibilityLimitation.new(
              wheelchair_access: 'true',
              audible_signals_available: 'true',
              visual_signs_available: 'false'
            )
          ]
        )
      )
    end

    before do
      document.add quay
    end

    self::XML = <<~XML
      <?xml version="1.0" encoding="UTF-8"?>
      <PublicationDelivery xmlns="http://www.netex.org.uk/netex" xmlns:gml="http://www.opengis.net/gml/3.2" version="1.04:FR1-NETEX-2.0">
        <PublicationTimestamp>2030-01-01T12:00:00Z</PublicationTimestamp>
        <ParticipantRef>participant-ref</ParticipantRef>
        <dataObjects>
          <GeneralFrame id="test:GeneralFrame:NETEX_ARRET_IDF_20300101T120000Z:LOC" version="any" modification="revise">
            <TypeOfFrameRef ref="FR1:TypeOfFrame:NETEX_ARRET">version="1.04:FR1-NETEX_ARRET_IDF-2.1"</TypeOfFrameRef>
            <members>
              <Quay id="FR::Quay:50121060:FR1" version="any">
                <keyList>
                  <KeyValue typeOfKey="ALTERNATE_IDENTIFIER">
                    <Key>ACS_ID</Key>
                    <Value>123</Value>
                  </KeyValue>
                  <KeyValue typeOfKey="ALTERNATE_NAME">
                    <Key>ACS_NAMEA</Key>
                    <Value>EPINAYSENART</Value>
                  </KeyValue>
                </keyList>
                <Name>Gare Epinay-sous-Sénart</Name>
                <PrivateCode>TESTALIM14</PrivateCode>
                <Centroid version="any">
                  <Location>
                    <gml:pos srsName="EPSG:27572">613081 2410342</gml:pos>
                  </Location>
                </Centroid>
                <PostalAddress id="FR::PostalAddress:50121060:FR1" version="any">
                  <AddressLine1>rue de la Forêt</AddressLine1>
                  <PostalRegion>91215</PostalRegion>
                </PostalAddress>
                <AccessibilityAssessment id="FR::AccessibilityAssessment:50121060:FR1" version="any">
                  <MobilityImpairedAccess>unknown</MobilityImpairedAccess>
                  <limitations>
                    <AccessibilityLimitation version="any">
                      <WheelchairAccess>true</WheelchairAccess>
                      <AudibleSignalsAvailable>true</AudibleSignalsAvailable>
                      <VisualSignsAvailable>false</VisualSignsAvailable>
                    </AccessibilityLimitation>
                  </limitations>
                </AccessibilityAssessment>
                <TransportMode>bus</TransportMode>
              </Quay>
            </members>
          </GeneralFrame>
        </dataObjects>
      </PublicationDelivery>
    XML

    let(:expected_xml) do
      self.class::XML.strip
    end

    it "creates this XML structure:\n#{self::XML}" do
      is_expected.to be_same_xml(expected_xml)
    end
  end
end
