# frozen_string_literal: true

require "spec_helper"

RSpec.describe Netex::Profile::IDFM::Full do

  let(:profile) { Netex::Profile::IDFM::Full.new }

  describe "output XML" do
    let(:zip_file) { Zip::Test.new }
    let(:target) { Netex::Target.build zip_file.buffer, profile: profile, publication_timestamp: ::Time.utc(2030) }

    describe "for StopPlaceEntrance" do
      describe "arrets.xml" do
        subject { zip_file.content('arrets.xml') }

        before do
          target.add Netex::StopPlaceEntrance.new(id: 1).with_tag(parent_id: "parent")
          target.add Netex::StopPlaceEntrance.new(id: 2).with_tag(parent_id: "parent")
          target.add Netex::StopPlace.new id: "parent"
          target.close
        end

        it do
          expected_xml =<<~XML
        <?xml version="1.0" encoding="UTF-8"?>
        <PublicationDelivery xmlns="http://www.netex.org.uk/netex" xmlns:gml="http://www.opengis.net/gml/3.2" version="1.04:FR1-NETEX-1.6-1.8">
          <PublicationTimestamp>2030-01-01T00:00:00Z</PublicationTimestamp>
          <ParticipantRef>FR1_OFFRE</ParticipantRef>
          <dataObjects>
            <GeneralFrame id="FR1:GeneralFrame:NETEX_ARRET_IDF-20300101T000000Z:LOC" version="1.8" dataSourceRef="FR1-OFFRE_AUTO">
              <TypeOfFrameRef ref="FR1:TypeOfFrame:NETEX_ARRET_IDF:"/>
              <members>
                <StopPlace id="parent" version="any">
          <entrances>
            <StopPlaceEntranceRef ref="1">version="any"</StopPlaceEntranceRef><StopPlaceEntranceRef ref="2">version="any"</StopPlaceEntranceRef>
          </entrances>
        </StopPlace>
                <StopPlaceEntrance id="1" version="any"/>
        <StopPlaceEntrance id="2" version="any"/>
              </members>
            </GeneralFrame>
          </dataObjects>
        </PublicationDelivery>
          XML

          is_expected.to eq(expected_xml)
        end
      end
    end

    describe "for Quay" do
      describe "arrets.xml" do
        subject { zip_file.content('arrets.xml') }

        before do
          @quay_1 = Netex::Quay.new(id: 1)
          @quay_1.tags[:parent_id] = "parent"
          target.add @quay_1

          @quay_2 = Netex::Quay.new(id: 2)
          @quay_2.tags[:parent_id] = "parent"
          target.add  @quay_2

          target.add Netex::StopPlace.new id: "parent"
          target.close
        end

        it do
          expected_xml =<<~XML
          <?xml version="1.0" encoding="UTF-8"?>
          <PublicationDelivery xmlns="http://www.netex.org.uk/netex" xmlns:gml="http://www.opengis.net/gml/3.2" version="1.04:FR1-NETEX-1.6-1.8">
            <PublicationTimestamp>2030-01-01T00:00:00Z</PublicationTimestamp>
            <ParticipantRef>FR1_OFFRE</ParticipantRef>
            <dataObjects>
              <GeneralFrame id="FR1:GeneralFrame:NETEX_ARRET_IDF-20300101T000000Z:LOC" version="1.8" dataSourceRef="FR1-OFFRE_AUTO">
                <TypeOfFrameRef ref="FR1:TypeOfFrame:NETEX_ARRET_IDF:"/>
                <members>
                  <StopPlace id="parent" version="any"/>
                  <Quay id="1" version="any">
            <ParentSiteRef ref="parent" version="any"/>
          </Quay>
          <Quay id="2" version="any">
            <ParentSiteRef ref="parent" version="any"/>
          </Quay>
                </members>
              </GeneralFrame>
            </dataObjects>
          </PublicationDelivery>
          XML
          is_expected.to eq(expected_xml)
        end
      end
    end
  end

  describe "#document_for" do
    describe "file destination" do
      subject { profile.document_for(resource)&.filename }

      context "when the resource as a operator_id tag (ex: '42') and a operator_name tag (ex: 'Dummy')" do
        before { resource.tags.merge!(operator_id: '42', operator_name: 'Dummy') }

        {
          "OFFRE_42_Dummy/calendriers.xml" => [
            Netex::DayType, Netex::DayTypeAssignment, Netex::OperatingPeriod
          ],
          "OFFRE_42_Dummy/commun.xml" => [
            Netex::OrganisationalUnit
          ]
        }.each do |filename, resource_classes|
          resource_classes.each do |resource_class|
            context "when the resource is a #{resource_class}" do
              let(:resource) { resource_class.new }
              it { is_expected.to eq(filename) }
            end
          end
        end

        context "when the resource as a line_id tag (ex: '123') and a line_name tag (ex: 'test')" do
          let(:resource) { Netex::ServiceJourney.new tags: { line_id: '123', line_name: 'test' } }
          it { is_expected.to eq('OFFRE_42_Dummy/offre_123_test.xml') }
        end

      end

      [ Netex::Notice, Netex::Operator, Netex::Line, Netex::Network ].each do |resource_class|
        context "when the resource is a #{resource_class}" do
          let(:resource) { resource_class.new }
          it { is_expected.to eq("lignes.xml") }
        end
      end

      [ Netex::DayType, Netex::DayTypeAssignment, Netex::OperatingPeriod ].each do |resource_class|
        context "when the resource is a #{resource_class}" do
          let(:resource) { resource_class.new }
          it { is_expected.to eq("calendriers.xml") }
        end
      end

      context "when the resource is a StopPlace" do
        let(:resource) { Netex::StopPlace.new }
        it { is_expected.to eq("arrets.xml") }
      end
    end
  end

  describe "#operator_folder" do
    subject(:operator_folder) { profile.operator_folder operator_id: operator_id, operator_name: operator_name }
    let(:operator_id) { 42 }
    let(:operator_name) { "dummy" }

    it { is_expected.to have_attributes(id: operator_id, name: operator_name) }

    describe "#validity_periods" do
      subject { operator_folder.validity_periods }
      before { profile.validity_periods = double("Profile Validity Periods") }

      it { is_expected.to eq(profile.validity_periods) }
    end
  end

  describe Netex::Profile::IDFM::Full::OperatorFolder do
    subject(:folder) do
      Netex::Profile::IDFM::Full::OperatorFolder.new(
        operator_id: operator_id, operator_name: operator_name)
    end

    let(:operator_id) { 42 }
    let(:operator_name) { 'test' }

    describe "#normalized_name" do
      subject { folder.normalized_name }

      [
        [ "dummy", "dummy"],
        [ "with space", "with_space"],
        [ "WithUpcase", "WithUpcase"],
        [ "with accent é", "with_accent__"],
        [ "with-dash", "with-dash"],
        [ "with-number-01234567890", "with-number-01234567890" ],
        [ "with_underscore", "with_underscore"],
      ].each do |name, expected_normalized_name|
        context "when name is '#{name}'" do
          before { allow(folder).to receive(:name).and_return(name) }
          it { is_expected.to eq(expected_normalized_name) }
        end
      end

    end

    describe "#calendar_document" do
      subject { folder.calendar_document }
      let(:validity_periods) { [ Date.today..(Date.today+30) ] }
      before { folder.validity_periods = validity_periods }

      it { is_expected.to have_attributes(validity_periods: a_collection_including(an_instance_of(Range))) }
    end
  end

end

RSpec.describe Netex::Profile::IDFM::Full::Document::Calendar do

  subject(:document) do
    Netex::Profile::IDFM::Full::Document::Calendar.new(
      publication_timestamp: Time.parse("2030-01-01 12:00 UTC"),
      validity_periods: [Date.parse("2030-01-01")..Date.parse("2030-12-31")])
  end

  it { is_expected.to have_attributes(filename: "calendriers.xml") }

  describe "publication_delivery" do
    subject { document.publication_delivery }
    it { is_expected.to have_attributes(participant_ref: "FR1_OFFRE", version: "1.04:FR1-NETEX-1.6-1.8") }
    it "has the timestamp than the Document publication_timestamp" do
      is_expected.to have_attributes(timestamp: document.publication_timestamp)
    end
  end

  describe "GeneralFrame 'CALENDRIER'" do
    subject { document.frames.first }

    it "has an id with this format FR1:GeneralFrame:NETEX_CALENDRIER-{timestamp}:LOC (for ex: FR1:GeneralFrame:NETEX_CALENDRIER-20300101T120000Z:LOC)" do
      is_expected.to have_attributes(id: "FR1:GeneralFrame:NETEX_CALENDRIER-20300101T120000Z:LOC")
    end

    it { is_expected.to have_attributes(version: "1.8", data_source_ref: "FR1-OFFRE_AUTO", type_of_frame_ref: "FR1:TypeOfFrame:NETEX_CALENDRIER:") }
  end

  describe "XML sample" do

    before do
      document.add Netex::DayType.new
      document.add Netex::DayTypeAssignment.new
      document.add Netex::OperatingPeriod.new
    end

    subject { Ox.dump(document.to_xml_document, encoding: "UTF-8").strip }

    self::XML = <<~XML
<?xml version="1.0" encoding="UTF-8"?>
<PublicationDelivery xmlns="http://www.netex.org.uk/netex" version="1.04:FR1-NETEX-1.6-1.8">
  <PublicationTimestamp>2030-01-01T12:00:00Z</PublicationTimestamp>
  <ParticipantRef>FR1_OFFRE</ParticipantRef>
  <dataObjects>
    <GeneralFrame id="FR1:GeneralFrame:NETEX_CALENDRIER-20300101T120000Z:LOC" version="1.8" dataSourceRef="FR1-OFFRE_AUTO">
      <ValidBetween>
        <FromDate>2030-01-01T00:00:00+00:00</FromDate>
        <ToDate>2030-12-31T00:00:00+00:00</ToDate>
      </ValidBetween>
      <TypeOfFrameRef ref="FR1:TypeOfFrame:NETEX_CALENDRIER:"/>
      <members>
        <DayType version="any"/>
        <DayTypeAssignment version="any"/>
        <OperatingPeriod version="any"/>
      </members>
    </GeneralFrame>
  </dataObjects>
</PublicationDelivery>
      XML

    let(:expected_xml) do
      self.class::XML.strip
    end

    it "creates this XML structure:\n#{self::XML}" do
      is_expected.to eq(expected_xml)
    end

  end

end

RSpec.describe Netex::Profile::IDFM::Full::Document::Lines do

  subject(:document) do
    Netex::Profile::IDFM::Full::Document::Lines.new(
      publication_timestamp: Time.parse("2030-01-01 12:00 UTC"))
  end

  it { is_expected.to have_attributes(filename: "lignes.xml") }

  describe "publication_delivery" do
    subject { document.publication_delivery }
    it { is_expected.to have_attributes(participant_ref: "FR1_OFFRE", version: "1.04:FR1-NETEX-1.6-1.8") }
    it "has the timestamp than the Document publication_timestamp" do
      is_expected.to have_attributes(timestamp: document.publication_timestamp)
    end
  end

  describe "GeneralFrame 'COMMUN'" do
    subject { document.frames.first }

    it "has an id with this format FR1:GeneralFrame:NETEX_COMMUN-{timestamp}:LOC (for ex: FR1:GeneralFrame:NETEX_COMMUN-20300101T120000Z:LOC)" do
      is_expected.to have_attributes(id: "FR1:GeneralFrame:NETEX_COMMUN-20300101T120000Z:LOC")
    end

    it { is_expected.to have_attributes(version: "1.8", data_source_ref: "FR1-OFFRE_AUTO", type_of_frame_ref: "FR1:TypeOfFrame:NETEX_COMMUN:") }
  end

  describe "GeneralFrame 'LIGNE'" do
    subject { document.frames.last }

    it "has an id with this format FR1:GeneralFrame:NETEX_LIGNE-{timestamp}:LOC (for ex: FR1:GeneralFrame:NETEX_LIGNE-20300101T120000Z:LOC)" do
      is_expected.to have_attributes(id: "FR1:GeneralFrame:NETEX_LIGNE-20300101T120000Z:LOC")
    end

    it { is_expected.to have_attributes(version: "1.8", data_source_ref: "FR1-OFFRE_AUTO", type_of_frame_ref: "FR1:TypeOfFrame:NETEX_LIGNE:") }
  end

  describe "XML sample" do

    before do
      document.add Netex::Notice.new
      document.add Netex::Operator.new
      document.add Netex::Line.new
      document.add Netex::Network.new
    end

    subject { Ox.dump(document.to_xml_document, encoding: "UTF-8").strip }

    self::XML = <<~XML
<?xml version="1.0" encoding="UTF-8"?>
<PublicationDelivery xmlns="http://www.netex.org.uk/netex" version="1.04:FR1-NETEX-1.6-1.8">
  <PublicationTimestamp>2030-01-01T12:00:00Z</PublicationTimestamp>
  <ParticipantRef>FR1_OFFRE</ParticipantRef>
  <dataObjects>
    <CompositeFrame id="FR1:CompositeFrame:NETEX_IDF-20300101T120000Z:LOC" version="1.8" dataSourceRef="FR1-OFFRE_AUTO">
      <TypeOfFrameRef ref="FR1:TypeOfFrame:NETEX_IDF:"/>
      <frames>
        <GeneralFrame id="FR1:GeneralFrame:NETEX_COMMUN-20300101T120000Z:LOC" version="1.8" dataSourceRef="FR1-OFFRE_AUTO">
          <TypeOfFrameRef ref="FR1:TypeOfFrame:NETEX_COMMUN:"/>
          <members>
            <Notice version="any"/>
            <Operator version="any"/>
          </members>
        </GeneralFrame>
        <GeneralFrame id="FR1:GeneralFrame:NETEX_LIGNE-20300101T120000Z:LOC" version="1.8" dataSourceRef="FR1-OFFRE_AUTO">
          <TypeOfFrameRef ref="FR1:TypeOfFrame:NETEX_LIGNE:"/>
          <members>
            <Line version="any"/>
            <Network version="any"/>
          </members>
        </GeneralFrame>
      </frames>
    </CompositeFrame>
  </dataObjects>
</PublicationDelivery>
      XML

    let(:expected_xml) do
      self.class::XML.strip
    end

    it "creates this XML structure:\n#{self::XML}" do
      is_expected.to eq(expected_xml)
    end
  end
end

RSpec.describe Netex::Profile::IDFM::Full::Document::Stops do

  subject(:document) do
    Netex::Profile::IDFM::Full::Document::Stops.new(
      publication_timestamp: Time.parse("2030-01-01 12:00 UTC"))
  end

  it { is_expected.to have_attributes(filename: "arrets.xml") }

  describe "publication_delivery" do
    subject { document.publication_delivery }
    it { is_expected.to have_attributes(participant_ref: "FR1_OFFRE", version: "1.04:FR1-NETEX-1.6-1.8") }
    it "has the timestamp than the Document publication_timestamp" do
      is_expected.to have_attributes(timestamp: document.publication_timestamp)
    end
  end

  describe "GeneralFrame 'ARRET_IDF'" do
    subject { document.frames.first }

    it "has an id with this format FR1:GeneralFrame:NETEX_ARRET_IDF-{timestamp}:LOC (for ex: FR1:GeneralFrame:NETEX_ARRET_IDF-20300101T120000Z:LOC)" do
      is_expected.to have_attributes(id: "FR1:GeneralFrame:NETEX_ARRET_IDF-20300101T120000Z:LOC")
    end

    it { is_expected.to have_attributes(version: "1.8", data_source_ref: "FR1-OFFRE_AUTO", type_of_frame_ref: "FR1:TypeOfFrame:NETEX_ARRET_IDF:") }
  end

  describe "XML sample" do

    before do
      document.add Netex::StopPlace.new
      document.add Netex::Quay.new
      document.add Netex::StopPlaceEntrance.new
    end

    subject { Ox.dump(document.to_xml_document, encoding: "UTF-8").strip }

    self::XML = <<~XML
<?xml version="1.0" encoding="UTF-8"?>
<PublicationDelivery xmlns="http://www.netex.org.uk/netex" xmlns:gml="http://www.opengis.net/gml/3.2" version="1.04:FR1-NETEX-1.6-1.8">
  <PublicationTimestamp>2030-01-01T12:00:00Z</PublicationTimestamp>
  <ParticipantRef>FR1_OFFRE</ParticipantRef>
  <dataObjects>
    <GeneralFrame id="FR1:GeneralFrame:NETEX_ARRET_IDF-20300101T120000Z:LOC" version="1.8" dataSourceRef="FR1-OFFRE_AUTO">
      <TypeOfFrameRef ref="FR1:TypeOfFrame:NETEX_ARRET_IDF:"/>
      <members>
        <StopPlace version="any"/>
        <Quay version="any"/>
        <StopPlaceEntrance version="any"/>
      </members>
    </GeneralFrame>
  </dataObjects>
</PublicationDelivery>
      XML

    let(:expected_xml) do
      self.class::XML.strip
    end

    it "creates this XML structure:\n#{self::XML}" do
      is_expected.to eq(expected_xml)
    end
  end
end

RSpec.describe Netex::Profile::IDFM::Full::Document::Offre do

  subject(:document) do
    Netex::Profile::IDFM::Full::Document::Offre.new(
      publication_timestamp: Time.parse("2030-01-01 12:00 UTC"),
      line_id: 42,
      line_name: "Test"
    )
  end

  describe "XML sample" do
    subject { Ox.dump(document.to_xml_document, encoding: "UTF-8").strip }

    before do
      document.add Netex::Route.new
      document.add Netex::ServiceJourney.new
    end

    self::XML = <<~XML
<?xml version="1.0" encoding="UTF-8"?>
<PublicationDelivery xmlns="http://www.netex.org.uk/netex" version="1.04:FR1-NETEX-1.6-1.8">
  <PublicationTimestamp>2030-01-01T12:00:00Z</PublicationTimestamp>
  <ParticipantRef>FR1_OFFRE</ParticipantRef>
  <dataObjects>
    <CompositeFrame id="FR1:CompositeFrame:NETEX_OFFRE_LIGNE-20300101T120000Z:LOC" version="1.8" dataSourceRef="FR1-OFFRE_AUTO">
      <Name>Test</Name>
      <TypeOfFrameRef ref="FR1:TypeOfFrame:NETEX_OFFRE_LIGNE:"/>
      <frames>
        <GeneralFrame id="FR1:GeneralFrame:NETEX_STRUCTURE-20300101T120000Z:LOC" version="1.8" dataSourceRef="FR1-OFFRE_AUTO">
          <TypeOfFrameRef ref="FR1:TypeOfFrame:NETEX_STRUCTURE:"/>
          <members>
            <Route version="any"/>
          </members>
        </GeneralFrame>
        <GeneralFrame id="FR1:GeneralFrame:NETEX_HORAIRE-20300101T120000Z:LOC" version="1.8" dataSourceRef="FR1-OFFRE_AUTO">
          <TypeOfFrameRef ref="FR1:TypeOfFrame:NETEX_HORAIRE:"/>
          <members>
            <ServiceJourney version="any"/>
          </members>
        </GeneralFrame>
      </frames>
    </CompositeFrame>
  </dataObjects>
</PublicationDelivery>
      XML

    let(:expected_xml) do
      self.class::XML.strip
    end

    it "creates this XML structure:\n#{self::XML}" do
      is_expected.to eq(expected_xml)
    end
  end
end
