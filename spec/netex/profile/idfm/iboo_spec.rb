# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Netex::Profile::IDFM::IBOO do
  subject(:profile) { described_class.new }

  describe '#transform' do
    subject { profile.transform(resource) }

    context 'when resource is Netex::Line' do
      let(:resource) { Netex::Line.new }

      it { is_expected.to be_nil }
    end

    context 'when resource is Netex::ServiceJourneyPattern' do
      let(:resource) { Netex::ServiceJourneyPattern.new }

      it { expect { subject }.to change(resource, :service_journey_pattern_type).to('passenger') }
    end
  end
end

RSpec.describe Netex::Profile::IDFM::IBOO::Document::Calendar do
  subject(:document) do
    described_class.new(
      code_space: 'code-space',
      participant_ref: 'participant-ref',
      publication_timestamp: Time.parse('2030-01-01 12:00 UTC'),
      validity_periods: [Date.parse('2030-01-01')..Date.parse('2030-12-31')],
      builder_book: Netex::Profile::IDFM::BuilderBook.new
    )
  end

  it { is_expected.to have_attributes(filename: 'OFFRE_participant-ref_20300101120000Z/calendriers.xml') }

  describe 'publication_delivery' do
    subject { document.publication_delivery }
    it { is_expected.to have_attributes(participant_ref: 'participant-ref', version: '1.04:FR1-NETEX-2.0-0') }
    it 'has the timestamp than the Document publication_timestamp' do
      is_expected.to have_attributes(timestamp: document.publication_timestamp)
    end
  end

  describe "GeneralFrame 'CALENDRIER'" do
    subject { document.frames.first }

    it 'has an id with this format code-space:GeneralFrame:NETEX_CALENDRIER-{timestamp}:LOC (for ex: code-space:GeneralFrame:NETEX_CALENDRIER-20300101T120000Z:LOC)' do
      is_expected.to have_attributes(id: 'code-space:GeneralFrame:NETEX_CALENDRIER-20300101T120000Z:LOC')
    end

    it { is_expected.to have_attributes(version: nil) }

    it do
      is_expected.to have_attributes(
                       type_of_frame_ref: an_object_having_attributes(ref: 'FR1:TypeOfFrame:NETEX_CALENDRIER:')
                     )
    end
  end

  describe 'XML sample' do
    before do
      document.add Netex::DayType.new
      document.add Netex::DayTypeAssignment.new
      document.add Netex::OperatingPeriod.new
    end

    subject { Ox.dump(document.to_xml_document, encoding: 'UTF-8').strip }

    self::XML = <<~XML
      <?xml version="1.0" encoding="UTF-8"?>
      <PublicationDelivery xmlns="http://www.netex.org.uk/netex" version="1.04:FR1-NETEX-2.0-0">
        <PublicationTimestamp>2030-01-01T12:00:00Z</PublicationTimestamp>
        <ParticipantRef>participant-ref</ParticipantRef>
        <dataObjects>
          <GeneralFrame id="code-space:GeneralFrame:NETEX_CALENDRIER-20300101T120000Z:LOC" version="any">
            <ValidBetween>
              <FromDate>2030-01-01T00:00:00+00:00</FromDate>
              <ToDate>2030-12-31T00:00:00+00:00</ToDate>
            </ValidBetween>
            <TypeOfFrameRef ref="FR1:TypeOfFrame:NETEX_CALENDRIER:">version="1.04:FR1-NETEX_CALENDRIER-2.1"</TypeOfFrameRef>
            <members>
              <DayType version="any"/>
              <DayTypeAssignment version="any"/>
              <OperatingPeriod version="any"/>
            </members>
          </GeneralFrame>
        </dataObjects>
      </PublicationDelivery>
    XML

    let(:expected_xml) do
      self.class::XML.strip
    end

    it "creates this XML structure:\n#{self::XML}" do
      is_expected.to be_same_xml(expected_xml)
    end
  end
end

RSpec.describe Netex::Profile::IDFM::IBOO::Document::Offre do
  subject(:document) do
    described_class.new(
      code_space: 'code-space',
      participant_ref: 'participant-ref',
      publication_timestamp: Time.parse('2030-01-01 12:00 UTC'),
      line_id: 'FR1:Line:C90192:',
      line_name: 'Test',
      builder_book: Netex::Profile::IDFM::BuilderBook.new
    )
  end

  describe 'XML sample' do
    subject { Ox.dump(document.to_xml_document, encoding: 'UTF-8').strip }

    before do
      document.add Netex::Route.new
      document.add Netex::ServiceJourney.new
    end

    self::XML = <<~XML
      <?xml version="1.0" encoding="UTF-8"?>
      <PublicationDelivery xmlns="http://www.netex.org.uk/netex" version="1.04:FR1-NETEX-2.0-0">
        <PublicationTimestamp>2030-01-01T12:00:00Z</PublicationTimestamp>
        <ParticipantRef>participant-ref</ParticipantRef>
        <dataObjects>
          <CompositeFrame id="code-space:CompositeFrame:NETEX_OFFRE_LIGNE-C90192:LOC" version="any">
            <Name>Test</Name>
            <TypeOfFrameRef ref="FR1:TypeOfFrame:NETEX_OFFRE_LIGNE:">version="1.04:FR1-NETEX_OFFRE_LIGNE-2.1"</TypeOfFrameRef>
            <frames>
              <GeneralFrame id="code-space:GeneralFrame:NETEX_STRUCTURE-20300101T120000Z:LOC" version="any">
                <TypeOfFrameRef ref="FR1:TypeOfFrame:NETEX_STRUCTURE:">version="1.04:FR1-NETEX_STRUCTURE-2.1"</TypeOfFrameRef>
                <members>
                  <Route version="any"/>
                </members>
              </GeneralFrame>
              <GeneralFrame id="code-space:GeneralFrame:NETEX_HORAIRE-20300101T120000Z:LOC" version="any">
                <TypeOfFrameRef ref="FR1:TypeOfFrame:NETEX_HORAIRE:">version="1.04:FR1-NETEX_HORAIRE-2.1"</TypeOfFrameRef>
                <members>
                  <ServiceJourney version="any"/>
                </members>
              </GeneralFrame>
            </frames>
          </CompositeFrame>
        </dataObjects>
      </PublicationDelivery>
    XML

    let(:expected_xml) do
      self.class::XML.strip
    end

    it "creates this XML structure:\n#{self::XML}" do
      is_expected.to be_same_xml(expected_xml)
    end
  end
end
