# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Netex::ObjectId do
  def parse(definition)
    Netex::ObjectId.parse definition
  end

  describe '.parse' do
    [
      ['chouette:JourneyPattern:1:LOC', nil, 'chouette', 'JourneyPattern', '1', 'LOC'],
      ['chouette:JourneyPattern:1:', nil, 'chouette', 'JourneyPattern', '1', nil],
      ['FR:SMIRT:MonomodalStopPlace:454e74:', 'FR', 'SMIRT', 'MonomodalStopPlace', '454e74', nil]
    ].each do |definition, country, local, type, technical, provider|
      attributes = { country: country, local: local, type: type, technical: technical, provider: provider }
      it "creates a ObjectId with #{attributes.inspect} from #{definition}" do
        expect(parse(definition)).to have_attributes(attributes)
      end
    end

    it 'returns nil when the given definition is nil' do
      expect(parse(nil)).to be_nil
    end
  end

  describe '#change' do
    [
      ['test:JourneyPattern:1:', { country: 'FR' }, 'FR:test:JourneyPattern:1:'],
      ['test:JourneyPattern:1:', { local: 'ara' }, 'ara:JourneyPattern:1:'],
      ['test:JourneyPattern:1:', { type: 'StopPoint' }, 'test:StopPoint:1:'],
      ['test:JourneyPattern:1:', { technical: 2 }, 'test:JourneyPattern:2:'],
      ['test:JourneyPattern:1:', { technical: [1, 2] }, 'test:JourneyPattern:1-2:'],
      ['test:JourneyPattern:1:', { provider: 'LOC' }, 'test:JourneyPattern:1:LOC']
    ].each do |definition, attributes, expected|
      it "with #{attributes.inspect} transforms #{definition} into #{expected}" do
        expect(parse(definition).change(**attributes).to_s).to eq(expected)
      end
    end
  end

  describe '#merge' do
    [
      ['test:JourneyPattern:1:', 'other:StopPoint:2:', 'StopPointInJourneyPattern',
       'provider', 'test:StopPointInJourneyPattern:1-2:provider']
    ].each do |definition, other, type, provider, expected|
      it "creates #{expected} from #{definition} and #{other} with #{type}" do
        expect(parse(definition).merge(parse(other), type: type, provider: provider).to_s).to eq(expected)
      end
    end

    [
      ['test:JourneyPattern:1:', 'other:StopPoint:2:', 'StopPointInJourneyPattern',
       'test:StopPointInJourneyPattern:1-2:'],
      ['test:JourneyPattern:1:', '2', 'StopPointInJourneyPattern', 'test:StopPointInJourneyPattern:1-2:']
    ].each do |definition, other, type, expected|
      it "creates #{expected} from #{definition} and #{other} with #{type}" do
        other = parse(other) || other
        expect(parse(definition).merge(other, type: type).to_s).to eq(expected)
      end
    end

    [
      ['test:JourneyPattern:1:', 'other:StopPoint:2:', 'test:JourneyPattern:1-2:'],
      ['test:JourneyPattern:1:', '2', 'test:JourneyPattern:1-2:']
    ].each do |definition, other, expected|
      it "creates #{expected} from #{definition} and #{other} without type" do
        other = parse(other) || other
        expect(parse(definition).merge(other).to_s).to eq(expected)
      end
    end
  end
end
