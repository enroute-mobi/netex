# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Netex::Transformer::CoordinatesFromLocation do
  let(:transformer) { Netex::Transformer::CoordinatesFromLocation.new target_projection: 'epsg:4326' }

  context 'when target projection is epsg:2154' do
    let(:transformer) { Netex::Transformer::CoordinatesFromLocation.new target_projection: 'epsg:2154' }

    subject(:transformed_location) { transformer.transform(location) }

    context 'when locatiion latitude/longitude is 48.896297960237824,2.318820355524601' do
      let(:location) { Netex::Location.new(latitude: 48.896297960237824, longitude: 2.318820355524601) }

      describe 'location coordinates' do
        subject { transformed_location.coordinates }

        it { is_expected.to have_attributes(x: a_value_within(0.1).of(650_058)) }
        it { is_expected.to have_attributes(y: a_value_within(0.1).of(6_866_470)) }
        it { is_expected.to have_attributes(srs_name: 'epsg:2154') }
      end
    end
  end

  context 'when the location has already coordinates' do
    let(:location) { Netex::Location.new(coordinates: GML::Pos.new) }

    it { expect { transformer.transform(location) }.to_not change(location, :coordinates) }
  end

  context 'when the location has no latitude/longitude' do
    let(:location) { Netex::Location.new }

    subject(:transformed_location) { transformer.transform(location) }

    it { is_expected.to have_attributes(coordinates: nil) }
  end
end

RSpec.describe Netex::Transformer::LocationFromCoordinates do
  it 'fill location latitude/longitude/srs_name with coordinates' do
    location = Netex::Location.new(coordinates: GML::Pos.new(srs_name: 'EPSG:2154', x: 650_058.0, y: 6_866_470.0))
    subject.transform location

    expect(location.latitude).to be_within(0.00000001).of(48.896297960237824)
    expect(location.longitude).to be_within(0.00000001).of(2.318820355524601)
    expect(location.srs_name).to eq('epsg:4326')
  end

  it 'keeps existing latitude/longitude' do
    location = Netex::Location.new(latitude: 48, longitude: 2, coordinates: GML::Pos.new(x: 650_058.0, y: 6_866_470.0))
    expect { subject.transform(location) }.to not_change(location, :latitude).and not_change(location, :longitude)
  end

  it 'ignores undefined coordinates' do
    location = Netex::Location.new
    subject.transform location
    expect(location).to have_attributes(latitude: nil, longitude: nil)
  end

  it 'ignores incomplete coordinates' do
    location = Netex::Location.new(coordinates: GML::Pos.new(x: 650_058.0, y: 6_866_470.0))
    subject.transform location
    expect(location).to have_attributes(latitude: nil, longitude: nil)
  end
end

RSpec.describe Netex::Transformer::EmbeddedQuays do
  alias_method :transformer, :subject

  before do
    transformer.builder_book = Netex::Target::BuilderBook.new
  end

  describe '#transform_stop_place' do
    context 'when a Quay has been transformed previously' do
      [
        ['parent', :itself],
        [Netex::ObjectId.parse('test:StopPoint:1:'), :itself],
        [Netex::ObjectId.parse('test:StopPoint:1:'), :to_s]
      ].each do |stop_place_id, parent_id_method|
        parent_id = stop_place_id.send(parent_id_method)
        context "and StopPlace is identified by #{stop_place_id.inspect} and Quay parent_id tag #{parent_id}" do
          let(:stop_place) { Netex::StopPlace.new id: stop_place_id }
          let(:quay) { Netex::Quay.new(id: 'child').with_tag(parent_id: parent_id) }
          let(:quay_xml) { '<Quay id="child" version="any"/>' }

          before { transformer.transform quay }

          it 'changes the StopPlace quays raw_content' do
            expect { transformer.transform(stop_place) }.to change { stop_place.quays.raw_content }.to(quay_xml)
          end
        end
      end
    end
  end
end

RSpec.describe Netex::Transformer::Indexer do
  subject { described_class.new(Netex::Line, by: attribute) }
  let(:attribute) { :transport_mode }

  describe '#transform' do
    context 'when given resource is an instance of the indexed class' do
      let(:resource) { Netex::StopPlace.new(attribute => 'wrong') }

      it { expect { subject.transform(resource) }.to_not change(resource, :indexes) }
    end

    context 'when given resource is an instance of the indexed class' do
      let(:resource) { Netex::Line.new(attribute => 'target') }

      it { expect { subject.transform(resource) }.to change(resource, :indexes).from({}).to({ attribute => 'target' }) }
    end

    context 'when target attribute is a Reference' do
      let(:attribute) { :operator_ref }
      let(:resource) { Netex::Line.new(attribute => Netex::Reference.new('target', type: Netex::Operator)) }

      it { expect { subject.transform(resource) }.to change(resource, :indexes).from({}).to({ attribute => 'target' }) }
    end
  end
end

RSpec.describe Netex::Transformer::Uniqueness do
  subject(:transformer) { described_class.new }

  let(:resource) { Netex::Quay.new id: 42 }

  describe '#known?' do
    subject { transformer.known?(resource) }

    context 'when a first resource is given' do
      it { is_expected.to be_falsy }
    end

    context 'when a resource with the same Class and id has been already given' do
      before { transformer.known?(resource.dup) }

      it { is_expected.to be_truthy }
    end

    context 'when a resource with a different Class and the same id has been already given' do
      before { transformer.known?(Netex::StopPlace.new(id: resource.id)) }

      it { is_expected.to be_falsy }
    end
  end

  describe '#transform' do
    subject { transformer.transform(resource) }
    let(:resource) { double 'given NeTEx resource' }

    context 'when known? returns false' do
      before { allow(transformer).to receive(:known?).and_return(false) }

      it { is_expected.to eq(resource) }
    end

    context 'when known? returns true' do
      before { allow(transformer).to receive(:known?).and_return(true) }

      it { is_expected.to be_nil }
    end
  end

  describe 'complete example' do
    let(:source) { Netex::Source.new }

    before { source.transformers << described_class.new }

    context 'when a Source reads "<StopPlace id="target"/><StopPlace id="target"/>"' do
      let(:xml) { '<root><StopPlace id="target"/><StopPlace id="target"/></root>' }

      before { source.parse(StringIO.new(xml)) }

      describe 'related collection' do
        subject(:collection) { source.stop_places }

        it { is_expected.to have_attributes(count: 1) }
        it { is_expected.to contain_exactly(an_object_having_attributes(id: 'target')) }
      end
    end
  end
end
