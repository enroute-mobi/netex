# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Netex::Content do
  subject(:content) { Netex::Content.new('test') }

  describe "#xml_name" do
    subject { content.xml_name }

    context "when Content is attribute" do
      before { content.content_type = :attribute }

      [
        [ :version, :version ],
        [ :type_of_key, :typeOfKey],
        [ :data_source_ref, :dataSourceRef ]
      ].each do |name, expected_xml_name|
        context "when name is '#{name}'" do
          before { content.name = name }

          it { is_expected.to eq(expected_xml_name) }
        end
      end
    end

    context "when Content is collection" do
      before { content.content_type = :collection }

      [
        [ :quays, :quays ],
        [ :key_list, :keyList ],
        [ :notice_assignments, :noticeAssignments ]
      ].each do |name, expected_xml_name|
        context "when name is '#{name}'" do
          before { content.name = name }

          it { is_expected.to eq(expected_xml_name) }
        end
      end
    end

    context "when Content is element" do
      before { content.content_type = :element }

      [
        [ :name, :Name ],
        [ :departure_time, :DepartureTime ],
        [ :is_available, :isAvailable ]
      ].each do |name, expected_xml_name|
        context "when name is '#{name}'" do
          before { content.name = name }

          it { is_expected.to eq(expected_xml_name) }
        end
      end
    end
  end
end
