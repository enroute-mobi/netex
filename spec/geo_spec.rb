require "spec_helper"

RSpec.describe Geo::Transformation do

  it "converts RGF93 / Lambert-93 into WGS84" do
    source = Geo::Position.new(x: 650058.0, y: 6866470.0)
    transformation = Geo::Transformation.create('epsg:2154', 'epsg:4326')
    target = transformation.transform(source)

    expect(target.latitude).to be_within(0.000001).of(48.896297960237824)
    expect(target.longitude).to be_within(0.000001).of(2.318820355524601)

    expect(target.latitude).to be_an(Float)
    expect(target.longitude).to be_an(Float)
  end
end
