# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'netex/version'

Gem::Specification.new do |spec| # ruboco
  spec.name          = 'netex'
  spec.version       = Netex::VERSION
  spec.authors       = ['Alban Peignier']
  spec.email         = ['alban.peignier@enroute.mobi']

  spec.summary       = 'Read and write NeTEx data'
  spec.description   = 'Manage NeTEx (CEN Technical Standard for exchanging Public Transport schedules and related data) content'
  spec.homepage      = 'http://bitbucket.org/enroute-mobi/netex'
  spec.license       = 'Apache License Version 2.0'

  spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = spec.homepage
  spec.metadata['changelog_uri'] = spec.homepage

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec', '~> 3.2'
  spec.add_development_dependency 'stackprof'
  spec.add_development_dependency 'simplecov'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'diffy'

  spec.add_runtime_dependency 'rubyzip'
  spec.add_runtime_dependency 'ox'
  spec.add_runtime_dependency 'rgeo-proj4'
  spec.add_runtime_dependency 'activesupport'
end
